﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BaseballTours
{
    public partial class Welcome : Form
    {
        Torneo torneo = null;
        string torneoSelezionato = null;
        string annoSelezionato = null;

        public Welcome()
        {
            InitializeComponent();

            this.updateTourInfo();
            this.updateTournaments(0);

            //imposto la visualizzazione dei dati nella listBox
            this.listTournaments.DisplayMember = "Nome";
            this.listTournaments.ValueMember = "ID";
        }

        private void updateTournaments(int from)
        {
            Torneo temp;

            int count = 0;
            int index = 0;

            this.listTournaments.Items.Clear();

            string selectTornei = "SELECT * FROM TORNEO";
            var cmd = DbConnection.Open(selectTornei);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                temp = new Torneo
                {
                    Nome = (string)reader["NOME"],
                    ID = (int)reader["ID"],
                    Squadra = (string)reader["IDSQUADRAORGANIZZATRICE"]
                };

                this.listTournaments.Items.Add(temp);

                if (from != 0 && temp.ID == from)
                    index = count;

                count++;
            }

            if (index != 0)
                this.listTournaments.SelectedIndex = index;

            DbConnection.Close();
        }

        private void listTournament_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listTournaments.SelectedItem == null)
                return;
            torneo = (Torneo) this.listTournaments.SelectedItem;
            this.annoSelezionato = null;
            this.updateTourInfo();
            this.listAnni.Items.Clear();

            string selectAnni = "SELECT ANNO FROM EDIZIONE WHERE IDTORNEO = @TORNEO";
            var cmd = DbConnection.Open(selectAnni);
            cmd.Parameters.AddWithValue("TORNEO", torneo.ID);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.listAnni.Items.Add(reader["ANNO"]);
            }
            DbConnection.Close();
        }

        private void carica_Click(object sender, EventArgs e)
        {
            if (this.torneo == null || this.annoSelezionato == null)
            {
                ErrorDialog.Show("Errore", "Selezionare un torneo ed un relativo anno!");
                return;
            }
            this.Hide();

            using (var tourWindow = new GestioneTorneo(this.torneo, this.annoSelezionato))
            {
                DialogResult result = tourWindow.ShowDialog(this);
                this.updateTournaments(tourWindow.Torneo);
                this.updateTourInfo();
            }
            
        }

        private void nuovoTour_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new InserisciTorneo().ShowDialog();
            this.updateTournaments(0);
        }

        private void listAnni_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listAnni.SelectedItem != null)
            {
                this.annoSelezionato = this.listAnni.SelectedItem.ToString();
                this.updateTourInfo();
            }
        }

        private void updateTourInfo()
        {
            if (this.annoSelezionato == null || this.torneo == null)
            {
                this.selData.Visible = false;
                this.selNomeTour.Visible = false;
                this.selSquadra.Visible = false;
                this.carica.Visible = false;
                if (this.torneo == null)
                    this.txtInfoTorneo.Text = "Seleziona un torneo!";
                else
                {
                    this.selNomeTour.Visible = true;
                    this.selNomeTour.Text = this.torneoSelezionato;
                    this.txtInfoTorneo.Text = "Seleziona un anno!";
                }

                return;
            }

            // Recupero l'inizio del torneo andando a pescare la data della prima partita che viene giocata
            var selectSQL = "SELECT MIN(DATAORAPARTITA) AS INIZIO FROM PARTITA WHERE IDTORNEO = @ID " + 
                "AND ANNOEDIZIONE = @ANNO";

            var cmd = DbConnection.Open(selectSQL);
            cmd.Parameters.AddWithValue("ID", this.torneo.ID);
            cmd.Parameters.AddWithValue("ANNO", this.annoSelezionato);
            var reader = cmd.ExecuteReader();
            DateTime? dataTorneo = null;

            if (reader.Read() && !reader.IsDBNull(0))
            {
                this.selNomeTour.Text = this.torneo.Nome;
                this.selSquadra.Text = this.torneo.Squadra;
                dataTorneo = reader.GetDateTime(0);
                this.selData.Text = ((DateTime)dataTorneo).ToString("dd/MM/yyyy");

                //Setting all visible
                this.selData.Visible = true;
                this.selNomeTour.Visible = true;
                this.selSquadra.Visible = true;
            }
            else
            {
                //Setting all invisible
                this.selData.Visible = false;
                this.selNomeTour.Visible = false;
                this.selSquadra.Visible = false;
            }
            DbConnection.Close();

            this.carica.Visible = true;
            this.txtInfoTorneo.Text = DbConnection.retrieveClassifica(this.torneo.ID, this.annoSelezionato);

            return;
        }

        private void campiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaCampi().ShowDialog();
        }

        private void volontariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaPersone().ShowDialog();
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Welcome_Load(object sender, EventArgs e)
        {
            //TODO: scrivere qui il codice da eseguire all'avvio della finestra
        }

        private void Welcome_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void squadreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaSquadre().ShowDialog();
            this.updateTournaments(0);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //MODIFICA
            if (this.listTournaments.SelectedItem != null)
            {
                if (this.listAnni.SelectedItem != null)
                {
                    //apri modifica anno edizione
                    new ModificaTorneo(this.torneo, this.annoSelezionato).ShowDialog();
                }
                else
                {
                    //apri modifica nome / squadra organizzatrice
                    new ModificaTorneo(this.torneo).ShowDialog();
                }

                this.updateTournaments(0);
                this.listAnni.Items.Clear();
            }
            else
            {
                ErrorDialog.Show("Errore", "Selezionare almeno un torneo!");
                return;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //ELIMINAZIONE

            try
            {
                if (this.torneo != null)
                {
                    string sql = "";
                    SqlCeCommand cmd = null;

                    if (this.annoSelezionato != null)
                    {
                        //elimino l'edizione
                        sql = "DELETE FROM EDIZIONE WHERE IDTORNEO = @ID AND ANNO = @ANNO";

                        cmd = DbConnection.Open(sql);

                        cmd.CommandText = sql;
                        cmd.Parameters.AddWithValue("ID", this.torneo.ID);
                        cmd.Parameters.AddWithValue("ANNO", this.annoSelezionato);

                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        //elimino il torneo
                        sql = "DELETE FROM TORNEO WHERE ID = @ID";

                        cmd = DbConnection.Open(sql);

                        cmd.CommandText = sql;
                        cmd.Parameters.AddWithValue("ID", this.torneo.ID);

                        cmd.ExecuteNonQuery();
                    }

                    DbConnection.Close();

                    this.updateTourInfo();
                    this.updateTournaments(0);
                    this.listAnni.Items.Clear();
                }
                else
                {
                    ErrorDialog.Show("Errore", "Selezionare un torneo e / o un anno!");
                    return;
                }
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
        }

        private void mansioniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaMansioni().ShowDialog();
        }
    }
}
