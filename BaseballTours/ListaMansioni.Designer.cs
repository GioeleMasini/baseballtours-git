﻿namespace BaseballTours
{
    partial class ListaMansioni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaMansioni));
            this.gridMansioni = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEMANSIONEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIZIONEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mANSIONEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnElimina = new System.Windows.Forms.Button();
            this.btnModifica = new System.Windows.Forms.Button();
            this.btnAggiungi = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIndietro = new System.Windows.Forms.Button();
            this.mANSIONETableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.MANSIONETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridMansioni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mANSIONEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridMansioni
            // 
            this.gridMansioni.AllowUserToAddRows = false;
            this.gridMansioni.AllowUserToDeleteRows = false;
            this.gridMansioni.AllowUserToOrderColumns = true;
            this.gridMansioni.AllowUserToResizeRows = false;
            this.gridMansioni.AutoGenerateColumns = false;
            this.gridMansioni.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridMansioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMansioni.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nOMEMANSIONEDataGridViewTextBoxColumn,
            this.dESCRIZIONEDataGridViewTextBoxColumn});
            this.tableLayoutPanel1.SetColumnSpan(this.gridMansioni, 2);
            this.gridMansioni.DataSource = this.mANSIONEBindingSource;
            this.gridMansioni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMansioni.Location = new System.Drawing.Point(3, 4);
            this.gridMansioni.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridMansioni.MultiSelect = false;
            this.gridMansioni.Name = "gridMansioni";
            this.gridMansioni.ReadOnly = true;
            this.gridMansioni.RowHeadersVisible = false;
            this.gridMansioni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMansioni.Size = new System.Drawing.Size(423, 252);
            this.gridMansioni.TabIndex = 1;
            this.gridMansioni.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMansioni_CellDoubleClick);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // nOMEMANSIONEDataGridViewTextBoxColumn
            // 
            this.nOMEMANSIONEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nOMEMANSIONEDataGridViewTextBoxColumn.DataPropertyName = "NOMEMANSIONE";
            this.nOMEMANSIONEDataGridViewTextBoxColumn.HeaderText = "Mansione";
            this.nOMEMANSIONEDataGridViewTextBoxColumn.Name = "nOMEMANSIONEDataGridViewTextBoxColumn";
            this.nOMEMANSIONEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMEMANSIONEDataGridViewTextBoxColumn.Width = 104;
            // 
            // dESCRIZIONEDataGridViewTextBoxColumn
            // 
            this.dESCRIZIONEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dESCRIZIONEDataGridViewTextBoxColumn.DataPropertyName = "DESCRIZIONE";
            this.dESCRIZIONEDataGridViewTextBoxColumn.HeaderText = "Descrizione";
            this.dESCRIZIONEDataGridViewTextBoxColumn.Name = "dESCRIZIONEDataGridViewTextBoxColumn";
            this.dESCRIZIONEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mANSIONEBindingSource
            // 
            this.mANSIONEBindingSource.DataMember = "MANSIONE";
            this.mANSIONEBindingSource.DataSource = this.baseDataDataSet;
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.04651F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.95349F));
            this.tableLayoutPanel1.Controls.Add(this.gridMansioni, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(429, 320);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnElimina);
            this.flowLayoutPanel1.Controls.Add(this.btnModifica);
            this.flowLayoutPanel1.Controls.Add(this.btnAggiungi);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(114, 264);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(312, 52);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btnElimina
            // 
            this.btnElimina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnElimina.Location = new System.Drawing.Point(227, 8);
            this.btnElimina.Margin = new System.Windows.Forms.Padding(3, 8, 10, 4);
            this.btnElimina.Name = "btnElimina";
            this.btnElimina.Size = new System.Drawing.Size(75, 40);
            this.btnElimina.TabIndex = 4;
            this.btnElimina.Text = "Elimina";
            this.btnElimina.UseVisualStyleBackColor = true;
            this.btnElimina.Click += new System.EventHandler(this.btnElimina_Click);
            // 
            // btnModifica
            // 
            this.btnModifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModifica.Location = new System.Drawing.Point(139, 8);
            this.btnModifica.Margin = new System.Windows.Forms.Padding(3, 8, 10, 4);
            this.btnModifica.Name = "btnModifica";
            this.btnModifica.Size = new System.Drawing.Size(75, 40);
            this.btnModifica.TabIndex = 3;
            this.btnModifica.Text = "Modifica";
            this.btnModifica.UseVisualStyleBackColor = true;
            this.btnModifica.Click += new System.EventHandler(this.btnModifica_Click);
            // 
            // btnAggiungi
            // 
            this.btnAggiungi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAggiungi.Location = new System.Drawing.Point(51, 8);
            this.btnAggiungi.Margin = new System.Windows.Forms.Padding(3, 8, 10, 4);
            this.btnAggiungi.Name = "btnAggiungi";
            this.btnAggiungi.Size = new System.Drawing.Size(75, 40);
            this.btnAggiungi.TabIndex = 2;
            this.btnAggiungi.Text = "Aggiungi";
            this.btnAggiungi.UseVisualStyleBackColor = true;
            this.btnAggiungi.Click += new System.EventHandler(this.btnAggiungi_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnIndietro);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 263);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(105, 54);
            this.flowLayoutPanel2.TabIndex = 2;
            // 
            // btnIndietro
            // 
            this.btnIndietro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIndietro.Location = new System.Drawing.Point(10, 8);
            this.btnIndietro.Margin = new System.Windows.Forms.Padding(10, 8, 10, 4);
            this.btnIndietro.Name = "btnIndietro";
            this.btnIndietro.Size = new System.Drawing.Size(75, 40);
            this.btnIndietro.TabIndex = 5;
            this.btnIndietro.Text = "Indietro";
            this.btnIndietro.UseVisualStyleBackColor = true;
            this.btnIndietro.Click += new System.EventHandler(this.btnIndietro_Click);
            // 
            // mANSIONETableAdapter
            // 
            this.mANSIONETableAdapter.ClearBeforeFill = true;
            // 
            // ListaMansioni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 320);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "ListaMansioni";
            this.Text = "Baseball Tournaments - Mansioni";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridMansioni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mANSIONEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridMansioni;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnIndietro;
        private System.Windows.Forms.Button btnElimina;
        private System.Windows.Forms.Button btnModifica;
        private System.Windows.Forms.Button btnAggiungi;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.BindingSource mANSIONEBindingSource;
        private BaseDataDataSetTableAdapters.MANSIONETableAdapter mANSIONETableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEMANSIONEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIZIONEDataGridViewTextBoxColumn;
    }
}