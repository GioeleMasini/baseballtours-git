﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlServerCe;

namespace BaseballTours
{
    public partial class InserisciTorneo : Form
    {
        public InserisciTorneo()
        {
            InitializeComponent();
            this.radioButton1.Checked = true;
            this.radioButton2.Checked = false;
            this.listaTornei.Enabled = true;
            this.nuovoTorneo.Enabled = false;
            this.annoTorneo.Enabled = true;
            this.listaSquadre.Enabled = false;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.listaTornei.Enabled = this.radioButton1.Checked ? true : false;
            if (this.radioButton1.Checked)
                this.autoSelectListaSquadre();
        }

        private void InserisciTorneo_Load(object sender, EventArgs e)
        {
            this.caricaSquadre("");

            string selectTornei = "SELECT ID, NOME FROM TORNEO";
            var cmd = DbConnection.Open(selectTornei);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Item i = new Item();
                i.Text = reader["NOME"].ToString();
                i.Value = reader["ID"];
                this.listaTornei.Items.Add(i);
            }

            DbConnection.Close();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.nuovoTorneo.Enabled = this.radioButton2.Checked ? true : false;
            this.listaSquadre.Enabled = this.radioButton2.Checked ? true : false;
        }

        private void salva_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = (this.radioButton1.Checked ? listaTornei.SelectedItem.ToString() : nuovoTorneo.Text);
                string anno = this.annoTorneo.Text;
                string squadra = (this.listaSquadre.SelectedItem as Item).Value.ToString();

                int affectedRows = 0;

                Int32 torneoID = 0;
                SqlCeCommand cmd = null;

                //Se il torneo non esiste lo creo
                if (radioButton2.Checked)
                {
                    String insertTorneoSQL = "INSERT INTO TORNEO (NOME, IDSQUADRAORGANIZZATRICE) "
                        + "VALUES(@NOME, @SQUADRA)";

                    cmd = DbConnection.Open(insertTorneoSQL);
                    cmd.CommandText = insertTorneoSQL;
                    cmd.Parameters.AddWithValue("NOME", nome);
                    cmd.Parameters.AddWithValue("SQUADRA", squadra);

                    affectedRows += cmd.ExecuteNonQuery();

                    String retrieveID = "SELECT @@Identity";
                    cmd.CommandText = retrieveID;

                    torneoID = Convert.ToInt32(cmd.ExecuteScalar());
                }
                else
                {
                    torneoID = Convert.ToInt32((listaTornei.SelectedItem as Item).Value.ToString());
                }

                if (torneoID == 0)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", "Errore nel recupero dell'ID del torneo per inserimento nuova edizione");
                    DbConnection.Close();
                    return;
                }

                String insertEdizioneSQL = "INSERT INTO EDIZIONE (IDTORNEO, ANNO) "
                    + "VALUES(@TORNEO, @ANNO)";

                if (cmd == null)
                    cmd = DbConnection.Open(insertEdizioneSQL);

                cmd.CommandText = insertEdizioneSQL;
                cmd.Parameters.AddWithValue("TORNEO", torneoID);
                cmd.Parameters.AddWithValue("ANNO", anno);

                affectedRows += cmd.ExecuteNonQuery();

                if (affectedRows < 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                    DbConnection.Close();
                    return;
                }
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
            DbConnection.Close();
            this.Close();
        }

        private void nuovaSquadra_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new InserisciSquadra().ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listaTornei_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autoSelectListaSquadre();
        }

        private void autoSelectListaSquadre()
        {
            if (this.listaTornei.SelectedItem == null)
                this.listaSquadre.SelectedItem = null;
            else
            {
                Int32 idTorneo = Convert.ToInt32((this.listaTornei.SelectedItem as Item).Value.ToString());

                //Vado a cercare la squadra organizzatrice di quel torneo
                var selectSquadra = "SELECT SQUADRA.NOME, SQUADRA.ID FROM SQUADRA, TORNEO WHERE SQUADRA.ID = TORNEO.IDSQUADRAORGANIZZATRICE AND TORNEO.ID = @IDTORNEO";
                SqlCeCommand cmd = DbConnection.Open(selectSquadra);
                cmd.Parameters.AddWithValue("IDTORNEO", idTorneo);
                SqlCeDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    foreach (Item i in this.listaSquadre.Items)
                    {
                        if (i.Text == reader[0].ToString())
                        {
                            this.listaSquadre.SelectedItem = i;
                            break;
                        }
                    }

                }

                DbConnection.Close();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //new InserisciSquadra().ShowDialog();
            using (var form = new InserisciSquadra()){
                var result = form.ShowDialog();

                if (result == DialogResult.OK)
                {
                    var id = form.id;
                    this.caricaSquadre(id);
                }

            }
            
        }

        //funzione per il load delle squadre nella combo box
        private void caricaSquadre(string id)
        {
            this.listaSquadre.Items.Clear();

            //Inserisco le squadre in lista squadre
            string selectSquadre = "SELECT NOME, ID FROM SQUADRA";
            var cmd = DbConnection.Open(selectSquadre);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Item i = new Item();
                i.Text = reader["NOME"].ToString();
                i.Value = reader["ID"].ToString();
                this.listaSquadre.Items.Add(i);

                //TODO seleziona squadra appena inserita
                if(!id.Equals("") && id.Equals(i.Value))
                {
                    this.listaSquadre.SelectedItem = i;
                }
            }

            DbConnection.Close();
        }
    }
}
