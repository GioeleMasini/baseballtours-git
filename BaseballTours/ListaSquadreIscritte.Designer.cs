﻿namespace BaseballTours
{
    partial class ListaSquadreIscritte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaSquadreIscritte));
            this.gridSquadre = new System.Windows.Forms.DataGridView();
            this.sQUADRABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnElimina = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.sQUADRATableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.SQUADRATableAdapter();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cITTADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkIscritta = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cOLOREDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lOGODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridSquadre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridSquadre
            // 
            this.gridSquadre.AllowUserToAddRows = false;
            this.gridSquadre.AllowUserToDeleteRows = false;
            this.gridSquadre.AllowUserToOrderColumns = true;
            this.gridSquadre.AllowUserToResizeRows = false;
            this.gridSquadre.AutoGenerateColumns = false;
            this.gridSquadre.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSquadre.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridSquadre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSquadre.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nOMEDataGridViewTextBoxColumn,
            this.cITTADataGridViewTextBoxColumn,
            this.chkIscritta,
            this.cOLOREDataGridViewTextBoxColumn,
            this.lOGODataGridViewTextBoxColumn});
            this.gridSquadre.DataSource = this.sQUADRABindingSource;
            this.gridSquadre.Location = new System.Drawing.Point(3, 4);
            this.gridSquadre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridSquadre.MultiSelect = false;
            this.gridSquadre.Name = "gridSquadre";
            this.gridSquadre.RowHeadersVisible = false;
            this.gridSquadre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSquadre.ShowCellErrors = false;
            this.gridSquadre.ShowRowErrors = false;
            this.gridSquadre.Size = new System.Drawing.Size(537, 303);
            this.gridSquadre.TabIndex = 0;
            // 
            // sQUADRABindingSource
            // 
            this.sQUADRABindingSource.DataMember = "SQUADRA";
            this.sQUADRABindingSource.DataSource = this.baseDataDataSet;
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.gridSquadre);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(544, 370);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel2.Controls.Add(this.btnElimina);
            this.flowLayoutPanel2.Controls.Add(this.button1);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 314);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(537, 51);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // btnElimina
            // 
            this.btnElimina.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnElimina.Location = new System.Drawing.Point(443, 10);
            this.btnElimina.Margin = new System.Windows.Forms.Padding(3, 10, 10, 3);
            this.btnElimina.Name = "btnElimina";
            this.btnElimina.Size = new System.Drawing.Size(84, 38);
            this.btnElimina.TabIndex = 0;
            this.btnElimina.Text = "Salva";
            this.btnElimina.UseVisualStyleBackColor = true;
            this.btnElimina.Click += new System.EventHandler(this.btnElimina_Click);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button1.Location = new System.Drawing.Point(11, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 10, 345, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "Annulla";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sQUADRATableAdapter
            // 
            this.sQUADRATableAdapter.ClearBeforeFill = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 46;
            // 
            // nOMEDataGridViewTextBoxColumn
            // 
            this.nOMEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nOMEDataGridViewTextBoxColumn.DataPropertyName = "NOME";
            this.nOMEDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nOMEDataGridViewTextBoxColumn.Name = "nOMEDataGridViewTextBoxColumn";
            this.nOMEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cITTADataGridViewTextBoxColumn
            // 
            this.cITTADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cITTADataGridViewTextBoxColumn.DataPropertyName = "CITTA";
            this.cITTADataGridViewTextBoxColumn.HeaderText = "Città";
            this.cITTADataGridViewTextBoxColumn.Name = "cITTADataGridViewTextBoxColumn";
            this.cITTADataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // chkIscritta
            // 
            this.chkIscritta.HeaderText = "Iscritta";
            this.chkIscritta.Name = "chkIscritta";
            this.chkIscritta.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chkIscritta.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cOLOREDataGridViewTextBoxColumn
            // 
            this.cOLOREDataGridViewTextBoxColumn.DataPropertyName = "COLORE";
            this.cOLOREDataGridViewTextBoxColumn.HeaderText = "COLORE";
            this.cOLOREDataGridViewTextBoxColumn.Name = "cOLOREDataGridViewTextBoxColumn";
            this.cOLOREDataGridViewTextBoxColumn.ReadOnly = true;
            this.cOLOREDataGridViewTextBoxColumn.Visible = false;
            // 
            // lOGODataGridViewTextBoxColumn
            // 
            this.lOGODataGridViewTextBoxColumn.DataPropertyName = "LOGO";
            this.lOGODataGridViewTextBoxColumn.HeaderText = "LOGO";
            this.lOGODataGridViewTextBoxColumn.Name = "lOGODataGridViewTextBoxColumn";
            this.lOGODataGridViewTextBoxColumn.ReadOnly = true;
            this.lOGODataGridViewTextBoxColumn.Visible = false;
            // 
            // ListaSquadreIscritte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(544, 370);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "ListaSquadreIscritte";
            this.Text = "Baseball Tournaments - Squadre";
            this.Load += new System.EventHandler(this.ListaSquadre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridSquadre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridSquadre;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnElimina;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.BindingSource sQUADRABindingSource;
        private BaseDataDataSetTableAdapters.SQUADRATableAdapter sQUADRATableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cITTADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkIscritta;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOLOREDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lOGODataGridViewTextBoxColumn;
    }
}