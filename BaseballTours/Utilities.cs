﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace BaseballTours
{
    class Utilities
    {
        public static string listToString(List<int> list)
        {
            //Trasforma una lista in una commaseparated-list
            return String.Join(";", list);   
        }

        public static List<int> stringToList(string stringa)
        {
            //Fa il contrario, dalla stringa alla lista
            return stringa == "" ? new List<int>() : stringa.Split(';').Select(int.Parse).ToList();   
        }

        public static object[] getDataGridRowValues(DataGridViewRow row)
        {
            return Utilities.getDataGridRowValues(row, 0, row.Cells.Count - 1);
        }

        //ATTENZIONE: endIndex compreso!
        public static object[] getDataGridRowValues(DataGridViewRow row, int startIndex, int endIndex)
        {
            List<object> values = new List<object>();
            for (int i = startIndex; i <= endIndex; i++)
            {
                values.Add(row.Cells[i].Value);
            }
            return values.ToArray();
        }
        //Per convertire l'array in un altro tipo usare
        //objectArray.Cast<int>().ToArray(); PER GLI INT NON CI DEVONO ESSERE VALORI NULL

        public static string colorToHex(Color c)
        {
            return ColorTranslator.ToHtml(c);
        }

        public static Color colorFromHex(string htmlString)
        {
            return ColorTranslator.FromHtml(htmlString);
        }

        public static int? stringToNullableInt(string s)
        {
            int i;
            if (Int32.TryParse(s, out i)) return i;
            return null;
        }

        public static string deleteUselessSpaces(string dirtyString)
        {
            return System.Text.RegularExpressions.Regex.Replace(dirtyString, @"\s+", " ");
        }
    }
}
