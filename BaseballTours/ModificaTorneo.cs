﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ModificaTorneo : Form
    {
        private Torneo torneo;
        private string anno;

        public ModificaTorneo(Torneo torneo)
        {
            InitializeComponent();
            this.annoEdizione.Visible = false;
            this.label4.Visible = false;

            this.torneo = torneo;
        }

        public ModificaTorneo(Torneo torneo, string anno)
        {
            InitializeComponent();
            this.listaSquadre.Enabled = false;
            this.nomeTorneo.Enabled = false;

            this.torneo = torneo;
            this.anno = anno;
            this.annoEdizione.Text = anno;
        }

        private void ModificaTorneo_Load(object sender, EventArgs e)
        {
            string selectSquadre = "SELECT NOME, ID FROM SQUADRA";
            var cmd = DbConnection.Open(selectSquadre);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Item i = new Item();
                i.Text = reader["NOME"].ToString();
                i.Value = reader["ID"].ToString();
                this.listaSquadre.Items.Add(i);

                if (i.Value.Equals(this.torneo.Squadra))
                    this.listaSquadre.SelectedItem = i;
            }

            this.nomeTorneo.Text = this.torneo.Nome;
        }

        private void annulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void salva_Click(object sender, EventArgs e)
        {
            //FIX ME: bisognerebbe aggiornare tutte le relazioni dove c'è ANNOEDIZIONE
            //          visto che al momento sembrerebbe che la regola di aggiornamento
            //          a cascata non funziona

            try
            {
                //Ho modificato nome e / o squadra organizzatrice
                if (this.nomeTorneo.Enabled)
                {
                    if (this.nomeTorneo.Text.Equals(""))
                    {
                        //ERROR
                        ErrorDialog.Show("Errore", "Inserire il nome del torneo");
                        return;
                    }

                    string sql = "UPDATE TORNEO SET NOME = @NOME, IDSQUADRAORGANIZZATRICE = @SQUADRA "
                        + "WHERE ID = @TORNEO";

                    var cmd = DbConnection.Open(sql);

                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("NOME", this.nomeTorneo.Text);
                    cmd.Parameters.AddWithValue("SQUADRA", (this.listaSquadre.SelectedItem as Item).Value.ToString());
                    cmd.Parameters.AddWithValue("TORNEO", this.torneo.ID);

                    cmd.ExecuteNonQuery();

                    DbConnection.Close();
                }
                else
                {
                    //Ho modificato l'anno edizione

                    if (this.annoEdizione.Text.Equals(""))
                    {
                        //ERROR
                        ErrorDialog.Show("Errore", "Inserire il nuovo anno edizione");
                        return;
                    }

                    string sql = "UPDATE EDIZIONE SET ANNO = @ANNO WHERE IDTORNEO = @TORNEO AND ANNO = @OLDANNO";

                    var cmd = DbConnection.Open(sql);

                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("OLDANNO", this.anno);
                    cmd.Parameters.AddWithValue("ANNO", this.annoEdizione.Text);
                    cmd.Parameters.AddWithValue("TORNEO", this.torneo.ID);

                    cmd.ExecuteNonQuery();

                    DbConnection.Close();
                }

                MessageBox.Show("Modifiche avvenute con successo", "Successo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
            
        }
    }
}
