﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class InserisciMansione : Form
    {
        Boolean editable = false;
        String insertMansione;
        string mansione;
        string descrizione;
        int idMansione;
        System.Data.SqlServerCe.SqlCeCommand cmd;
        System.Data.SqlServerCe.SqlCeDataReader reader;

        public InserisciMansione()
        {
            InitializeComponent();
        }

        public InserisciMansione(int id)
        {
            InitializeComponent();

            this.idMansione = id;

            string selection = "SELECT NOMEMANSIONE, DESCRIZIONE FROM MANSIONE WHERE ID = @ID";
            
            this.cmd = DbConnection.Open(selection);
            this.cmd.Parameters.AddWithValue("ID", this.idMansione);
            this.reader = this.cmd.ExecuteReader();

            if (this.reader.Read())
            {
                this.nomeMansione.Text = reader[0].ToString();
                this.descrizioneMansione.Text = reader[1].ToString();
            }

            this.cmd.Parameters.Clear();
            DbConnection.Close();

            this.editable = true;
            this.label1.Text = "Modifica mansione";
        }

        private void Resetta_Click(object sender, EventArgs e)
        {
            this.nomeMansione.ResetText();
            this.descrizioneMansione.ResetText();
        }

        private void GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Salva_Click(object sender, EventArgs e)
        {
            // Controllo presenza valori nei campi
            if (this.nomeMansione.Text.Equals("") || this.descrizioneMansione.Text.Equals(""))
            {
                ErrorDialog.Show("Errore", "Inserire tutti i valori per tutti i campi");
                return;
            }

            this.mansione = this.nomeMansione.Text;
            this.descrizione = this.descrizioneMansione.Text;

            if (!this.editable)
            {
                // Controllo presenza mansioni con gli stessi valori
                string controlloDoppio = "SELECT * FROM MANSIONE WHERE NOMEMANSIONE = @MANSIONE AND DESCRIZIONE = @DESC";

                this.cmd = DbConnection.Open(controlloDoppio);

                this.cmd.CommandText = controlloDoppio;
                this.cmd.Parameters.AddWithValue("MANSIONE", this.mansione);
                this.cmd.Parameters.AddWithValue("DESC", this.descrizione);
                this.reader = this.cmd.ExecuteReader();

                if (this.reader.Read())
                {
                    // Errore, tupla con valore di chiave già presente
                    ErrorDialog.Show("Errore di duplicazione", "Mansione con tale descrizione già presente.\n"
                        + "Inserire un'altra mansione o un'altra descrizione.");
                    return;
                }
                else
                {
                    // Nessuna tupla con quella chiave, può continuare, ripulisco il cmd.
                    this.cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }
            else
            {
                string controlloDoppio = "SELECT * FROM MANSIONE WHERE NOMEMANSIONE = @MANSIONE AND DESCRIZIONE = @DESC "
                    + "AND ID <> @ID";

                this.cmd = DbConnection.Open(controlloDoppio);

                this.cmd.CommandText = controlloDoppio;
                this.cmd.Parameters.AddWithValue("MANSIONE", this.mansione);
                this.cmd.Parameters.AddWithValue("DESC", this.descrizione);
                this.cmd.Parameters.AddWithValue("ID", this.idMansione);
                this.reader = this.cmd.ExecuteReader();

                if (this.reader.Read())
                {
                    // Errore, tupla con valore di chiave già presente
                    ErrorDialog.Show("Errore di duplicazione", "Mansione con tale descrizione già presente.\n"
                        + "Inserire un'altra mansione o un'altra descrizione.");
                    return;
                }
                else
                {
                    // Nessuna tupla con quella chiave, può continuare, ripulisco il cmd.
                    this.cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }

            try
            {
                if (!this.editable)
                {
                    this.insertMansione = "INSERT INTO MANSIONE (NOMEMANSIONE, DESCRIZIONE) "
                        + "VALUES (@MANSIONE, @DESCRIZIONE)";

                    this.cmd = DbConnection.Open(this.insertMansione);
                }
                else
                {
                    this.insertMansione = "UPDATE MANSIONE SET DESCRIZIONE = @DESCRIZIONE, NOMEMANSIONE = @MANSIONE"
                        + " WHERE ID = @ID";

                    this.cmd = DbConnection.Open(this.insertMansione);
                }

                this.cmd.CommandText = this.insertMansione;
                this.cmd.Parameters.AddWithValue("MANSIONE", this.mansione);
                this.cmd.Parameters.AddWithValue("DESCRIZIONE", this.descrizione);
                this.cmd.Parameters.AddWithValue("ID", this.idMansione);

                int affectedRows = this.cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                }

                DbConnection.Close();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }

            this.Close();
        }
    }
}
