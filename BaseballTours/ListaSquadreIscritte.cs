﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaSquadreIscritte : Form
    {
        private int torneoId;
        private string annoTorneo;
        private List<string> listIscritte;
        public ListaSquadreIscritte(int torneoId, string annoTorneo)
        {
            this.torneoId = torneoId;
            this.annoTorneo = annoTorneo;
            InitializeComponent();
        }

        private void ListaSquadre_Load(object sender, EventArgs e)
        {
            gridRefresh();
        }

        private void btnElimina_Click(object sender, EventArgs e)
        {
            //Salva le squadre iscritte al torneo
            try
            {
                //Creo la lista con le nuove squadre iscritte
                var newListIscritte = new List<string>();
                foreach (DataGridViewRow row in this.gridSquadre.Rows)
                {
                    var isIscritta = row.Cells["chkIscritta"].Value;
                    if (isIscritta != null && (bool)isIscritta)
                    {
                        var idSquadra = row.Cells["iDDataGridViewTextBoxColumn"].Value.ToString();
                        newListIscritte.Add(idSquadra);
                    }
                }

                //Preparo le query
                string deleteSquadra = "DELETE FROM SQUADRAEDIZIONE WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDSQUADRA = @IDSQUADRA";
                var cmdDelete = DbConnection.Open(deleteSquadra);
                cmdDelete.Parameters.AddWithValue("IDTORNEO", this.torneoId);
                cmdDelete.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                cmdDelete.Parameters.AddWithValue("IDSQUADRA", null);

                string sqlInsert = "INSERT INTO SQUADRAEDIZIONE VALUES (@IDSQUADRA, @IDTORNEO, @ANNOEDIZIONE)";
                var cmdInsert = DbConnection.Open(sqlInsert);
                cmdInsert.Parameters.AddWithValue("IDTORNEO", this.torneoId);
                cmdInsert.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                cmdInsert.Parameters.AddWithValue("IDSQUADRA", null);

                //Cancello tutte le squadre non più iscritte
                foreach (var idSquadra in this.listIscritte)
                {
                    if (newListIscritte.IndexOf(idSquadra) < 0)
                    {
                        cmdDelete.Parameters["IDSQUADRA"].Value = idSquadra;
                        cmdDelete.ExecuteNonQuery();
                    }
                }

                //Inserisco tutte le squadre che sono appena state iscritte
                foreach (var idSquadra in newListIscritte)
                {
                    if (this.listIscritte.IndexOf(idSquadra) < 0)
                    {
                        cmdInsert.Parameters["IDSQUADRA"].Value = idSquadra;
                        cmdInsert.ExecuteNonQuery();
                    }
                }

                DbConnection.Close();

                this.listIscritte = newListIscritte;
                //gridRefresh();
                this.Close();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridRefresh()
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.SQUADRA'. È possibile spostarla o rimuoverla se necessario.
            this.sQUADRATableAdapter.Fill(this.baseDataDataSet.SQUADRA);

            //Una volta riempita la tabella, imposto tramite select manuale la check di iscrizione
            string sqlSelectIscritta = "SELECT IDSQUADRA FROM SQUADRAEDIZIONE WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE";
            var cmd = DbConnection.Open(sqlSelectIscritta);

            cmd.Parameters.AddWithValue("IDTORNEO", this.torneoId);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);

            var reader = cmd.ExecuteReader();

            this.listIscritte = new List<string>();
            while (reader.Read())
            {
                listIscritte.Add(reader["IDSQUADRA"].ToString());
            }

            //Cerco l'id nella tabella
            foreach (DataGridViewRow row in this.gridSquadre.Rows)
            {
                var idPersonaTable = row.Cells["iDDataGridViewTextBoxColumn"].Value.ToString();
                var index = listIscritte.IndexOf(idPersonaTable);
                if (index >= 0)
                {
                    var arrayDati = listIscritte[index];
                    row.Cells["chkIscritta"].Value = true;
                }
            }
        }
    }
}
