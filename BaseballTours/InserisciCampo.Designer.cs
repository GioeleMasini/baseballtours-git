﻿namespace BaseballTours
{
    partial class InserisciCampo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciCampo));
            this.GoBack = new System.Windows.Forms.Button();
            this.Resetta = new System.Windows.Forms.Button();
            this.Salva = new System.Windows.Forms.Button();
            this.civicoCampo = new System.Windows.Forms.TextBox();
            this.nomeCittà = new System.Windows.Forms.TextBox();
            this.nomeCampo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.viaCampo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // GoBack
            // 
            this.GoBack.Location = new System.Drawing.Point(12, 232);
            this.GoBack.Margin = new System.Windows.Forms.Padding(2);
            this.GoBack.Name = "GoBack";
            this.GoBack.Size = new System.Drawing.Size(91, 41);
            this.GoBack.TabIndex = 7;
            this.GoBack.Text = "Annulla";
            this.GoBack.UseVisualStyleBackColor = true;
            this.GoBack.Click += new System.EventHandler(this.GoBack_Click);
            // 
            // Resetta
            // 
            this.Resetta.Location = new System.Drawing.Point(137, 232);
            this.Resetta.Margin = new System.Windows.Forms.Padding(2);
            this.Resetta.Name = "Resetta";
            this.Resetta.Size = new System.Drawing.Size(91, 41);
            this.Resetta.TabIndex = 6;
            this.Resetta.Text = "Resetta campi";
            this.Resetta.UseVisualStyleBackColor = true;
            this.Resetta.Click += new System.EventHandler(this.Resetta_Click);
            // 
            // Salva
            // 
            this.Salva.Location = new System.Drawing.Point(254, 232);
            this.Salva.Margin = new System.Windows.Forms.Padding(2);
            this.Salva.Name = "Salva";
            this.Salva.Size = new System.Drawing.Size(91, 41);
            this.Salva.TabIndex = 5;
            this.Salva.Text = "Salva";
            this.Salva.UseVisualStyleBackColor = true;
            this.Salva.Click += new System.EventHandler(this.Salva_Click);
            // 
            // civicoCampo
            // 
            this.civicoCampo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.civicoCampo.Location = new System.Drawing.Point(111, 110);
            this.civicoCampo.Margin = new System.Windows.Forms.Padding(2);
            this.civicoCampo.MaxLength = 6;
            this.civicoCampo.Name = "civicoCampo";
            this.civicoCampo.Size = new System.Drawing.Size(82, 27);
            this.civicoCampo.TabIndex = 4;
            // 
            // nomeCittà
            // 
            this.nomeCittà.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeCittà.Location = new System.Drawing.Point(111, 39);
            this.nomeCittà.Margin = new System.Windows.Forms.Padding(2);
            this.nomeCittà.Name = "nomeCittà";
            this.nomeCittà.Size = new System.Drawing.Size(224, 27);
            this.nomeCittà.TabIndex = 2;
            // 
            // nomeCampo
            // 
            this.nomeCampo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeCampo.Location = new System.Drawing.Point(111, 4);
            this.nomeCampo.Margin = new System.Windows.Forms.Padding(2);
            this.nomeCampo.Name = "nomeCampo";
            this.nomeCampo.Size = new System.Drawing.Size(224, 27);
            this.nomeCampo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nome";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Città";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 77);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Via";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.20779F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.79221F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.nomeCampo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.civicoCampo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.nomeCittà, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.viaCampo, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 73);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(339, 144);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 114);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Civico";
            // 
            // viaCampo
            // 
            this.viaCampo.Location = new System.Drawing.Point(112, 73);
            this.viaCampo.Name = "viaCampo";
            this.viaCampo.Size = new System.Drawing.Size(223, 27);
            this.viaCampo.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(8, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(337, 69);
            this.label3.TabIndex = 0;
            this.label3.Text = "Inserisci un nuovo campo";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // InserisciCampo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 282);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GoBack);
            this.Controls.Add(this.Resetta);
            this.Controls.Add(this.Salva);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "InserisciCampo";
            this.Text = "Baseball Tournaments - Inserisci campo";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GoBack;
        private System.Windows.Forms.Button Resetta;
        private System.Windows.Forms.Button Salva;
        private System.Windows.Forms.TextBox civicoCampo;
        private System.Windows.Forms.TextBox nomeCittà;
        private System.Windows.Forms.TextBox nomeCampo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox viaCampo;
    }
}