﻿namespace BaseballTours
{
    partial class InserisciTurno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciTurno));
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.persona = new System.Windows.Forms.ComboBox();
            this.oraFine = new System.Windows.Forms.DateTimePicker();
            this.annoTorneo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nomeTorneo = new System.Windows.Forms.TextBox();
            this.oraInizio = new System.Windows.Forms.DateTimePicker();
            this.mansione = new System.Windows.Forms.ComboBox();
            this.Salva = new System.Windows.Forms.Button();
            this.Resetta = new System.Windows.Forms.Button();
            this.GoBack = new System.Windows.Forms.Button();
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.visualizzaInfo = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(366, 74);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserisci un nuovo turno";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.33952F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.66048F));
            this.tableLayoutPanel1.Controls.Add(this.persona, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.oraFine, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.annoTorneo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.nomeTorneo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.oraInizio, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.mansione, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(14, 84);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(377, 226);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // persona
            // 
            this.persona.DropDownHeight = 130;
            this.persona.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.persona.FormattingEnabled = true;
            this.persona.IntegralHeight = false;
            this.persona.Location = new System.Drawing.Point(139, 117);
            this.persona.Name = "persona";
            this.persona.Size = new System.Drawing.Size(228, 27);
            this.persona.TabIndex = 2;
            this.persona.MouseHover += new System.EventHandler(this.persona_MouseHover);
            // 
            // oraFine
            // 
            this.oraFine.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.oraFine.CustomFormat = "dd/MM HH:mm";
            this.oraFine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.oraFine.Location = new System.Drawing.Point(139, 193);
            this.oraFine.Name = "oraFine";
            this.oraFine.Size = new System.Drawing.Size(136, 27);
            this.oraFine.TabIndex = 5;
            // 
            // annoTorneo
            // 
            this.annoTorneo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.annoTorneo.Location = new System.Drawing.Point(139, 42);
            this.annoTorneo.Name = "annoTorneo";
            this.annoTorneo.Size = new System.Drawing.Size(66, 27);
            this.annoTorneo.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Torneo";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Anno";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mansione";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Persona";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ora inizio";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Ora fine";
            // 
            // nomeTorneo
            // 
            this.nomeTorneo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeTorneo.Location = new System.Drawing.Point(139, 5);
            this.nomeTorneo.Name = "nomeTorneo";
            this.nomeTorneo.Size = new System.Drawing.Size(231, 27);
            this.nomeTorneo.TabIndex = 0;
            // 
            // oraInizio
            // 
            this.oraInizio.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.oraInizio.CustomFormat = "dd/MM HH:mm";
            this.oraInizio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.oraInizio.Location = new System.Drawing.Point(139, 157);
            this.oraInizio.Name = "oraInizio";
            this.oraInizio.Size = new System.Drawing.Size(136, 27);
            this.oraInizio.TabIndex = 4;
            // 
            // mansione
            // 
            this.mansione.DisplayMember = "ID";
            this.mansione.DropDownHeight = 100;
            this.mansione.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mansione.FormattingEnabled = true;
            this.mansione.IntegralHeight = false;
            this.mansione.Location = new System.Drawing.Point(139, 77);
            this.mansione.Name = "mansione";
            this.mansione.Size = new System.Drawing.Size(228, 27);
            this.mansione.TabIndex = 1;
            this.mansione.ValueMember = "ID";
            this.mansione.MouseHover += new System.EventHandler(this.mansione_MouseHover);
            // 
            // Salva
            // 
            this.Salva.Location = new System.Drawing.Point(306, 320);
            this.Salva.Name = "Salva";
            this.Salva.Size = new System.Drawing.Size(85, 40);
            this.Salva.TabIndex = 6;
            this.Salva.Text = "Salva";
            this.Salva.UseVisualStyleBackColor = true;
            this.Salva.Click += new System.EventHandler(this.Salva_Click);
            // 
            // Resetta
            // 
            this.Resetta.Location = new System.Drawing.Point(166, 320);
            this.Resetta.Name = "Resetta";
            this.Resetta.Size = new System.Drawing.Size(85, 40);
            this.Resetta.TabIndex = 7;
            this.Resetta.Text = "Resetta campi";
            this.Resetta.UseVisualStyleBackColor = true;
            this.Resetta.Click += new System.EventHandler(this.Resetta_Click);
            // 
            // GoBack
            // 
            this.GoBack.Location = new System.Drawing.Point(12, 320);
            this.GoBack.Name = "GoBack";
            this.GoBack.Size = new System.Drawing.Size(85, 40);
            this.GoBack.TabIndex = 8;
            this.GoBack.Text = "Indietro";
            this.GoBack.UseVisualStyleBackColor = true;
            this.GoBack.Click += new System.EventHandler(this.GoBack_Click);
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // InserisciTurno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 372);
            this.Controls.Add(this.GoBack);
            this.Controls.Add(this.Resetta);
            this.Controls.Add(this.Salva);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "InserisciTurno";
            this.Text = "Baseball Tournaments - Inserisci turno";
            this.Load += new System.EventHandler(this.InserisciTurno_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox annoTorneo;
        private System.Windows.Forms.TextBox nomeTorneo;
        private System.Windows.Forms.DateTimePicker oraFine;
        private System.Windows.Forms.DateTimePicker oraInizio;
        private System.Windows.Forms.Button Salva;
        private System.Windows.Forms.Button Resetta;
        private System.Windows.Forms.Button GoBack;
        private System.Windows.Forms.ComboBox persona;
        private System.Windows.Forms.ComboBox mansione;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.ToolTip visualizzaInfo;
    }
}