﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BaseballTours
{
    public partial class Partita : Form
    {
        //private string nomeTorneo;
        private long? idPartita;
        private int idTorneo;
        private string annoTorneo;
        private DateTime dataPartita;
        private int? idCampo = null;
        private string nomeCampo;
        private string cittaCampo;

        private string nomeSquadraCasa;
        private string nomeSquadraOspiti;
        private string codSquadraCasa;
        private string codSquadraOspite;
        private int codTipologia;
        private List<short?> parzialiCasa = new List<short?>();
        private List<short> parzialiOspiti = new List<short>();
        private int? punteggioCasa;
        private int? punteggioOspiti;
        private short valideCasa;
        private short erroriCasa;
        private short valideOspiti;
        private short erroriOspiti;
        private Image imgCasa;
        private Image imgOspiti;
        private List<object[]> rosterCasa = new List<object[]>();
        private List<object[]> rosterOspiti = new List<object[]>();
        private List<object[]> cambiCasa = new List<object[]>();
        private List<object[]> cambiOspiti = new List<object[]>();
        private long? idFormazioneCasa;
        private long? idFormazioneOspiti;
        private List<Persona> arbitri = new List<Persona>();

        private Boolean editable = false;
        private Boolean newInsert;

        public Partita(int idTorneo, string annoEdizione)
        {
            init(null, true);
            this.idTorneo = idTorneo;
            this.annoTorneo = annoEdizione;
            this.setMaxMinData(this.annoTorneo);
        }

        public void init(long? idPartita, Boolean editable)
        {
            InitializeComponent();

            this.idPartita = idPartita;
            this.editable = editable;
            this.newInsert = true;

            //Setting some style
            this.gridPunteggi.Columns[0].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.gridPunteggi.Columns[10].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            if (editable)
            {
                //Disabling modifica formazione
                this.btnModifica1_formazione.Visible = false;
                this.btnModifica2_formazione.Visible = false;

                //Enabling editing controls
                this.Team1.Visible = false;
                this.Team2.Visible = false;
                this.lvArbitri.Visible = false;
                this.txtCampo.Visible = false;
                this.addSquadra1.Visible = true;
                this.addSquadra2.Visible = true;
                this.addArbitro.Visible = true;
                this.addCampo.Visible = true;
                this.addData.Visible = true;
                this.Risultato.Text = "Inserimento di una nuova partita";
                this.gridPunteggi.ReadOnly = false;
                this.gridPunteggi.Rows.Add("OSP.");
                this.gridPunteggi.Rows.Add("CASA");
                this.gridPunteggi.RowsDefaultCellStyle.BackColor = Color.White;
                this.gridPunteggi.RowsDefaultCellStyle.SelectionBackColor = Color.White;
                this.gridPunteggi.Columns[0].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                this.gridPunteggi.Columns[0].ReadOnly = true;
                this.gridPunteggi.Columns[10].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                //this.gridRosterCasa.DefaultCellStyle = this.gridRosterCasa.AlternatingRowsDefaultCellStyle;
                //this.gridRosterOspiti.DefaultCellStyle = this.gridRosterOspiti.AlternatingRowsDefaultCellStyle;
                this.btnSalva.Visible = true;
                this.btnAnnulla.Visible = true;
                this.btnModifica1_cambi.Visible = false;
                this.btnModifica2_cambi.Visible = false;
                this.addTipologia.Visible = true;
                this.tipoPartita.Visible = false;
                this.txtValide1.Visible = false;
                this.txtErrori1.Visible = false;
                this.txtValide2.Visible = false;
                this.txtErrori2.Visible = false;
                this.addValide1.Visible = true;
                this.addErrori1.Visible = true;
                this.addValide2.Visible = true;
                this.addErrori2.Visible = true;
            }
            else
            {

            }
        }

        private void setMaxMinData(string annoEdizione)
        {
            this.addData.MinDate = new DateTime(int.Parse(annoEdizione), 1, 1);
            this.addData.MaxDate = new DateTime(int.Parse(annoEdizione), 12, 31);
        }

        public Partita(int idPartita)
            : this(idPartita, false)
        {
        }

        public Partita(int idPartita, Boolean editable)
        {
            init(idPartita, editable);
            this.newInsert = !editable;
            //this.dataPartita = dataOra;
            //this.idCampo = codCampo;
            //Recupero i dati dal database
            string selectPartita = 
                "SELECT"
                        + " TORNEO.ID AS IDTORNEO, E.ANNO AS ANNOEDIZIONE, PA.DATAORAPARTITA, PA.IDSQUADRACASA, PA.IDSQUADRAOSPITI, C.NOME AS NOMECAMPO, C.CITTA AS CITTACAMPO, C.ID AS IDCAMPO, "
                        + " PA.PUNTICASA, PA.PUNTIOSPITI, PA.VALIDECASA, PA.VALIDEOSPITI, PA.ERRORICASA, PA.ERRORIOSPITI, PA.IDFORMAZIONECASA, PA.IDFORMAZIONEOSPITI, PA.IDTIPOLOGIA, "
                        + " TI.NOME AS NOMETIPOLOGIA, TORNEO.NOME AS NOMETORNEO"
                + " FROM"
                        + " TORNEO INNER JOIN"
                        + " EDIZIONE AS E ON TORNEO.ID = E.IDTORNEO INNER JOIN"
                        + " CAMPO AS C RIGHT OUTER JOIN"
                        + " PARTITA AS PA ON C.ID = PA.IDCAMPO ON E.IDTORNEO = PA.IDTORNEO AND E.ANNO = PA.ANNOEDIZIONE LEFT OUTER JOIN"
                        + " TIPOPARTITA AS TI ON PA.IDTIPOLOGIA = TI.CODICE"
                + " WHERE"
                        + " (PA.ID = @IDPARTITA)";
            var cmd = DbConnection.Open(selectPartita);
            cmd.Parameters.AddWithValue("IDPARTITA", idPartita);
            var reader = cmd.ExecuteReader();

            if (!reader.Read())
            {
                ErrorDialog.Show("Errore", "Impossibile recuperare la partita.");
                this.Close();
                return;
            }

            //Imposto tutte le variabili
            string pathImgCasa = null;
            string pathImgOspiti = null;
            this.nomeCampo = reader["NOMECAMPO"].ToString();
            this.cittaCampo = reader["CITTACAMPO"].ToString();
            this.codSquadraCasa = reader["IDSQUADRACASA"].ToString();
            this.codSquadraOspite = reader["IDSQUADRAOSPITI"].ToString();
            this.punteggioCasa = Utilities.stringToNullableInt(reader["PUNTICASA"].ToString());
            this.punteggioOspiti = Utilities.stringToNullableInt(reader["PUNTIOSPITI"].ToString());
            this.dataPartita = reader.GetDateTime(2);
            if (reader["IDTIPOLOGIA"] != DBNull.Value)
                this.codTipologia = int.Parse(reader["IDTIPOLOGIA"].ToString());
            if (reader["NOMETIPOLOGIA"] != DBNull.Value)
                this.tipoPartita.Text = reader["NOMETIPOLOGIA"].ToString();
            this.valideCasa = short.Parse(reader["VALIDECASA"].ToString());
            this.valideOspiti = short.Parse(reader["VALIDEOSPITI"].ToString());
            this.erroriCasa = short.Parse(reader["ERRORICASA"].ToString());
            this.erroriOspiti = short.Parse(reader["ERRORIOSPITI"].ToString());

            this.idTorneo = int.Parse(reader["IDTORNEO"].ToString());
            this.annoTorneo = reader["ANNOEDIZIONE"].ToString();
            this.txtNomeTour.Text = reader["NOMETORNEO"].ToString();
            DateTime dataOra = this.dataPartita;
            if (reader["IDCAMPO"] != DBNull.Value)
                this.idCampo = int.Parse(reader["IDCAMPO"].ToString());
            if (reader["IDFORMAZIONECASA"] != DBNull.Value)
                this.idFormazioneCasa = long.Parse(reader["IDFORMAZIONECASA"].ToString());
            if (reader["IDFORMAZIONEOSPITI"] != DBNull.Value)
                this.idFormazioneOspiti = long.Parse(reader["IDFORMAZIONEOSPITI"].ToString());

            try
            {
                this.addData.Value = this.dataPartita;
            }
            catch (ArgumentOutOfRangeException)
            {
                ErrorDialog.Show("Attenzione", "Probabilmente a causa di altre modifiche la data precedentemente inserita non è più valida. Per risolvere, modificarla correttamente e salvare.");
            }


            //validecasa/ospiti, erroricasa/ospiti

            //Recupero le info delle due squadre (casa ...
            string selectSquadra = "SELECT * FROM SQUADRA WHERE ID = @ID";
            cmd = DbConnection.Query(selectSquadra);
            cmd.Parameters.AddWithValue("ID", this.codSquadraCasa);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                pathImgCasa = reader["LOGO"].ToString();
                this.nomeSquadraCasa = reader["NOME"].ToString();
            }
            //... e ospiti)
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("ID", this.codSquadraOspite);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                pathImgOspiti = reader["LOGO"].ToString();
                this.nomeSquadraOspiti = reader["NOME"].ToString();
            }

            //Imposto le info nella view
            this.txtDataPartita.Text = this.dataPartita.ToString("dd/MM/yyyy hh:mm");
            this.txtCampo.Text = this.nomeCampo + " di " + this.cittaCampo;

            try
            {
                this.imgCasa = Image.FromFile(pathImgCasa);
                this.imgSquadra1.Image = this.imgCasa;
            }
            catch (System.IO.FileNotFoundException) { }
            try
            {
                this.imgOspiti = Image.FromFile(pathImgOspiti);
                this.imgSquadra2.Image = this.imgOspiti;
            }
            catch (System.IO.FileNotFoundException) { }

            this.Team1.Text = this.nomeSquadraCasa;
            this.Team2.Text = this.nomeSquadraOspiti;
            this.Risultato.Text = this.nomeSquadraCasa + " " + this.punteggioCasa + ", " + this.nomeSquadraOspiti + " " + this.punteggioOspiti;


            //Ricerco e aggiorno gli arbitri
            this.lvArbitri.Items.Clear();
            string selectArbitro = "SELECT P.ID, P.COGNOME, P.NOME FROM ARBITRA AS A INNER JOIN PERSONA AS P ON A.IDPERSONA = P.ID WHERE IDPARTITA = @IDPARTITA";
            cmd = DbConnection.Query(selectArbitro);
            cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var arbitro = new Persona();
                arbitro.ID = long.Parse(reader["ID"].ToString());
                arbitro.Nome = reader["NOME"].ToString();
                arbitro.Cognome = reader["COGNOME"].ToString();
                this.arbitri.Add(arbitro);
                this.lvArbitri.Items.Add(arbitro.Cognome + " " + arbitro.Nome);
            }

            //Seleziono gli arbitri giusti nell'oggetto addArbitro
            this.addArbitro.SizeChanged += (a, b) =>
            {
                this.addArbitro.ClearSelected();
                foreach (var arbitro in this.arbitri)
                {
                    var indexToSelect = -1;
                    foreach (DataRowView arb in this.addArbitro.Items)
                    {
                        long arbId = (long)arb.Row.ItemArray[0];
                        if (arbId == arbitro.ID)
                        {
                            indexToSelect = this.addArbitro.Items.IndexOf(arb);
                        }
                    }
                    if (indexToSelect >= 0)
                        this.addArbitro.SetSelected(indexToSelect, true);
                }
            };


            //Ricerco i parziali
            string selectParziali = "SELECT INNING, PUNTICASA, PUNTIOSPITI FROM PARZIALIPUNTI WHERE IDPARTITA = @IDPARTITA";
            cmd = DbConnection.Query(selectParziali);
            cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.parzialiCasa.Add(reader["PUNTICASA"].ToString() == "" ? null : (short?)short.Parse(reader["PUNTICASA"].ToString()));
                this.parzialiOspiti.Add(short.Parse(reader["PUNTIOSPITI"].ToString()));
            }
            
            //Scrivo i parziali
            this.gridPunteggi.Rows.Clear();
            this.gridPunteggi.Columns[0].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.gridPunteggi.Columns[10].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            //ospiti
            object[]  o = new object[2 + 9];
            o[0] = this.codSquadraOspite;
            this.parzialiOspiti.ToArray().CopyTo(o, 1);
            o[o.Length - 1] = this.punteggioOspiti;
            this.gridPunteggi.Rows.Add(o);
            //casa
            o = new object[2 + 9];
            o[0] = this.codSquadraCasa;
            this.parzialiCasa.ToArray().CopyTo(o, 1);
            o[o.Length - 1] = this.punteggioCasa;
            this.gridPunteggi.Rows.Add(o);

            this.refreshGiocatori();

            this.txtValide1.Text = this.valideCasa.ToString();
            this.txtErrori1.Text = this.erroriCasa.ToString();
            this.txtValide2.Text = this.valideOspiti.ToString();
            this.txtErrori2.Text = this.erroriOspiti.ToString();

            DbConnection.Close();
            this.setMaxMinData(this.annoTorneo);
        }

        private void refreshGiocatori()
        {
            if (this.idFormazioneCasa == null || this.idFormazioneOspiti == null)
            {
                var sqlSelectIdFormazioni = "SELECT IDFORMAZIONECASA, IDFORMAZIONEOSPITI FROM PARTITA WHERE ID = @IDPARTITA";
                var cmd = DbConnection.Open(sqlSelectIdFormazioni);
                cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["IDFORMAZIONECASA"] != DBNull.Value)
                        this.idFormazioneCasa = long.Parse(reader["IDFORMAZIONECASA"].ToString());
                    if (reader["IDFORMAZIONEOSPITI"] != DBNull.Value)
                        this.idFormazioneOspiti = long.Parse(reader["IDFORMAZIONEOSPITI"].ToString());
                }
            }

            //Leggo e imposto i giocatori
            this.refreshRosterCambi(this.codSquadraCasa, this.rosterCasa, this.cambiCasa);
            this.refreshRosterCambi(this.codSquadraOspite, this.rosterOspiti, this.cambiOspiti);

            this.refreshViewGiocatori();
        }

        private void refreshRosterCambi(string codSquadra, List<object[]> roster, List<object[]> cambi)
        {
            //pulisco roster e cambi
            roster.Clear();
            cambi.Clear();

            Boolean casa = this.codSquadraCasa == codSquadra;

            if ((this.idFormazioneCasa != null && casa) || (this.idFormazioneOspiti != null && !casa))
            {
                //Recupero tutti i giocatori che hanno partecipato alla partita
                string selectGiocatori =
                        "SELECT "
                            + "P.NOME AS NOMEGIOCATORE, P.COGNOME AS COGNOMEGIOCATORE, M.NUMERO, RI.INNING, R.ID AS IDRUOLO, R.NOME AS NOMERUOLO "
                        + "FROM "
                             + "PERSONA AS P INNER JOIN "
                             + "RUOLOINNING AS RI ON P.ID = RI.IDPERSONA INNER JOIN "
                             + "RUOLO AS R ON RI.IDRUOLO = R.ID INNER JOIN "
                             + "FORMAZIONE AS F ON RI.IDFORMAZIONE = F.ID INNER JOIN "
                             + "MILITA AS M ON P.ID = M.IDPERSONA "
                        + "WHERE "
                            + (casa ? "F.ID = @IDFORMCASA" : "F.ID = @IDFORMOSPITI");
                var cmd = DbConnection.Open(selectGiocatori);
                cmd.Parameters.AddWithValue("IDFORMCASA", this.idFormazioneCasa);
                cmd.Parameters.AddWithValue("IDFORMOSPITI", this.idFormazioneOspiti);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["INNING"].ToString() == "0")
                    {
                        object[] giocatore = new object[] { reader["COGNOMEGIOCATORE"], reader["NOMEGIOCATORE"], reader["NOMERUOLO"] };
                        roster.Add(giocatore);
                    }
                    else
                    {
                        object[] giocatore = new object[] { reader["COGNOMEGIOCATORE"], reader["NOMEGIOCATORE"], reader["NOMERUOLO"], reader["INNING"] };
                        cambi.Add(giocatore);
                    }
                }

            }
        }

        private void refreshViewGiocatori()
        {
            this.gridRosterCasa.Rows.Clear();
            this.gridRosterOspiti.Rows.Clear();
            
            //Scrivo i roster nelle grid
            foreach (object[] g in this.rosterCasa)
            {   //Casa
                object[] o = new object[] { g[1] + " " + g[0], g[2] };
                this.gridRosterCasa.Rows.Add(o);
            }
            foreach (object[] g in this.rosterOspiti)
            {   //Ospiti
                object[] o = new object[] { g[1] + " " + g[0], g[2] };
                this.gridRosterOspiti.Rows.Add(o);
            }

            //Azzero i testi delle label dei cambi
            for (int i = 1; i < 10; i++)
            {
                string nomeLabel = "txtCambi1" + i;
                this.Controls.Find(nomeLabel, true)[0].Text = "";
                this.Controls.Find(nomeLabel, true)[0].Visible = false;
                this.Controls.Find(nomeLabel + "_title", true)[0].Visible = false;
            }

            //Scrivo i cambi
            this.txtCambi1_empty.Visible = this.cambiCasa.Count == 0 ? true : false;
            foreach (object[] g in this.cambiCasa)
            {   //Casa
                string toWrite = this.getStringaCambio(g);
                string inning = g[3].ToString();

                Label labToModify = (Label)this.Controls.Find("txtCambi1" + inning, true)[0];

                if (labToModify.Visible == false)
                {
                    labToModify.Visible = true;
                }

                labToModify.Text += labToModify.Text == "" ? "" : Environment.NewLine;
                labToModify.Text += toWrite;
                ((Label)this.Controls.Find("txtCambi1" + inning + "_title", true)[0]).Visible = true;
            }
            this.txtCambi2_empty.Visible = this.cambiOspiti.Count == 0 ? true : false;
            foreach (object[] g in this.cambiOspiti)
            {   //Ospiti
                string toWrite = this.getStringaCambio(g);
                string inning = g[3].ToString();

                Label labToModify = (Label)this.Controls.Find("txtCambi2" + inning, true)[0];

                if (labToModify.Visible == false)
                {
                    labToModify.Text = "";
                    labToModify.Visible = true;
                }

                labToModify.Text += toWrite;
                ((Label)this.Controls.Find("txtCambi2" + inning + "_title", true)[0]).Visible = true;
            }
        }

        private string getStringaCambio(object[] datiCambio)
        {
            string cognome = datiCambio[0].ToString();
            string nome = datiCambio[1].ToString();
            string ruolo = datiCambio[2].ToString();
            string battuta = datiCambio[3].ToString();
            string s = "";
            if (ruolo.ToUpper() == "PANCHINA")
            {
                s += "<=  esce ";
                s += cognome + " " + nome;
            }
            else
            {
                s += "=>  ";
                s += cognome + " " + nome;
                s += " nel ruolo di " + ruolo + ", ";
            }
            return s;
        }

        private void Partita_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.PERSONA'. È possibile spostarla o rimuoverla se necessario.
            this.pERSONATableAdapter.Fill(this.baseDataDataSet.PERSONA);
            if (editable)
            {
                this.fillComboBoxes();

                this.addTipologia.SelectedValue = this.codTipologia;
                //this.addArbitro.SelectedValue = this.cfArbitro;
                this.addValide1.Value = this.valideCasa;
                this.addValide2.Value = this.valideOspiti;
                this.addErrori1.Value = this.erroriCasa;
                this.addErrori2.Value = this.erroriOspiti;
            }
        }

        private void startToEdit(DataGridView grid, DataGridViewCellEventArgs e)
        {
            if (this.editable)
            {
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
                grid.BeginEdit(true);
            }
        }

        private void fillComboBoxes()
        {
            //Recupero i dati delle squadre
            string selectSquadre = "SELECT IDSQUADRA, NOME FROM SQUADRAEDIZIONE, SQUADRA WHERE SQUADRA.ID = SQUADRAEDIZIONE.IDSQUADRA AND IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE";
            var cmd = DbConnection.Open(selectSquadre);
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
            var reader = cmd.ExecuteReader();
            this.addSquadra1.Items.Clear();
            this.addSquadra2.Items.Clear();
            while (reader.Read())
            {
                Item i = new Item(reader["NOME"].ToString(), reader["IDSQUADRA"].ToString());
                this.addSquadra1.Items.Add((object)i);
                this.addSquadra2.Items.Add((object)i);
            }

            //Recupero i campi
            string selectCampi = "SELECT * FROM CAMPO";
            reader = DbConnection.Query(selectCampi).ExecuteReader();
            while (reader.Read())
            {
                string temp = reader["NOME"].ToString() + " - " + reader["CITTA"].ToString();
                Item i = new Item(temp, int.Parse(reader["ID"].ToString()));
                this.addCampo.Items.Add((object)i);
            }

            //Se siamo in modifica, imposto anche le box per gli inserimenti
            if (!this.newInsert && this.editable)
            {
                //this.addArbitro.SelectedValue = this.cfArbitro;
                this.addCampo.SelectedItem = this.nomeCampo + " - " + this.cittaCampo;
                this.addSquadra1.SelectedItem = this.nomeSquadraCasa;
                this.addSquadra2.SelectedItem = this.nomeSquadraOspiti;
                //this.addData.Value = this.dataPartita;
            }

            DbConnection.Close();

            try
            {
                // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.ARBITRO'. È possibile spostarla o rimuoverla se necessario.
                //this.aRBITROTableAdapter.Fill(this.baseDataDataSet.ARBITRO);
                // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.TIPOPARTITA'. È possibile spostarla o rimuoverla se necessario.
                this.tIPOPARTITATableAdapter.Fill(this.baseDataDataSet.TIPOPARTITA);
            }
            catch (Exception ex)
            {
                Console.Write(ex.StackTrace);
            }
        }

        private void gridPunteggi_CellSelected(object sender, DataGridViewCellEventArgs e)
        {
            this.startToEdit(this.gridPunteggi, e);
        }

        private void gridRosterCasa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.startToEdit(this.gridRosterCasa, e);
        }

        private void gridRosterOspiti_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.startToEdit(this.gridRosterOspiti, e);
        }

        private void updateOnChangeTeam(string codTeam, Boolean casa)
        {
            //Recupero la squadra
            string selectSquadra = "SELECT * FROM SQUADRA WHERE ID = @ID";
            var cmd = DbConnection.Open(selectSquadra);
            cmd.Parameters.AddWithValue("ID", codTeam);
            var reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                string cod = reader["ID"].ToString();
                string nome = reader["NOME"].ToString();
                if (casa)
                {
                    this.codSquadraCasa = cod;
                    this.nomeSquadraCasa = nome;
                    if (this.gridPunteggi.Rows.Count == 0)
                    {
                        this.gridPunteggi.Rows.Add();
                        this.gridPunteggi.Rows.Add();
                    }
                    //if (this.gridPunteggi.Rows[0].Cells.Count == 0)
                    //    this.gridPunteggi.Rows[0].Cells.Add(new DataGridViewCell());
                    this.gridPunteggi.Rows[1].Cells[0].Value = cod;
                }
                else
                {
                    if (this.gridPunteggi.Rows.Count == 0)
                    {
                        this.gridPunteggi.Rows.Add();
                        this.gridPunteggi.Rows.Add();
                    }
                    this.gridPunteggi.Rows[0].Cells[0].Value = cod;
                    this.codSquadraOspite = cod;
                    this.nomeSquadraOspiti = nome;
                }
            }
            DbConnection.Close();
        }

        private void addSquadra1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.updateOnChangeTeam(((Item)this.addSquadra1.SelectedItem).Value.ToString(), true);
        }

        private void addSquadra2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.updateOnChangeTeam(((Item)this.addSquadra2.SelectedItem).Value.ToString(), false);
        }

        private void btnSalva_Click(object sender, EventArgs e)
        {
            string errors = this.checkErrorsInForm();
            
            if (errors == "")
            {
                //Recupero i dati dalle forms
                DateTime dataPartita = this.addData.Value;
                if (this.addCampo.SelectedItem != null)
                {
                    idCampo = int.Parse((this.addCampo.SelectedItem as Item).Value.ToString());
                }
                short? punticasa;
                short? puntiospiti;
                try
                {
                    punticasa = short.Parse(this.gridPunteggi.Rows[1].Cells[10].Value.ToString());
                    puntiospiti = short.Parse(this.gridPunteggi.Rows[0].Cells[10].Value.ToString());
                }
                catch (Exception)
                {
                    punticasa = null;
                    puntiospiti = null;
                }

                //string cfArbitro = this.addArbitro.SelectedValue.ToString();
                string codSquadra1 = (this.addSquadra1.SelectedItem as Item).Value.ToString();
                string codSquadra2 = (this.addSquadra2.SelectedItem as Item).Value.ToString();
                int? codTipo = null;
                if (this.addTipologia.SelectedValue != null)
                {
                    codTipo = int.Parse(this.addTipologia.SelectedValue.ToString());
                }
                short valideCasa = Convert.ToInt16(this.addValide1.Value);
                short valideOspiti = Convert.ToInt16(this.addErrori1.Value);
                short erroriCasa = Convert.ToInt16(this.addErrori1.Value);
                short erroriOspiti = Convert.ToInt16(this.addErrori2.Value);

                string query;
                SqlCeCommand cmd;
                if (this.newInsert)
                {
                    //TODO inserire anche le due formazioni!
                    //setto la query di inserimento
                    query = "INSERT INTO PARTITA (PUNTICASA, VALIDECASA, ERRORICASA, PUNTIOSPITI, VALIDEOSPITI, ERRORIOSPITI, DATAORAPARTITA, IDCAMPO, IDTIPOLOGIA, IDSQUADRACASA, IDSQUADRAOSPITI, IDTORNEO, ANNOEDIZIONE, IDFORMAZIONECASA, IDFORMAZIONEOSPITI) "
                            + " VALUES (@PUNTICASA,@VALIDECASA,@ERRORICASA,@PUNTIOSPITI,@VALIDEOSPITI,@ERRORIOSPITI,@DATAORA,@IDCAMPO,@TIPO,@CODCASA,@CODOSPITI,@IDTORNEO,@ANNOTORNEO,NULL,NULL);";
                }
                else
                {
                    //Preparo l'eventuale query di cancellazione per le formazioni
                    string deleteFormazione = "DELETE FROM FORMAZIONE WHERE IDSQUADRA = @CODSQUADRA AND IDPARTITA = @IDPARTITA";
                    cmd = DbConnection.Open(deleteFormazione);
                    cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);

                    //Controllo se devo cancellare una qualche formazione vecchia
                    if (this.codSquadraCasa != codSquadra1)
                    {
                        cmd.Parameters.AddWithValue("CODSQUADRA", this.codSquadraCasa);
                        cmd.ExecuteNonQuery();
                    }
                    if (this.codSquadraOspite != codSquadra2)
                    {
                        cmd.Parameters.AddWithValue("CODSQUADRA", this.codSquadraOspite);
                        cmd.ExecuteNonQuery();
                    }

                    if ((codTipo == 1 && checkFinaleExists()) || (codTipo == 2 && checkFinalinaExists()))
                    {
                        ErrorDialog.Show("Errore nel salvataggio", "Impossibile inserire una seconda partita della tipologia selezionata.");
                        return;
                    }

                    //setto la query di update
                    query = "UPDATE PARTITA "
                            + "SET DATAORAPARTITA = @DATAORA, IDCAMPO = @IDCAMPO, IDTIPOLOGIA = @TIPO, PUNTICASA = @PUNTICASA, PUNTIOSPITI = @PUNTIOSPITI, "
                            + "VALIDECASA = @VALIDECASA, VALIDEOSPITI = @VALIDEOSPITI, ERRORICASA = @ERRORICASA, ERRORIOSPITI = @ERRORIOSPITI, "
                            + "IDSQUADRACASA = @CODCASA, IDSQUADRAOSPITI = @CODOSPITI "
                            + "WHERE ID = @IDPARTITA";
                }
                cmd = DbConnection.Open(query);
                Item[] parameters = new Item[] { new Item("DATAORA", dataPartita), new Item("IDCAMPO", idCampo), new Item("TIPO", codTipo), new Item("PUNTICASA", punticasa),
                            new Item("PUNTIOSPITI", puntiospiti), new Item("VALIDECASA", valideCasa), new Item("VALIDEOSPITI", valideOspiti),
                            new Item("ERRORICASA", erroriCasa), new Item("ERRORIOSPITI", erroriOspiti)/*, new Item("CFARBITRO", cfArbitro)*/, new Item("CODCASA", codSquadra1), new Item("CODOSPITI", codSquadra2),
                            new Item("IDPARTITA", this.idPartita),new Item("IDTORNEO", this.idTorneo),new Item("ANNOTORNEO", this.annoTorneo) };
                foreach (Item i in parameters)
                    cmd.Parameters.AddWithValue(i.Text, i.Value == null ? DBNull.Value : i.Value);

                if (cmd.ExecuteNonQuery() != 1)
                {
                    ErrorDialog.Show("Errore nel salvataggio", DbConnection.GENERAL_ERROR_SQL);
                    return;
                }

                if (this.newInsert)
                {
                    cmd.CommandText = "SELECT MAX(ID) FROM PARTITA;";
                    this.idPartita = (long)cmd.ExecuteScalar();
                } 


                /**** Salvataggio dei punteggi parziali ****/
                //Recupero i parziali
                List<object> parzialicasa = Utilities.getDataGridRowValues(this.gridPunteggi.Rows[1], 1, 9).ToList();
                List<object> parzialiospiti = Utilities.getDataGridRowValues(this.gridPunteggi.Rows[0], 1, 9).ToList();

                for (int inning = 9; inning >= 1; inning--)
                {
                    if (parzialicasa[inning - 1] == null && parzialiospiti[inning - 1] == null)
                    {
                        parzialiospiti.RemoveAt(inning - 1);
                        parzialicasa.RemoveAt(inning - 1);
                    }
                    else if (parzialicasa[inning - 1] == null)
                    {
                        parzialicasa.RemoveAt(inning - 1);
                        break;
                    }
                    else
                    {
                        break;
                    }
                }

                //Cancello tutti i vecchi parziali
                if (!this.newInsert)
                {
                    var queryClearParziali = "DELETE FROM PARZIALIPUNTI WHERE IDPARTITA = @IDPARTITA";
                    cmd = DbConnection.Open(queryClearParziali);
                    cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                    cmd.ExecuteNonQuery();
                }

                //E li reinserisco
                var queryInsertParziale = "INSERT INTO PARZIALIPUNTI VALUES (@IDPARTITA, @INNING, @PARZCASA, @PARZOSPITI)";
                cmd = DbConnection.Open(queryInsertParziale);

                for (int inning = 1; inning <= parzialiospiti.Count; inning++)
                {
                    short parzOspiti = parzialiospiti[inning - 1] != null ? short.Parse(parzialiospiti[inning - 1].ToString()) : (short)0;
                    short? parzCasa;
                    if (inning == parzialiospiti.Count && parzialicasa.Count < parzialiospiti.Count)
                        parzCasa = null;
                    else
                        parzCasa = parzialicasa[inning - 1] != null ? short.Parse(parzialicasa[inning - 1].ToString()) : (short)0;

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                    cmd.Parameters.AddWithValue("INNING", inning);
                    cmd.Parameters.AddWithValue("PARZCASA", parzCasa);
                    cmd.Parameters.AddWithValue("PARZOSPITI", parzOspiti);
                    if (parzCasa == null)
                        cmd.Parameters["PARZCASA"].Value = DBNull.Value;
                    cmd.ExecuteNonQuery();
                }

                /**** Salvataggio degli arbitri ****/
                //Recupero gli arbitri
                ListBox.SelectedObjectCollection arbitri = this.addArbitro.SelectedItems;

                //Cancello tutti i vecchi arbitri
                var queryClearArbitri = "DELETE FROM ARBITRA WHERE IDPARTITA = @IDPARTITA";
                cmd = DbConnection.Open(queryClearArbitri);
                cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                cmd.ExecuteNonQuery();

                //E li reinserisco
                var queryInsertArbitra = "INSERT INTO ARBITRA VALUES (@IDPARTITA, @IDPERSONA)";
                cmd = DbConnection.Open(queryInsertArbitra);

                foreach (DataRowView arbitro in arbitri)
                {
                    var idArbitro = arbitro.Row.ItemArray[0];
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                    cmd.Parameters.AddWithValue("IDPERSONA", idArbitro);
                    cmd.ExecuteNonQuery();
                }

                DbConnection.Close();
                MessageBox.Show("Modifiche salvate con successo!", "Successo!", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            else
            {
                ErrorDialog.Show("Impossibile salvare", "Impossibile salvare: " + Environment.NewLine + errors);
            }
        }

        //Ritorna una stringa di errori, ritorna "" se non ce ne sono
        private string checkErrorsInForm()
        {
            string errors = "";
            

            //Controllo che le squadre siano diverse
            if (this.addSquadra1.Text == "" || this.addSquadra2.Text == "" || this.addSquadra1.Text == this.addSquadra2.Text)
                errors += "- Le due squadre sono obbligatorie e non possono essere uguali" + Environment.NewLine;

            //Controllo la somma dei parziali
            for (int i = 0; i < 2; i++)
            {
                int somma = 0;
                int numDiParziali = 0;
                foreach (object value in Utilities.getDataGridRowValues(this.gridPunteggi.Rows[i], 1, 9))
                {
                    try {
                        string stringValue = value.ToString();
                        var parziale = int.Parse(stringValue.Trim());
                        if (parziale < 0) 
                            throw new ArgumentException("Valore negativo in un punteggio parziale.");
                        somma += parziale;
                        numDiParziali++;
                    }
                    catch (Exception) {
                        if (value != null) {
                            errors += "- I punteggi parziali possono contenere solo numeri" + Environment.NewLine;
                            break;
                        }
                    }
                }
                var temp = this.gridPunteggi.Rows[i].Cells[10].Value;
                var tot = temp != null ? int.Parse(temp.ToString()) : 0;
                if (numDiParziali > 0 && somma != tot)
                {
                    errors += "- La somma (" + somma + ") dei punteggi parziali non corrisponde al totale (" + tot + ")" + Environment.NewLine;
                }
            }

            return errors;
        }

        private static int ConvertObjectToInt(object obj)
        {
            if (obj == null) throw new ArgumentNullException();
            return int.Parse(obj.ToString());
        }

        private void btnModifica1_Click(object sender, EventArgs e)
        {
            if (this.idPartita != null)
            {
                new InserisciFormazione(this.idTorneo, this.annoTorneo, this.codSquadraCasa, this.idPartita.GetValueOrDefault(), this.idFormazioneCasa).ShowDialog();
                this.refreshGiocatori();
            }
        }

        private void btnModifica2_Click(object sender, EventArgs e)
        {
            if (this.idPartita != null)
            {
                new InserisciFormazione(this.idTorneo, this.annoTorneo, this.codSquadraOspite, this.idPartita.GetValueOrDefault(), this.idFormazioneOspiti).ShowDialog();
                this.refreshGiocatori();
            }
        }

        private void btnModifica1_cambi_Click(object sender, EventArgs e)
        {
            if (this.idPartita != null)
            {
                new InserisciFormazione(this.idTorneo, this.annoTorneo, this.codSquadraCasa, this.idPartita.GetValueOrDefault(), this.idFormazioneCasa).ShowDialog();
                this.refreshGiocatori();
            }
        }

        private void btnModifica2_cambi_Click(object sender, EventArgs e)
        {
            if (this.idPartita != null)
            {
                new InserisciFormazione(this.idTorneo, this.annoTorneo, this.codSquadraOspite, this.idPartita.GetValueOrDefault(), this.idFormazioneOspiti).ShowDialog();
                this.refreshGiocatori();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Team1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!this.editable)
                new Squadra(this.codSquadraCasa).Show();
        }

        private void Team2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!this.editable)
                new Squadra(this.codSquadraOspite).Show();
        }

        private Boolean checkFinaleExists()
        {
            var sqlFinale = "SELECT ID FROM PARTITA WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDTIPOLOGIA = 1";
            var cmd = DbConnection.Open(sqlFinale);
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                return reader["ID"].ToString() != this.idPartita.ToString();
            }
            return false;
        }

        private Boolean checkFinalinaExists()
        {
            var sqlFinale = "SELECT ID FROM PARTITA WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDTIPOLOGIA = 2";
            var cmd = DbConnection.Open(sqlFinale);
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                return reader["ID"].ToString() != this.idPartita.ToString();
            }
            return false;
        }
    }
}