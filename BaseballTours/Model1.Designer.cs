﻿//------------------------------------------------------------------------------
// <auto-generated>
//    Codice generato da un modello.
//
//    Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//    Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace BaseballTours
{
    #region Contesti
    
    /// <summary>
    /// Nessuna documentazione sui metadati disponibile.
    /// </summary>
    public partial class Model1Container : ObjectContext
    {
        #region Costruttori
    
        /// <summary>
        /// Inizializza un nuovo oggetto Model1Container utilizzando la stringa di connessione trovata nella sezione 'Model1Container' del file di configurazione dell'applicazione.
        /// </summary>
        public Model1Container() : base("name=Model1Container", "Model1Container")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Inizializzare un nuovo oggetto Model1Container.
        /// </summary>
        public Model1Container(string connectionString) : base(connectionString, "Model1Container")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Inizializzare un nuovo oggetto Model1Container.
        /// </summary>
        public Model1Container(EntityConnection connection) : base(connection, "Model1Container")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Metodi parziali
    
        partial void OnContextCreated();
    
        #endregion
    
    }

    #endregion

    
}
