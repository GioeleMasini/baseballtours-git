﻿namespace BaseballTours
{
    partial class InserisciSquadra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciSquadra));
            this.GoBack = new System.Windows.Forms.Button();
            this.Resetta = new System.Windows.Forms.Button();
            this.Salva = new System.Windows.Forms.Button();
            this.cittàSquadra = new System.Windows.Forms.TextBox();
            this.nomeSquadra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.caricaLogo = new System.Windows.Forms.Button();
            this.scegliColore = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.idSquadra = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.squadraLogo = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squadraLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // GoBack
            // 
            this.GoBack.Location = new System.Drawing.Point(42, 264);
            this.GoBack.Margin = new System.Windows.Forms.Padding(2);
            this.GoBack.Name = "GoBack";
            this.GoBack.Size = new System.Drawing.Size(102, 40);
            this.GoBack.TabIndex = 8;
            this.GoBack.Text = "Annulla";
            this.GoBack.UseVisualStyleBackColor = true;
            this.GoBack.Click += new System.EventHandler(this.GoBack_Click);
            // 
            // Resetta
            // 
            this.Resetta.Location = new System.Drawing.Point(204, 264);
            this.Resetta.Margin = new System.Windows.Forms.Padding(2);
            this.Resetta.Name = "Resetta";
            this.Resetta.Size = new System.Drawing.Size(102, 40);
            this.Resetta.TabIndex = 7;
            this.Resetta.Text = "Resetta campi";
            this.Resetta.UseVisualStyleBackColor = true;
            // 
            // Salva
            // 
            this.Salva.Location = new System.Drawing.Point(359, 264);
            this.Salva.Margin = new System.Windows.Forms.Padding(2);
            this.Salva.Name = "Salva";
            this.Salva.Size = new System.Drawing.Size(102, 40);
            this.Salva.TabIndex = 6;
            this.Salva.Text = "Salva";
            this.Salva.UseVisualStyleBackColor = true;
            this.Salva.Click += new System.EventHandler(this.Salva_Click);
            // 
            // cittàSquadra
            // 
            this.cittàSquadra.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cittàSquadra.Location = new System.Drawing.Point(123, 74);
            this.cittàSquadra.Margin = new System.Windows.Forms.Padding(2);
            this.cittàSquadra.Name = "cittàSquadra";
            this.cittàSquadra.Size = new System.Drawing.Size(217, 27);
            this.cittàSquadra.TabIndex = 4;
            // 
            // nomeSquadra
            // 
            this.nomeSquadra.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeSquadra.Location = new System.Drawing.Point(123, 39);
            this.nomeSquadra.Margin = new System.Windows.Forms.Padding(2);
            this.nomeSquadra.Name = "nomeSquadra";
            this.nomeSquadra.Size = new System.Drawing.Size(217, 27);
            this.nomeSquadra.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Colore maglia*";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 78);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Città squadra*";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(490, 49);
            this.label1.TabIndex = 20;
            this.label1.Text = "Inserisci una nuova squadra";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.caricaLogo, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.scegliColore, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cittàSquadra, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.nomeSquadra, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.idSquadra, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 60);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(347, 179);
            this.tableLayoutPanel1.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 150);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "Logo squadra";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 35);
            this.label5.TabIndex = 10;
            this.label5.Text = "Nome squadra*";
            // 
            // caricaLogo
            // 
            this.caricaLogo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.caricaLogo.Location = new System.Drawing.Point(123, 144);
            this.caricaLogo.Margin = new System.Windows.Forms.Padding(2);
            this.caricaLogo.Name = "caricaLogo";
            this.caricaLogo.Size = new System.Drawing.Size(163, 30);
            this.caricaLogo.TabIndex = 9;
            this.caricaLogo.Text = "Carica logo squadra";
            this.caricaLogo.UseVisualStyleBackColor = true;
            this.caricaLogo.Click += new System.EventHandler(this.caricaLogo_Click);
            // 
            // scegliColore
            // 
            this.scegliColore.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.scegliColore.Location = new System.Drawing.Point(123, 107);
            this.scegliColore.Margin = new System.Windows.Forms.Padding(2);
            this.scegliColore.Name = "scegliColore";
            this.scegliColore.Size = new System.Drawing.Size(163, 30);
            this.scegliColore.TabIndex = 8;
            this.scegliColore.Text = "Scegli colore squadra";
            this.scegliColore.UseVisualStyleBackColor = true;
            this.scegliColore.Click += new System.EventHandler(this.scegliColore_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 19);
            this.label6.TabIndex = 11;
            this.label6.Text = "ID*";
            // 
            // idSquadra
            // 
            this.idSquadra.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.idSquadra.Location = new System.Drawing.Point(123, 4);
            this.idSquadra.Margin = new System.Windows.Forms.Padding(2);
            this.idSquadra.MaxLength = 4;
            this.idSquadra.Name = "idSquadra";
            this.idSquadra.Size = new System.Drawing.Size(89, 27);
            this.idSquadra.TabIndex = 2;
            this.idSquadra.Enter += new System.EventHandler(this.idSquadra_Enter);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "File JPEG (*.jpg)|*.jpg|File PNG (*.png)|*.png|File BMP (*.bmp)|*.bmp|Tutti i fil" +
                "e (*.*)|*.*";
            this.openFileDialog1.Title = "Selezionare un file immagine";
            // 
            // squadraLogo
            // 
            this.squadraLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.squadraLogo.Image = ((System.Drawing.Image)(resources.GetObject("squadraLogo.Image")));
            this.squadraLogo.InitialImage = null;
            this.squadraLogo.Location = new System.Drawing.Point(367, 85);
            this.squadraLogo.Margin = new System.Windows.Forms.Padding(2);
            this.squadraLogo.Name = "squadraLogo";
            this.squadraLogo.Size = new System.Drawing.Size(134, 114);
            this.squadraLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.squadraLogo.TabIndex = 22;
            this.squadraLogo.TabStop = false;
            // 
            // InserisciSquadra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 316);
            this.Controls.Add(this.squadraLogo);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.GoBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Resetta);
            this.Controls.Add(this.Salva);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "InserisciSquadra";
            this.Text = "Baseball Tournaments - Inserisci squadra";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squadraLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Resetta;
        private System.Windows.Forms.Button Salva;
        private System.Windows.Forms.TextBox cittàSquadra;
        private System.Windows.Forms.TextBox nomeSquadra;
        private System.Windows.Forms.Button GoBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button caricaLogo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox squadraLogo;
        private System.Windows.Forms.Button scegliColore;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idSquadra;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}