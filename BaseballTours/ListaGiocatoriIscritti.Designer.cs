﻿namespace BaseballTours
{
    partial class ListaGiocatoriIscritti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaGiocatoriIscritti));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sQUADRABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.pERSONABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pERSONATableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.PERSONATableAdapter();
            this.sQUADRATableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.SQUADRATableAdapter();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOGNOMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbSquadraIscritto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.txtNumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATANASCITADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIZIONEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERSONABindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nOMEDataGridViewTextBoxColumn,
            this.cOGNOMEDataGridViewTextBoxColumn,
            this.cbSquadraIscritto,
            this.txtNumero,
            this.dATANASCITADataGridViewTextBoxColumn,
            this.dESCRIZIONEDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.pERSONABindingSource;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(2, 2);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(796, 287);
            this.dataGridView1.TabIndex = 0;
            // 
            // sQUADRABindingSource
            // 
            this.sQUADRABindingSource.DataMember = "SQUADRA";
            this.sQUADRABindingSource.DataSource = this.baseDataDataSet;
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pERSONABindingSource
            // 
            this.pERSONABindingSource.DataMember = "PERSONA";
            this.pERSONABindingSource.DataSource = this.baseDataDataSet;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(692, 308);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "Salva";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 308);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 39);
            this.button2.TabIndex = 4;
            this.button2.Text = "Indietro";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pERSONATableAdapter
            // 
            this.pERSONATableAdapter.ClearBeforeFill = true;
            // 
            // sQUADRATableAdapter
            // 
            this.sQUADRATableAdapter.ClearBeforeFill = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // nOMEDataGridViewTextBoxColumn
            // 
            this.nOMEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nOMEDataGridViewTextBoxColumn.DataPropertyName = "NOME";
            this.nOMEDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nOMEDataGridViewTextBoxColumn.Name = "nOMEDataGridViewTextBoxColumn";
            this.nOMEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMEDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cOGNOMEDataGridViewTextBoxColumn
            // 
            this.cOGNOMEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cOGNOMEDataGridViewTextBoxColumn.DataPropertyName = "COGNOME";
            this.cOGNOMEDataGridViewTextBoxColumn.HeaderText = "Cognome";
            this.cOGNOMEDataGridViewTextBoxColumn.Name = "cOGNOMEDataGridViewTextBoxColumn";
            this.cOGNOMEDataGridViewTextBoxColumn.ReadOnly = true;
            this.cOGNOMEDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cbSquadraIscritto
            // 
            this.cbSquadraIscritto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cbSquadraIscritto.HeaderText = "Squadra";
            this.cbSquadraIscritto.MaxDropDownItems = 16;
            this.cbSquadraIscritto.Name = "cbSquadraIscritto";
            this.cbSquadraIscritto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cbSquadraIscritto.ToolTipText = "Squadra di iscrizione in questo torneo";
            // 
            // txtNumero
            // 
            this.txtNumero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtNumero.HeaderText = "N° Maglia";
            this.txtNumero.MaxInputLength = 2;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dATANASCITADataGridViewTextBoxColumn
            // 
            this.dATANASCITADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dATANASCITADataGridViewTextBoxColumn.DataPropertyName = "DATANASCITA";
            this.dATANASCITADataGridViewTextBoxColumn.HeaderText = "Data Nascita";
            this.dATANASCITADataGridViewTextBoxColumn.Name = "dATANASCITADataGridViewTextBoxColumn";
            this.dATANASCITADataGridViewTextBoxColumn.ReadOnly = true;
            this.dATANASCITADataGridViewTextBoxColumn.Width = 113;
            // 
            // dESCRIZIONEDataGridViewTextBoxColumn
            // 
            this.dESCRIZIONEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dESCRIZIONEDataGridViewTextBoxColumn.DataPropertyName = "DESCRIZIONE";
            this.dESCRIZIONEDataGridViewTextBoxColumn.HeaderText = "Descrizione";
            this.dESCRIZIONEDataGridViewTextBoxColumn.Name = "dESCRIZIONEDataGridViewTextBoxColumn";
            this.dESCRIZIONEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ListaGiocatoriIscritti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(800, 358);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "ListaGiocatoriIscritti";
            this.Text = "Baseball Tournaments - Persone";
            this.Load += new System.EventHandler(this.ListaVolontari_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERSONABindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource pERSONABindingSource;
        private BaseDataDataSetTableAdapters.PERSONATableAdapter pERSONATableAdapter;
        private System.Windows.Forms.BindingSource sQUADRABindingSource;
        private BaseDataDataSetTableAdapters.SQUADRATableAdapter sQUADRATableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOGNOMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbSquadraIscritto;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATANASCITADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIZIONEDataGridViewTextBoxColumn;
    }
}