﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;

namespace BaseballTours
{
    class DbConnection
    {
        public static string GENERAL_ERROR_SQL = "Errore nell'inserimento dei dati. Controlla di aver inserito tutti i dati correttamente.";
        public static string DELETE_ERROR = "Errore nella cancellazione della tupla, fatto inspiegabile.";
        public static string UNDELETABLE = "Valore utilizzato all'interno di altre tabelle, impossibile eliminare.";
        //static string CONNECTION_STRING = @"Data Source=|DataDirectory|\BaseData.sdf;Persist Security Info=False; password=BTmas&pru2014";
        static string CONNECTION_STRING = @"Data Source=|DataDirectory|\BaseData.sdf;Persist Security Info=False";
        static SqlCeConnection cn = null;

        public static SqlCeCommand Open(string queryString) {
            if (cn == null) {
                cn = new SqlCeConnection(CONNECTION_STRING);
                cn.Open();
            }
            return Query(queryString);
        }

        public static SqlCeCommand Query(string queryString)
        {
            if (queryString != null && cn != null)
            {
                var cmd = new SqlCeCommand(queryString, cn);
                return cmd;
            }
            return null;
        }

        public static void Close() {
            if (cn != null)
            {
                cn.Close();
                cn = null;
            }
        }

        /******* QUERIES *******/
        public static string retrieveClassifica(int codTorneo, string annoTorneo) {
            string classifica = "";

            //Recupero le date di inizio e fine torneo
            string selectDate = "SELECT MIN(DATAORAPARTITA) AS DATAINIZIO, MAX(DATAORAPARTITA) AS DATAFINE FROM PARTITA WHERE IDTORNEO = @NOMETORNEO AND ANNOEDIZIONE = @ANNOTORNEO";
            var cmd = DbConnection.Open(selectDate);
            cmd.Parameters.AddWithValue("NOMETORNEO", codTorneo);
            cmd.Parameters.AddWithValue("ANNOTORNEO", annoTorneo);
            var reader = cmd.ExecuteReader();

            if (!reader.Read())
            {
                DbConnection.Close();
                return "Errore nella lettura del torneo.";
            }
            else if (reader.IsDBNull(0))
            {
                DbConnection.Close();
                return "Nessuna partita ancora giocata";            
            }

            DateTime dataInizio = reader.GetDateTime(0);
            DateTime dataFine = reader.GetDateTime(1);

            string selectClassifica = "SELECT S1.NOME AS NOMECASA, S2.NOME AS NOMEOSPITI, PUNTICASA, PUNTIOSPITI, IDTIPOLOGIA " 
                + "FROM PARTITA P, SQUADRA S1, SQUADRA S2 " 
                + "WHERE IDSQUADRACASA = S1.ID AND IDSQUADRAOSPITI = S2.ID AND P.IDTORNEO = @NOMETORNEO AND P.ANNOEDIZIONE = @ANNOTORNEO AND (IDTIPOLOGIA = 0 OR IDTIPOLOGIA = 1) " 
                + "ORDER BY IDTIPOLOGIA";
            cmd.CommandText = selectClassifica;
            reader = cmd.ExecuteReader();

            if (reader.Read() && (!reader.IsDBNull(2) || !reader.IsDBNull(3)))
            {
                do
                {
                    int i = int.Parse(reader["IDTIPOLOGIA"].ToString()) * 2 - 1;

                    if (reader.GetInt16(2) > reader.GetInt16(3))
                    {
                        classifica += i + ". " + reader["NOMECASA"] + Environment.NewLine;
                        i++;
                        classifica += i + ". " + reader["NOMEOSPITI"] + Environment.NewLine;
                    }
                    else
                    {
                        classifica += i + ". " + reader["NOMEOSPITI"] + Environment.NewLine;
                        i++;
                        classifica += i + ". " + reader["NOMECASA"] + Environment.NewLine;
                    }
                } while (reader.Read() && (!reader.IsDBNull(2) || !reader.IsDBNull(3)));
            }
            else
            {
                if (dataInizio > DateTime.Now)
                    classifica = "Torneo non ancora iniziato.";
                else if (DateTime.Now >= dataInizio && DateTime.Now <= dataFine)
                    classifica = "Torneo in corso.";
                else
                    classifica = "Torneo concluso." + Environment.NewLine + Environment.NewLine
                        + "Impossibile determinare la classifica. "
                        + "Assicurarsi di aver inserito almeno la finale.";
            }

            DbConnection.Close();

            return classifica;
        }

        /*
         * Controlla se è già occupata quella persona in quel periodo
         */
        public static bool occupato(string orario, string persona)
        {
            bool esito = false;

            string controlla = "SELECT * FROM TURNO WHERE IDPERSONA = @ID AND @ORARIO BETWEEN DATAORAINIZIO AND DATAORAFINE";

            var cmd = DbConnection.Open(controlla);
            cmd.CommandText = controlla;
            cmd.Parameters.AddWithValue("ID", persona);
            cmd.Parameters.AddWithValue("ORARIO", orario);

            var reader = cmd.ExecuteReader();

            if (reader.Read() && !reader.IsDBNull(0))
                esito = true;

            DbConnection.Close();

            return esito;
        }

        /*
         * Controllo se la squadra che si vuole eliminare è organizzatrice di qualche torneo
         */
        public static bool organizza(string idSquadra)
        {
            int count = 0;

            string sql = "SELECT COUNT(*) AS TORNEI FROM TORNEO WHERE IDSQUADRAORGANIZZATRICE = @ID";

            var cmd = DbConnection.Open(sql);
            cmd.Parameters.AddWithValue("ID", idSquadra);

            var reader = cmd.ExecuteReader();

            if (reader.Read() && !reader.IsDBNull(0))
                count = reader.GetInt32(0);

            DbConnection.Close();

            return count > 0;
        }

        /*
         * Controllo se la squadra che si vuole eliminare partecipa come ospite in qualche partita
         */
        public static bool giocaOspite(string idSquadra)
        {
            int count = 0;

            string sql = "SELECT COUNT(*) FROM PARTITA WHERE IDSQUADRAOSPITI = @ID";

            var cmd = DbConnection.Open(sql);
            cmd.Parameters.AddWithValue("ID", idSquadra);

            var reader = cmd.ExecuteReader();

            if (reader.Read() && !reader.IsDBNull(0))
                count = reader.GetInt32(0);

            return count > 0;
        }
    }
}
