﻿namespace BaseballTours
{
    partial class InserisciPersona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciPersona));
            this.Salva = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.GoBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.nomePersona = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cognomePersona = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataPersona = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.descrizionePersona = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Salva
            // 
            this.Salva.Location = new System.Drawing.Point(268, 306);
            this.Salva.Margin = new System.Windows.Forms.Padding(2);
            this.Salva.Name = "Salva";
            this.Salva.Size = new System.Drawing.Size(87, 40);
            this.Salva.TabIndex = 5;
            this.Salva.Text = "Salva";
            this.Salva.UseVisualStyleBackColor = true;
            this.Salva.Click += new System.EventHandler(this.Salva_Click);
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(142, 306);
            this.Reset.Margin = new System.Windows.Forms.Padding(2);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(87, 40);
            this.Reset.TabIndex = 6;
            this.Reset.Text = "Resetta campi";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // GoBack
            // 
            this.GoBack.Location = new System.Drawing.Point(16, 306);
            this.GoBack.Margin = new System.Windows.Forms.Padding(2);
            this.GoBack.Name = "GoBack";
            this.GoBack.Size = new System.Drawing.Size(87, 40);
            this.GoBack.TabIndex = 7;
            this.GoBack.Text = "Indietro";
            this.GoBack.UseVisualStyleBackColor = true;
            this.GoBack.Click += new System.EventHandler(this.GoBack_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 52);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserisci nuova persona";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.67277F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.32723F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.nomePersona, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cognomePersona, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dataPersona, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.descrizionePersona, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 71);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(362, 215);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nome";
            // 
            // nomePersona
            // 
            this.nomePersona.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomePersona.Location = new System.Drawing.Point(141, 6);
            this.nomePersona.Margin = new System.Windows.Forms.Padding(2);
            this.nomePersona.Name = "nomePersona";
            this.nomePersona.Size = new System.Drawing.Size(219, 27);
            this.nomePersona.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cognome";
            // 
            // cognomePersona
            // 
            this.cognomePersona.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cognomePersona.Location = new System.Drawing.Point(141, 45);
            this.cognomePersona.Margin = new System.Windows.Forms.Padding(2);
            this.cognomePersona.Name = "cognomePersona";
            this.cognomePersona.Size = new System.Drawing.Size(219, 27);
            this.cognomePersona.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 88);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "Data Nascita";
            // 
            // dataPersona
            // 
            this.dataPersona.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataPersona.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataPersona.Location = new System.Drawing.Point(141, 84);
            this.dataPersona.Margin = new System.Windows.Forms.Padding(2);
            this.dataPersona.Name = "dataPersona";
            this.dataPersona.Size = new System.Drawing.Size(219, 27);
            this.dataPersona.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Descrizione";
            // 
            // descrizionePersona
            // 
            this.descrizionePersona.Location = new System.Drawing.Point(142, 120);
            this.descrizionePersona.Multiline = true;
            this.descrizionePersona.Name = "descrizionePersona";
            this.descrizionePersona.Size = new System.Drawing.Size(217, 92);
            this.descrizionePersona.TabIndex = 4;
            // 
            // InserisciPersona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 378);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GoBack);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.Salva);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "InserisciPersona";
            this.Text = "Baseball Tournaments - Inserisci persona";
            this.Load += new System.EventHandler(this.InserisciPersona_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Salva;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button GoBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox cognomePersona;
        private System.Windows.Forms.TextBox nomePersona;
        private System.Windows.Forms.DateTimePicker dataPersona;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox descrizionePersona;
    }
}