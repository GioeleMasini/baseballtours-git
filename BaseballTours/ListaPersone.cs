﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaPersone : Form
    {
        public ListaPersone()
        {
            InitializeComponent();
        }

        private void ListaVolontari_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.PERSONA'. È possibile spostarla o rimuoverla se necessario.
            this.pERSONATableAdapter.Fill(this.baseDataDataSet.PERSONA);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridRefresh()
        {
            this.pERSONATableAdapter.Fill(this.baseDataDataSet.PERSONA);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            new InserisciPersona((long)dataGridView1.CurrentRow.Cells[0].Value).ShowDialog();
            gridRefresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new InserisciPersona().ShowDialog();
            gridRefresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new InserisciPersona((long)dataGridView1.CurrentRow.Cells[0].Value).ShowDialog();
            gridRefresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "DELETE FROM PERSONA WHERE ID = @PERSONA";

                var cmd = DbConnection.Open(sql);

                cmd.CommandText = sql;

                cmd.Parameters.AddWithValue("PERSONA", dataGridView1.SelectedRows[0].Cells[0].Value);

                cmd.ExecuteNonQuery();

                DbConnection.Close();

                gridRefresh();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
           
            ///// codice vecchio /////
            
            
            /*string search = "SELECT COUNT(*) FROM TURNO WHERE "
                +"VOLONTARIO = @CODICE";

            var cmd = DbConnection.Open(search);
            cmd.CommandText = search;
            cmd.Parameters.AddWithValue("CODICE", Convert.ToString(dataGridView1.SelectedRows[0].Cells[2].Value));
            var reader = cmd.ExecuteReader();
            reader.Read();

            if (reader.GetInt32(0) > 0)
            {
                //ERROR
                ErrorDialog.Show("Errore", DbConnection.UNDELETABLE);
                return;
            }
            else
            {
                string deleteVolontario = "DELETE FROM VOLONTARIO WHERE CODICEFISCALE = @CODICE";

                cmd = DbConnection.Query(deleteVolontario);
                cmd.CommandText = deleteVolontario;
                cmd.Parameters.AddWithValue("CODICE", Convert.ToString(dataGridView1.SelectedRows[0].Cells[2].Value));

                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                }
            }

            cmd.Parameters.Clear();
            DbConnection.Close();

            gridRefresh();*/
        }
    }
}
