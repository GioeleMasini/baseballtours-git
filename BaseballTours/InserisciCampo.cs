﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class InserisciCampo : Form
    {
        Boolean editable = false;
        String insertCampo = "";
        int id;
        string nome;
        string città;
        string via;
        string civico;
        System.Data.SqlServerCe.SqlCeCommand cmd;
        System.Data.SqlServerCe.SqlCeDataReader reader;

        public InserisciCampo()
        {
            InitializeComponent();
        }

        public InserisciCampo(int idToEdit)
        {

            InitializeComponent();

            string selection = "SELECT NOME, CITTA, VIA, CIVICO FROM CAMPO WHERE ID = @ID";
            
            cmd = DbConnection.Open(selection);
            cmd.Parameters.AddWithValue("ID", idToEdit);
            reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                id = idToEdit;
                nomeCampo.Text = reader[0].ToString();
                nomeCittà.Text = reader[1].ToString();
                viaCampo.Text = reader[2].ToString();
                civicoCampo.Text = reader[3].ToString();
            }

            cmd.Parameters.Clear();
            DbConnection.Close();

            editable = true;
            label3.Text = "Modifica campo: " + nomeCampo.Text;
        }

        private void GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Salva_Click(object sender, EventArgs e)
        {
            // Controllo presenza valori campi
            if (nomeCampo.Text.Equals("") || nomeCittà.Text.Equals("") 
                || viaCampo.Text.Equals("") || civicoCampo.Text.Equals(""))
            {
                ErrorDialog.Show("Errore", "Inserire tutti i campi");
                return;
            }

            nome = nomeCampo.Text;
            città = nomeCittà.Text;
            via = viaCampo.Text;
            civico = civicoCampo.Text;

            if (!editable)
            {
                // Controllo presenza di tuple con stesso valore di nome e città
                string controllaDoppio = "SELECT * FROM CAMPO WHERE NOME = @NOME AND "
                    + "CITTA = @CITTA";
                cmd = DbConnection.Open(controllaDoppio);

                cmd.CommandText = controllaDoppio;
                cmd.Parameters.AddWithValue("NOME", nome);
                cmd.Parameters.AddWithValue("CITTA", città);
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    // Errore, tupla con valore di città e nome già presente
                    ErrorDialog.Show("Errore di chiave", "Nel DB è già presente un campo con questi valori di chiave.\n"
                        + "Inserire un campo con valori di chiave validi");
                    return;
                }
                else
                {
                    // Nessuna tupla con quel nome in quella città, può continuare, ripulisco il cmd.
                    cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }
            else
            {
                string controllaDoppio = "SELECT * FROM CAMPO WHERE NOME = @NOME AND CITTA = @CITTA "
                    + "AND VIA = @VIA AND CIVICO = @CIVICO AND ID <> @ID";

                cmd = DbConnection.Open(controllaDoppio);

                cmd.CommandText = controllaDoppio;

                cmd.Parameters.AddWithValue("NOME", nome);
                cmd.Parameters.AddWithValue("CITTA", città);
                cmd.Parameters.AddWithValue("VIA", via);
                cmd.Parameters.AddWithValue("CIVICO", civico);
                cmd.Parameters.AddWithValue("ID", id);
                reader = cmd.ExecuteReader();
                
                if (reader.Read())
                {
                    // Errore, tupla con stessi valori già presente
                    ErrorDialog.Show("Errore di duplicazione", "Nel DB è già presente una tupla con questi valori.\n"
                        + "Inserire dei valori validi.");
                    return;
                }
                else
                {
                    // Nessuna tupla con quei valori
                    cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }

            try
            {
                if (!editable)
                {
                    insertCampo = "INSERT INTO CAMPO (NOME, CITTA, VIA, CIVICO) "
                        + "VALUES (@NOME, @CITTA, @VIA, @CIVICO)";

                    cmd = DbConnection.Open(insertCampo);
                }
                else
                {
                    insertCampo = "UPDATE CAMPO SET NOME = @NOME, CITTA = @CITTA, VIA = @VIA, CIVICO = @CIVICO "
                        + "WHERE ID = @ID";

                    cmd = DbConnection.Open(insertCampo);
                    cmd.Parameters.AddWithValue("ID", id);
                }

                cmd.CommandText = insertCampo;
                cmd.Parameters.AddWithValue("@NOME", nome);
                cmd.Parameters.AddWithValue("@CITTA", città);
                cmd.Parameters.AddWithValue("@VIA", via);
                cmd.Parameters.AddWithValue("@CIVICO", civico);

                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                }
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }

            DbConnection.Close();
            this.Close();
        }

        private void Resetta_Click(object sender, EventArgs e)
        {
            nomeCampo.ResetText();
            nomeCittà.ResetText();
            civicoCampo.ResetText();
        }
    }
}
