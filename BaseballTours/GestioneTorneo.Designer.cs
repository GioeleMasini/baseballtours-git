﻿namespace BaseballTours
{
    partial class GestioneTorneo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestioneTorneo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chiudiTorneoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.squadreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.campiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.volontariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mansioniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.squadreIscritteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giocatoriIscrittiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.infoPage = new System.Windows.Forms.TabPage();
            this.TourView = new System.Windows.Forms.Panel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtTourInfo = new System.Windows.Forms.Label();
            this.txtBestTeams = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtProxPartite = new System.Windows.Forms.Label();
            this.gridProxPartite = new System.Windows.Forms.DataGridView();
            this.txtPastPartite = new System.Windows.Forms.Label();
            this.gridPastPartite = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDcampo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.infoTorneo = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nomeTour = new System.Windows.Forms.Label();
            this.partitePage = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnEliminaPartita = new System.Windows.Forms.Button();
            this.btnModificaPartita = new System.Windows.Forms.Button();
            this.btnInserisciPartita = new System.Windows.Forms.Button();
            this.partiteGridView = new System.Windows.Forms.DataGridView();
            this.dataOra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.squadraCasa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.punteggio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.squadraOspite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffPage = new System.Windows.Forms.TabPage();
            this.btnModificaStaff = new System.Windows.Forms.Button();
            this.staffDataGrid = new System.Windows.Forms.DataGridView();
            this.IDPERSONA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CognomeData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDMANSIONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MansioneData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIZIONEMANSIONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORADATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OraInizioDataTrim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORAFINEFULL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OraFineData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRimuoviStaff = new System.Windows.Forms.Button();
            this.btnAggiungiStaff = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.tURNOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tURNOTableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.TURNOTableAdapter();
            this.colData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSquadraCasa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSquadraOspite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.infoPage.SuspendLayout();
            this.TourView.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProxPartite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPastPartite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.partitePage.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.partiteGridView)).BeginInit();
            this.staffPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staffDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tURNOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.flowLayoutPanel1.Controls.Add(this.menuStrip1);
            this.flowLayoutPanel1.Controls.Add(this.tabControl1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-1, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(821, 738);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.visualizzaToolStripMenuItem,
            this.modificaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(821, 30);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuContainer";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chiudiTorneoToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 26);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // chiudiTorneoToolStripMenuItem
            // 
            this.chiudiTorneoToolStripMenuItem.Name = "chiudiTorneoToolStripMenuItem";
            this.chiudiTorneoToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.chiudiTorneoToolStripMenuItem.Text = "Chiudi torneo";
            this.chiudiTorneoToolStripMenuItem.Click += new System.EventHandler(this.chiudiTorneoToolStripMenuItem_Click);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.esciToolStripMenuItem_Click);
            // 
            // visualizzaToolStripMenuItem
            // 
            this.visualizzaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.squadreToolStripMenuItem,
            this.campiToolStripMenuItem,
            this.volontariToolStripMenuItem,
            this.mansioniToolStripMenuItem});
            this.visualizzaToolStripMenuItem.Name = "visualizzaToolStripMenuItem";
            this.visualizzaToolStripMenuItem.Size = new System.Drawing.Size(86, 26);
            this.visualizzaToolStripMenuItem.Text = "Visualizza";
            // 
            // squadreToolStripMenuItem
            // 
            this.squadreToolStripMenuItem.Name = "squadreToolStripMenuItem";
            this.squadreToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.squadreToolStripMenuItem.Text = "Squadre";
            this.squadreToolStripMenuItem.Click += new System.EventHandler(this.squadreToolStripMenuItem_Click);
            // 
            // campiToolStripMenuItem
            // 
            this.campiToolStripMenuItem.Name = "campiToolStripMenuItem";
            this.campiToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.campiToolStripMenuItem.Text = "Campi";
            this.campiToolStripMenuItem.Click += new System.EventHandler(this.campiToolStripMenuItem_Click);
            // 
            // volontariToolStripMenuItem
            // 
            this.volontariToolStripMenuItem.Name = "volontariToolStripMenuItem";
            this.volontariToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.volontariToolStripMenuItem.Text = "Persone";
            this.volontariToolStripMenuItem.Click += new System.EventHandler(this.volontariToolStripMenuItem_Click);
            // 
            // mansioniToolStripMenuItem
            // 
            this.mansioniToolStripMenuItem.Name = "mansioniToolStripMenuItem";
            this.mansioniToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.mansioniToolStripMenuItem.Text = "Mansioni";
            this.mansioniToolStripMenuItem.Click += new System.EventHandler(this.mansioniToolStripMenuItem_Click);
            // 
            // modificaToolStripMenuItem
            // 
            this.modificaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.squadreIscritteToolStripMenuItem,
            this.giocatoriIscrittiToolStripMenuItem});
            this.modificaToolStripMenuItem.Name = "modificaToolStripMenuItem";
            this.modificaToolStripMenuItem.Size = new System.Drawing.Size(116, 26);
            this.modificaToolStripMenuItem.Text = "Questo torneo";
            this.modificaToolStripMenuItem.Click += new System.EventHandler(this.modificaToolStripMenuItem_Click);
            // 
            // squadreIscritteToolStripMenuItem
            // 
            this.squadreIscritteToolStripMenuItem.Name = "squadreIscritteToolStripMenuItem";
            this.squadreIscritteToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.squadreIscritteToolStripMenuItem.Text = "Squadre iscritte";
            this.squadreIscritteToolStripMenuItem.Click += new System.EventHandler(this.squadreIscritteToolStripMenuItem_Click);
            // 
            // giocatoriIscrittiToolStripMenuItem
            // 
            this.giocatoriIscrittiToolStripMenuItem.Name = "giocatoriIscrittiToolStripMenuItem";
            this.giocatoriIscrittiToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.giocatoriIscrittiToolStripMenuItem.Text = "Giocatori iscritti";
            this.giocatoriIscrittiToolStripMenuItem.Click += new System.EventHandler(this.giocatoriIscrittiToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.infoPage);
            this.tabControl1.Controls.Add(this.partitePage);
            this.tabControl1.Controls.Add(this.staffPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 42);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(791, 564);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            // 
            // infoPage
            // 
            this.infoPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.infoPage.Controls.Add(this.TourView);
            this.infoPage.Location = new System.Drawing.Point(4, 28);
            this.infoPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.infoPage.Name = "infoPage";
            this.infoPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.infoPage.Size = new System.Drawing.Size(783, 532);
            this.infoPage.TabIndex = 0;
            this.infoPage.Text = "Info";
            // 
            // TourView
            // 
            this.TourView.BackColor = System.Drawing.Color.Transparent;
            this.TourView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.TourView.Controls.Add(this.flowLayoutPanel5);
            this.TourView.Controls.Add(this.flowLayoutPanel4);
            this.TourView.Controls.Add(this.infoTorneo);
            this.TourView.Controls.Add(this.pictureBox1);
            this.TourView.Controls.Add(this.nomeTour);
            this.TourView.Location = new System.Drawing.Point(0, 0);
            this.TourView.Margin = new System.Windows.Forms.Padding(2);
            this.TourView.Name = "TourView";
            this.TourView.Size = new System.Drawing.Size(776, 534);
            this.TourView.TabIndex = 0;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.txtTourInfo);
            this.flowLayoutPanel5.Controls.Add(this.txtBestTeams);
            this.flowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(517, 157);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(246, 371);
            this.flowLayoutPanel5.TabIndex = 6;
            // 
            // txtTourInfo
            // 
            this.txtTourInfo.AutoSize = true;
            this.txtTourInfo.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTourInfo.Location = new System.Drawing.Point(3, 0);
            this.txtTourInfo.MaximumSize = new System.Drawing.Size(243, 0);
            this.txtTourInfo.MinimumSize = new System.Drawing.Size(240, 0);
            this.txtTourInfo.Name = "txtTourInfo";
            this.txtTourInfo.Padding = new System.Windows.Forms.Padding(10);
            this.txtTourInfo.Size = new System.Drawing.Size(240, 43);
            this.txtTourInfo.TabIndex = 5;
            this.txtTourInfo.Text = "Classifica";
            // 
            // txtBestTeams
            // 
            this.txtBestTeams.AutoSize = true;
            this.txtBestTeams.Font = new System.Drawing.Font("Arial", 10F);
            this.txtBestTeams.Location = new System.Drawing.Point(3, 43);
            this.txtBestTeams.MaximumSize = new System.Drawing.Size(243, 0);
            this.txtBestTeams.MinimumSize = new System.Drawing.Size(240, 0);
            this.txtBestTeams.Name = "txtBestTeams";
            this.txtBestTeams.Padding = new System.Windows.Forms.Padding(10);
            this.txtBestTeams.Size = new System.Drawing.Size(240, 39);
            this.txtBestTeams.TabIndex = 6;
            this.txtBestTeams.Text = "- Best Teams -";
            this.txtBestTeams.Visible = false;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.txtProxPartite);
            this.flowLayoutPanel4.Controls.Add(this.gridProxPartite);
            this.flowLayoutPanel4.Controls.Add(this.txtPastPartite);
            this.flowLayoutPanel4.Controls.Add(this.gridPastPartite);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(8, 149);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(503, 379);
            this.flowLayoutPanel4.TabIndex = 4;
            // 
            // txtProxPartite
            // 
            this.txtProxPartite.AutoSize = true;
            this.txtProxPartite.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.txtProxPartite.Location = new System.Drawing.Point(3, 0);
            this.txtProxPartite.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.txtProxPartite.Name = "txtProxPartite";
            this.txtProxPartite.Size = new System.Drawing.Size(138, 19);
            this.txtProxPartite.TabIndex = 2;
            this.txtProxPartite.Text = "Prossime partite";
            // 
            // gridProxPartite
            // 
            this.gridProxPartite.AllowUserToAddRows = false;
            this.gridProxPartite.AllowUserToDeleteRows = false;
            this.gridProxPartite.AllowUserToResizeColumns = false;
            this.gridProxPartite.AllowUserToResizeRows = false;
            this.gridProxPartite.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridProxPartite.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridProxPartite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProxPartite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colData,
            this.colSquadraCasa,
            this.colSquadraOspite});
            this.gridProxPartite.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridProxPartite.Location = new System.Drawing.Point(2, 31);
            this.gridProxPartite.Margin = new System.Windows.Forms.Padding(2);
            this.gridProxPartite.MultiSelect = false;
            this.gridProxPartite.Name = "gridProxPartite";
            this.gridProxPartite.ReadOnly = true;
            this.gridProxPartite.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridProxPartite.RowHeadersVisible = false;
            this.gridProxPartite.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridProxPartite.RowTemplate.Height = 24;
            this.gridProxPartite.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridProxPartite.Size = new System.Drawing.Size(501, 150);
            this.gridProxPartite.TabIndex = 1;
            this.gridProxPartite.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProxPartite_CellDoubleClick);
            // 
            // txtPastPartite
            // 
            this.txtPastPartite.AutoSize = true;
            this.txtPastPartite.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.txtPastPartite.Location = new System.Drawing.Point(3, 193);
            this.txtPastPartite.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtPastPartite.Name = "txtPastPartite";
            this.txtPastPartite.Size = new System.Drawing.Size(151, 19);
            this.txtPastPartite.TabIndex = 3;
            this.txtPastPartite.Text = "Partite già giocate";
            // 
            // gridPastPartite
            // 
            this.gridPastPartite.AllowUserToAddRows = false;
            this.gridPastPartite.AllowUserToDeleteRows = false;
            this.gridPastPartite.AllowUserToOrderColumns = true;
            this.gridPastPartite.AllowUserToResizeColumns = false;
            this.gridPastPartite.AllowUserToResizeRows = false;
            this.gridPastPartite.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridPastPartite.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridPastPartite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPastPartite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.IDcampo,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.gridPastPartite.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridPastPartite.Location = new System.Drawing.Point(2, 224);
            this.gridPastPartite.Margin = new System.Windows.Forms.Padding(2);
            this.gridPastPartite.MultiSelect = false;
            this.gridPastPartite.Name = "gridPastPartite";
            this.gridPastPartite.ReadOnly = true;
            this.gridPastPartite.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridPastPartite.RowHeadersVisible = false;
            this.gridPastPartite.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridPastPartite.RowTemplate.Height = 24;
            this.gridPastPartite.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPastPartite.Size = new System.Drawing.Size(501, 150);
            this.gridPastPartite.TabIndex = 4;
            this.gridPastPartite.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPastPartite_CellDoubleClick);
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Width = 68;
            // 
            // IDcampo
            // 
            this.IDcampo.HeaderText = "ID Campo";
            this.IDcampo.Name = "IDcampo";
            this.IDcampo.ReadOnly = true;
            this.IDcampo.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Squadra Casa";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Squadra Ospite";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn3.HeaderText = "Punteggio";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 107;
            // 
            // infoTorneo
            // 
            this.infoTorneo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic);
            this.infoTorneo.Location = new System.Drawing.Point(9, 68);
            this.infoTorneo.Name = "infoTorneo";
            this.infoTorneo.Size = new System.Drawing.Size(502, 84);
            this.infoTorneo.TabIndex = 3;
            this.infoTorneo.Text = "Squadra organizzatrice/Data";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(517, 14);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(246, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // nomeTour
            // 
            this.nomeTour.BackColor = System.Drawing.Color.Transparent;
            this.nomeTour.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.nomeTour.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.nomeTour.Location = new System.Drawing.Point(2, 14);
            this.nomeTour.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nomeTour.Name = "nomeTour";
            this.nomeTour.Size = new System.Drawing.Size(509, 54);
            this.nomeTour.TabIndex = 0;
            this.nomeTour.Text = "Nome Torneo";
            this.nomeTour.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // partitePage
            // 
            this.partitePage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.partitePage.Controls.Add(this.flowLayoutPanel3);
            this.partitePage.Controls.Add(this.flowLayoutPanel2);
            this.partitePage.Controls.Add(this.partiteGridView);
            this.partitePage.Location = new System.Drawing.Point(4, 28);
            this.partitePage.Name = "partitePage";
            this.partitePage.Padding = new System.Windows.Forms.Padding(3);
            this.partitePage.Size = new System.Drawing.Size(783, 532);
            this.partitePage.TabIndex = 2;
            this.partitePage.Text = "Partite";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label1);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 7);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(405, 35);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(294, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "(Doppio click per visualizzare i dettagli)";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnEliminaPartita);
            this.flowLayoutPanel2.Controls.Add(this.btnModificaPartita);
            this.flowLayoutPanel2.Controls.Add(this.btnInserisciPartita);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(411, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(366, 42);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // btnEliminaPartita
            // 
            this.btnEliminaPartita.Location = new System.Drawing.Point(262, 3);
            this.btnEliminaPartita.Name = "btnEliminaPartita";
            this.btnEliminaPartita.Size = new System.Drawing.Size(101, 39);
            this.btnEliminaPartita.TabIndex = 1;
            this.btnEliminaPartita.Text = "Elimina";
            this.btnEliminaPartita.UseVisualStyleBackColor = true;
            this.btnEliminaPartita.Click += new System.EventHandler(this.btnEliminaPartita_Click);
            // 
            // btnModificaPartita
            // 
            this.btnModificaPartita.Location = new System.Drawing.Point(155, 3);
            this.btnModificaPartita.Name = "btnModificaPartita";
            this.btnModificaPartita.Size = new System.Drawing.Size(101, 39);
            this.btnModificaPartita.TabIndex = 0;
            this.btnModificaPartita.Text = "Modifica";
            this.btnModificaPartita.UseVisualStyleBackColor = true;
            this.btnModificaPartita.Click += new System.EventHandler(this.btnModificaPartita_Click);
            // 
            // btnInserisciPartita
            // 
            this.btnInserisciPartita.Location = new System.Drawing.Point(48, 3);
            this.btnInserisciPartita.Name = "btnInserisciPartita";
            this.btnInserisciPartita.Size = new System.Drawing.Size(101, 39);
            this.btnInserisciPartita.TabIndex = 2;
            this.btnInserisciPartita.Text = "Inserisci";
            this.btnInserisciPartita.UseVisualStyleBackColor = true;
            this.btnInserisciPartita.Click += new System.EventHandler(this.btnInserisciPartita_Click);
            // 
            // partiteGridView
            // 
            this.partiteGridView.AllowUserToAddRows = false;
            this.partiteGridView.AllowUserToDeleteRows = false;
            this.partiteGridView.AllowUserToResizeRows = false;
            this.partiteGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.partiteGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.partiteGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataOra,
            this.squadraCasa,
            this.punteggio,
            this.squadraOspite});
            this.partiteGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.partiteGridView.Location = new System.Drawing.Point(3, 45);
            this.partiteGridView.MultiSelect = false;
            this.partiteGridView.Name = "partiteGridView";
            this.partiteGridView.ReadOnly = true;
            this.partiteGridView.RowHeadersVisible = false;
            this.partiteGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.partiteGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.partiteGridView.Size = new System.Drawing.Size(777, 484);
            this.partiteGridView.TabIndex = 0;
            this.partiteGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.partiteGridView_CellDoubleClick);
            // 
            // dataOra
            // 
            this.dataOra.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataOra.HeaderText = "Data e Ora";
            this.dataOra.Name = "dataOra";
            this.dataOra.ReadOnly = true;
            this.dataOra.Width = 80;
            // 
            // squadraCasa
            // 
            this.squadraCasa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.squadraCasa.DefaultCellStyle = dataGridViewCellStyle7;
            this.squadraCasa.HeaderText = "Squadra Casa";
            this.squadraCasa.Name = "squadraCasa";
            this.squadraCasa.ReadOnly = true;
            // 
            // punteggio
            // 
            this.punteggio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.punteggio.DefaultCellStyle = dataGridViewCellStyle8;
            this.punteggio.HeaderText = "Punteggio";
            this.punteggio.Name = "punteggio";
            this.punteggio.ReadOnly = true;
            this.punteggio.Width = 107;
            // 
            // squadraOspite
            // 
            this.squadraOspite.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.squadraOspite.DefaultCellStyle = dataGridViewCellStyle9;
            this.squadraOspite.HeaderText = "Squadra Ospite";
            this.squadraOspite.Name = "squadraOspite";
            this.squadraOspite.ReadOnly = true;
            // 
            // staffPage
            // 
            this.staffPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.staffPage.Controls.Add(this.btnModificaStaff);
            this.staffPage.Controls.Add(this.staffDataGrid);
            this.staffPage.Controls.Add(this.btnRimuoviStaff);
            this.staffPage.Controls.Add(this.btnAggiungiStaff);
            this.staffPage.Controls.Add(this.label2);
            this.staffPage.Location = new System.Drawing.Point(4, 28);
            this.staffPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.staffPage.Name = "staffPage";
            this.staffPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.staffPage.Size = new System.Drawing.Size(783, 532);
            this.staffPage.TabIndex = 1;
            this.staffPage.Text = "Staff";
            // 
            // btnModificaStaff
            // 
            this.btnModificaStaff.Location = new System.Drawing.Point(325, 424);
            this.btnModificaStaff.Name = "btnModificaStaff";
            this.btnModificaStaff.Size = new System.Drawing.Size(160, 51);
            this.btnModificaStaff.TabIndex = 6;
            this.btnModificaStaff.Text = "Modifica";
            this.btnModificaStaff.UseVisualStyleBackColor = true;
            this.btnModificaStaff.Click += new System.EventHandler(this.btnModificaStaff_Click);
            // 
            // staffDataGrid
            // 
            this.staffDataGrid.AllowUserToAddRows = false;
            this.staffDataGrid.AllowUserToDeleteRows = false;
            this.staffDataGrid.AllowUserToResizeColumns = false;
            this.staffDataGrid.AllowUserToResizeRows = false;
            this.staffDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.staffDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.staffDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDPERSONA,
            this.NomeData,
            this.CognomeData,
            this.IDMANSIONE,
            this.MansioneData,
            this.DESCRIZIONEMANSIONE,
            this.ORADATA,
            this.OraInizioDataTrim,
            this.ORAFINEFULL,
            this.OraFineData});
            this.staffDataGrid.Location = new System.Drawing.Point(6, 71);
            this.staffDataGrid.MultiSelect = false;
            this.staffDataGrid.Name = "staffDataGrid";
            this.staffDataGrid.ReadOnly = true;
            this.staffDataGrid.RowTemplate.Height = 24;
            this.staffDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.staffDataGrid.Size = new System.Drawing.Size(771, 305);
            this.staffDataGrid.TabIndex = 4;
            // 
            // IDPERSONA
            // 
            this.IDPERSONA.HeaderText = "IdPersona";
            this.IDPERSONA.Name = "IDPERSONA";
            this.IDPERSONA.ReadOnly = true;
            this.IDPERSONA.Visible = false;
            // 
            // NomeData
            // 
            this.NomeData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NomeData.HeaderText = "Nome";
            this.NomeData.Name = "NomeData";
            this.NomeData.ReadOnly = true;
            // 
            // CognomeData
            // 
            this.CognomeData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CognomeData.HeaderText = "Cognome";
            this.CognomeData.Name = "CognomeData";
            this.CognomeData.ReadOnly = true;
            // 
            // IDMANSIONE
            // 
            this.IDMANSIONE.HeaderText = "IdMansione";
            this.IDMANSIONE.Name = "IDMANSIONE";
            this.IDMANSIONE.ReadOnly = true;
            this.IDMANSIONE.Visible = false;
            // 
            // MansioneData
            // 
            this.MansioneData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MansioneData.HeaderText = "Mansione";
            this.MansioneData.Name = "MansioneData";
            this.MansioneData.ReadOnly = true;
            // 
            // DESCRIZIONEMANSIONE
            // 
            this.DESCRIZIONEMANSIONE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DESCRIZIONEMANSIONE.HeaderText = "Descrizione";
            this.DESCRIZIONEMANSIONE.Name = "DESCRIZIONEMANSIONE";
            this.DESCRIZIONEMANSIONE.ReadOnly = true;
            // 
            // ORADATA
            // 
            this.ORADATA.HeaderText = "OraInizio";
            this.ORADATA.Name = "ORADATA";
            this.ORADATA.ReadOnly = true;
            this.ORADATA.Visible = false;
            // 
            // OraInizioDataTrim
            // 
            this.OraInizioDataTrim.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OraInizioDataTrim.HeaderText = "Ora inizio";
            this.OraInizioDataTrim.Name = "OraInizioDataTrim";
            this.OraInizioDataTrim.ReadOnly = true;
            // 
            // ORAFINEFULL
            // 
            this.ORAFINEFULL.HeaderText = "OraFine";
            this.ORAFINEFULL.Name = "ORAFINEFULL";
            this.ORAFINEFULL.ReadOnly = true;
            this.ORAFINEFULL.Visible = false;
            // 
            // OraFineData
            // 
            this.OraFineData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OraFineData.HeaderText = "Ora fine";
            this.OraFineData.Name = "OraFineData";
            this.OraFineData.ReadOnly = true;
            // 
            // btnRimuoviStaff
            // 
            this.btnRimuoviStaff.Location = new System.Drawing.Point(617, 424);
            this.btnRimuoviStaff.Name = "btnRimuoviStaff";
            this.btnRimuoviStaff.Size = new System.Drawing.Size(160, 51);
            this.btnRimuoviStaff.TabIndex = 7;
            this.btnRimuoviStaff.Text = "Rimuovi";
            this.btnRimuoviStaff.UseVisualStyleBackColor = true;
            this.btnRimuoviStaff.Click += new System.EventHandler(this.btnRimuoviStaff_Click);
            // 
            // btnAggiungiStaff
            // 
            this.btnAggiungiStaff.Location = new System.Drawing.Point(6, 424);
            this.btnAggiungiStaff.Name = "btnAggiungiStaff";
            this.btnAggiungiStaff.Size = new System.Drawing.Size(160, 51);
            this.btnAggiungiStaff.TabIndex = 5;
            this.btnAggiungiStaff.Text = "Aggiungi";
            this.btnAggiungiStaff.UseVisualStyleBackColor = true;
            this.btnAggiungiStaff.Click += new System.EventHandler(this.btnAggiungiStaff_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Elenco turni";
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tURNOBindingSource
            // 
            this.tURNOBindingSource.DataMember = "TURNO";
            this.tURNOBindingSource.DataSource = this.baseDataDataSet;
            // 
            // tURNOTableAdapter
            // 
            this.tURNOTableAdapter.ClearBeforeFill = true;
            // 
            // colData
            // 
            this.colData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colData.HeaderText = "Data";
            this.colData.Name = "colData";
            this.colData.ReadOnly = true;
            // 
            // colSquadraCasa
            // 
            this.colSquadraCasa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colSquadraCasa.HeaderText = "Sq. Casa";
            this.colSquadraCasa.Name = "colSquadraCasa";
            this.colSquadraCasa.ReadOnly = true;
            // 
            // colSquadraOspite
            // 
            this.colSquadraOspite.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colSquadraOspite.HeaderText = "Sq. Ospite";
            this.colSquadraOspite.Name = "colSquadraOspite";
            this.colSquadraOspite.ReadOnly = true;
            // 
            // GestioneTorneo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(819, 612);
            this.Controls.Add(this.flowLayoutPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "GestioneTorneo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baseball Tournaments - Gestione del torneo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GestioneTorneo_FormClosing);
            this.Load += new System.EventHandler(this.GestioneTorneo_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.infoPage.ResumeLayout(false);
            this.TourView.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProxPartite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPastPartite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.partitePage.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.partiteGridView)).EndInit();
            this.staffPage.ResumeLayout(false);
            this.staffPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staffDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tURNOBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chiudiTorneoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificaToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage infoPage;
        private System.Windows.Forms.Panel TourView;
        private System.Windows.Forms.Label infoTorneo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView gridProxPartite;
        private System.Windows.Forms.Label nomeTour;
        private System.Windows.Forms.TabPage partitePage;
        private System.Windows.Forms.DataGridView partiteGridView;
        private System.Windows.Forms.TabPage staffPage;
        private System.Windows.Forms.Button btnRimuoviStaff;
        private System.Windows.Forms.Button btnAggiungiStaff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataOra;
        private System.Windows.Forms.DataGridViewTextBoxColumn squadraCasa;
        private System.Windows.Forms.DataGridViewTextBoxColumn punteggio;
        private System.Windows.Forms.DataGridViewTextBoxColumn squadraOspite;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnEliminaPartita;
        private System.Windows.Forms.Button btnModificaPartita;
        private System.Windows.Forms.Button btnInserisciPartita;
        private System.Windows.Forms.ToolStripMenuItem visualizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem campiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volontariToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView staffDataGrid;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.BindingSource tURNOBindingSource;
        private BaseDataDataSetTableAdapters.TURNOTableAdapter tURNOTableAdapter;
        private System.Windows.Forms.Button btnModificaStaff;
        private System.Windows.Forms.ToolStripMenuItem squadreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mansioniToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label txtProxPartite;
        private System.Windows.Forms.Label txtPastPartite;
        private System.Windows.Forms.DataGridView gridPastPartite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDcampo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label txtTourInfo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label txtBestTeams;
        private System.Windows.Forms.ToolStripMenuItem giocatoriIscrittiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem squadreIscritteToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDPERSONA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeData;
        private System.Windows.Forms.DataGridViewTextBoxColumn CognomeData;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDMANSIONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn MansioneData;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIZIONEMANSIONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORADATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn OraInizioDataTrim;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORAFINEFULL;
        private System.Windows.Forms.DataGridViewTextBoxColumn OraFineData;
        private System.Windows.Forms.DataGridViewTextBoxColumn colData;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSquadraCasa;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSquadraOspite;
    }
}