﻿namespace BaseballTours
{
    partial class Squadra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Squadra));
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.canvas = new System.Windows.Forms.Panel();
            this.imgSquadra = new System.Windows.Forms.PictureBox();
            this.txtCittaSquadra = new System.Windows.Forms.Label();
            this.txtNomeSquadra = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSquadra)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(734, 540);
            this.panel1.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel2.Controls.Add(this.txtNomeSquadra);
            this.flowLayoutPanel2.Controls.Add(this.txtCittaSquadra);
            this.flowLayoutPanel2.Controls.Add(this.imgSquadra);
            this.flowLayoutPanel2.Controls.Add(this.canvas);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(299, 534);
            this.flowLayoutPanel2.TabIndex = 5;
            // 
            // canvas
            // 
            this.canvas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.canvas.Location = new System.Drawing.Point(58, 307);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(182, 157);
            this.canvas.TabIndex = 1;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            // 
            // imgSquadra
            // 
            this.imgSquadra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imgSquadra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.imgSquadra.Image = ((System.Drawing.Image)(resources.GetObject("imgSquadra.Image")));
            this.imgSquadra.InitialImage = ((System.Drawing.Image)(resources.GetObject("imgSquadra.InitialImage")));
            this.imgSquadra.Location = new System.Drawing.Point(49, 89);
            this.imgSquadra.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.imgSquadra.Name = "imgSquadra";
            this.imgSquadra.Size = new System.Drawing.Size(200, 200);
            this.imgSquadra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgSquadra.TabIndex = 5;
            this.imgSquadra.TabStop = false;
            // 
            // txtCittaSquadra
            // 
            this.txtCittaSquadra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtCittaSquadra.AutoSize = true;
            this.txtCittaSquadra.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic);
            this.txtCittaSquadra.Location = new System.Drawing.Point(131, 55);
            this.txtCittaSquadra.Margin = new System.Windows.Forms.Padding(3, 0, 3, 15);
            this.txtCittaSquadra.Name = "txtCittaSquadra";
            this.txtCittaSquadra.Size = new System.Drawing.Size(37, 16);
            this.txtCittaSquadra.TabIndex = 4;
            this.txtCittaSquadra.Text = "Città";
            // 
            // txtNomeSquadra
            // 
            this.txtNomeSquadra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNomeSquadra.BackColor = System.Drawing.Color.Transparent;
            this.txtNomeSquadra.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.txtNomeSquadra.Location = new System.Drawing.Point(2, 0);
            this.txtNomeSquadra.Margin = new System.Windows.Forms.Padding(2, 0, 2, 10);
            this.txtNomeSquadra.Name = "txtNomeSquadra";
            this.txtNomeSquadra.Size = new System.Drawing.Size(295, 45);
            this.txtNomeSquadra.TabIndex = 0;
            this.txtNomeSquadra.Text = "Nome della squadra";
            this.txtNomeSquadra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.030303F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 96.9697F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(734, 540);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // Squadra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(309, 516);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Squadra";
            this.Text = "Baseball Tournaments - Squadra";
            this.Load += new System.EventHandler(this.Squadra_Load);
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSquadra)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label txtNomeSquadra;
        private System.Windows.Forms.Label txtCittaSquadra;
        private System.Windows.Forms.PictureBox imgSquadra;
        private System.Windows.Forms.Panel canvas;
    }
}