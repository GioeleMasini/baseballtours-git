﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseballTours
{

    class Ruoli
    {
        private static string[] ruoli = new string[]{
                "Panchina",
                "Lanciatore",
                "Ricevitore",
                "1a Base",
                "2a Base",
                "3a Base",
                "Interbase",
                "Est. Sinistro",
                "Est. Centro",
                "Est. Destro" };

        public static string get(int numRuolo)
        {
            return Ruoli.ruoli[numRuolo];
        }

        public static int get(string nomeRuolo)
        {
            if (Ruoli.ruoli.Contains(nomeRuolo))
            {
                int i = 0;
                foreach (string nome in Ruoli.ruoli)
                {
                    if (nome.Equals(nomeRuolo)) return i;
                    i++;
                }
            }
            //Se non c'è nell'array, allora eccezione
            throw new ArgumentException("Nome del ruolo errato o non presente.");
        }

        public static string[] getAll()
        {
            return Ruoli.ruoli;
        }
    }
}
