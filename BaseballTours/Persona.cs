﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseballTours
{
    public class Persona
    {
        public long ID { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Descrizione { get; set; }
        public DateTime DataNascita { get; set; }
    }
}
