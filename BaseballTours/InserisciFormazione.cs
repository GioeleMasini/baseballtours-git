﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BaseballTours
{
    public partial class InserisciFormazione : Form
    {
        private string idSquadra;
        private long idPartita;

        private long codFormazione = -1;
        private Dictionary<long, string> listGiocatori;
        private int idTorneo;
        private string annoTorneo;
        private Dictionary<long, object[]> listFormazione;

        public InserisciFormazione(int idTorneo, string annoEdizione, string idSquadra, long idPartita, long? idFormazione)
        {
            InitializeComponent();

            this.idTorneo = idTorneo;
            this.annoTorneo = annoEdizione;

            this.idSquadra = idSquadra;
            this.idPartita = idPartita;
            this.codFormazione = idFormazione == null ? -1 : (long)idFormazione;
        }

        private void ListaVolontari_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.RUOLO'. È possibile spostarla o rimuoverla se necessario.
            this.rUOLOTableAdapter.Fill(this.baseDataDataSet.RUOLO);

            DataGridViewComboBoxColumn colGiocatore = (DataGridViewComboBoxColumn)this.gridFormazione.Columns["cbGiocatore"];

            //Una volta riempita la tabella, imposto tramite select manuale la check di iscrizione
            string sqlSelectGiocatori = "SELECT IDPERSONA, COGNOME, NUMERO FROM MILITA, PERSONA WHERE PERSONA.ID = IDPERSONA AND IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDSQUADRA = @IDSQUADRA";
            var cmd = DbConnection.Open(sqlSelectGiocatori);

            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
            cmd.Parameters.AddWithValue("IDSQUADRA", this.idSquadra);

            var reader = cmd.ExecuteReader();

            this.listGiocatori = new Dictionary<long, string>();
            this.listGiocatori.Add(-1, ""); //Aggiungo una riga vuota ai giocatori
            while (reader.Read())
            {
                this.listGiocatori.Add(long.Parse(reader["IDPERSONA"].ToString()), reader["COGNOME"].ToString() + " #" + reader["NUMERO"].ToString());
            }

            colGiocatore.DataSource = new BindingSource(this.listGiocatori, null);
            colGiocatore.DisplayMember = "Value";
            colGiocatore.ValueMember = "Key";


            //E poi carico i dati precedentemente inseriti
            string sqlSelectFormazione = "SELECT ID, IDPERSONA, IDRUOLO, INNING FROM RUOLOINNING WHERE IDFORMAZIONE = @IDFORMAZIONE AND IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE";
            cmd = DbConnection.Open(sqlSelectFormazione);

            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
            cmd.Parameters.AddWithValue("IDFORMAZIONE", this.codFormazione);

            reader = cmd.ExecuteReader();

            this.listFormazione = new Dictionary<long, object[]>();
            while (reader.Read())
            {
                var idCambio = long.Parse(reader["ID"].ToString());
                this.listFormazione.Add(idCambio, new object[] { long.Parse(reader["IDPERSONA"].ToString()), short.Parse(reader["IDRUOLO"].ToString()), reader["INNING"].ToString() });
            }

            foreach(var idCambio in this.listFormazione.Keys) {
                var cambio = this.listFormazione[idCambio];
                var idPersona = cambio[0];
                var idRuolo = cambio[1];
                var inning = cambio[2];
                this.gridFormazione.Rows.Add(idPersona, idRuolo, inning, idCambio);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Recupero tutti i giocatori dalla grid
            List<object[]> giocatori = new List<object[]>();
            List<object> idGiocatori = new List<object>();
            for (int i = 0; i < this.gridFormazione.Rows.Count; i++)
            {
                var giocatore = Utilities.getDataGridRowValues(this.gridFormazione.Rows[i]);
                if (giocatore[0] != null) {
                    giocatori.Add(giocatore);
                    idGiocatori.Add(giocatore[3]);
                }
            }

            //Faccio i dovuti controlli
            string errors = "";

            if (giocatori.Count() < 9)
            {
                errors += "- La formazione deve avere almeno 9 giocatori." + Environment.NewLine;
            }

            if (errors == "")
            {
                foreach (object[] giocatore in giocatori)
                {
                    //Imposto l'inning a 0 se non scelto dall'utente
                    if (giocatore[2] == null)
                    {
                        giocatore[2] = 0;
                    }

                    var ruolo = giocatore[1];
                    if (ruolo == null)
                    {
                        errors += "- Impostare il ruolo di tutti i giocatori scelti. " + Environment.NewLine;
                    }
                }
            }

            if (errors == "")
            {
                for (int i = 1; i <= 9; i++)
                {
                    int countRuoli = 0;

                    foreach (object[] giocatore in giocatori)
                    {
                        if (giocatore[2].ToString() == "0") //Formazione di partenza, inning "zero"
                            countRuoli += int.Parse(giocatore[1].ToString()) == i ? 1 : 0;
                    }

                    if (countRuoli == 0)
                        errors += "- Nessun giocatore assegnato alla posizione '" + i + "' nella formazione di partenza." + Environment.NewLine;
                }
            }

            if (errors != "")
            {
                ErrorDialog.Show("Errore", "Impossibile procedere per i seguenti errori: " + Environment.NewLine + errors);
                return;
            }

            //Inizio gli update/inserimenti
            SqlCeCommand cmd;
            SqlCeDataReader reader;
            if (this.codFormazione < 0) {
                // Formazione non presente, devo inserirla
                string insertFormazione = "INSERT INTO FORMAZIONE (COL) VALUES(NULL)";
                cmd = DbConnection.Query(insertFormazione);
                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    ErrorDialog.Show("Errore", "Errore nell'inserimento di una nuova formazione.");
                    return;
                }

                string selectFormazione = "SELECT MAX(ID) FROM FORMAZIONE";
                cmd = DbConnection.Open(selectFormazione);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    // Formazione già presente, prendo il codice
                    this.codFormazione = reader.GetInt64(0);
                }

                //Aggiorno la partita con la giusta formazione
                string updateFormazione1 = "UPDATE PARTITA SET IDFORMAZIONECASA = @IDFORMAZIONE WHERE ID = @IDPARTITA AND IDSQUADRACASA = @IDSQUADRA";
                string updateFormazione2 = "UPDATE PARTITA SET IDFORMAZIONEOSPITI = @IDFORMAZIONE WHERE ID = @IDPARTITA AND IDSQUADRAOSPITI = @IDSQUADRA";
                cmd = DbConnection.Open(updateFormazione1);
                cmd.Parameters.AddWithValue("IDPARTITA", this.idPartita);
                cmd.Parameters.AddWithValue("IDSQUADRA", this.idSquadra);
                cmd.Parameters.AddWithValue("IDFORMAZIONE", this.codFormazione);
                cmd.ExecuteNonQuery();
                cmd.CommandText = updateFormazione2;
                cmd.ExecuteNonQuery();
            }

            //Inizio ad inserire i cambi
            int cambiInseriti = 0;

            string deleteCambio = "DELETE FROM RUOLOINNING WHERE ID = @ID";
            var cmd1 = DbConnection.Open(deleteCambio);
            cmd1.Parameters.AddWithValue("ID", null);

            string updateCambio = "UPDATE RUOLOINNING SET IDFORMAZIONE = @IDFORMAZIONE, IDRUOLO = @IDRUOLO, INNING = @INNING, IDPERSONA = @IDPERSONA, IDTORNEO = @IDTORNEO, ANNOEDIZIONE = @ANNOEDIZIONE WHERE ID = @ID";
            var cmd2 = DbConnection.Open(updateCambio);
            cmd2.Parameters.AddWithValue("ID", null);
            cmd2.Parameters.AddWithValue("IDFORMAZIONE", this.codFormazione);
            cmd2.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd2.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
            cmd2.Parameters.AddWithValue("IDPERSONA", null);
            cmd2.Parameters.AddWithValue("IDRUOLO", null);
            cmd2.Parameters.AddWithValue("INNING", null);

            foreach (var idCambioVecchio in this.listFormazione.Keys)
            {
                if (idGiocatori.Contains(idCambioVecchio))
                {
                    var idx = idGiocatori.IndexOf(idCambioVecchio);
                    //update
                    cmd2.Parameters["ID"].Value = idCambioVecchio;
                    cmd2.Parameters["IDPERSONA"].Value = giocatori[idx][0];
                    cmd2.Parameters["IDRUOLO"].Value = giocatori[idx][1];
                    cmd2.Parameters["INNING"].Value = giocatori[idx][2];
                    cmd2.ExecuteNonQuery();
                }
                else
                {
                    //delete
                    cmd1.Parameters["ID"].Value = idCambioVecchio;
                    cmd1.ExecuteNonQuery();
                }
            }

            //Se rimangono dei cambi, devo inserirli
            while (cambiInseriti < giocatori.Count)
            {
                if (idGiocatori[cambiInseriti] == null)
                {
                    string insertCambio = "INSERT INTO RUOLOINNING (IDFORMAZIONE, IDRUOLO, INNING, IDPERSONA, IDTORNEO, ANNOEDIZIONE) VALUES( "
                            + "@FORMAZIONE, @RUOLO, @INNING, @IDPERSONA, @IDTORNEO, @ANNOEDIZIONE)";
                    cmd = DbConnection.Query(insertCambio);
                    cmd.Parameters.AddWithValue("FORMAZIONE", this.codFormazione);
                    cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
                    cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                    cmd.Parameters.AddWithValue("IDPERSONA", giocatori[cambiInseriti][0]);
                    cmd.Parameters.AddWithValue("RUOLO", giocatori[cambiInseriti][1]);
                    cmd.Parameters.AddWithValue("INNING", giocatori[cambiInseriti][2]);
                    var affectedQuery = cmd.ExecuteNonQuery();
                    if (affectedQuery != 1)
                    {
                        ErrorDialog.Show("Errore", "Errore nell'inserimento dei dati.");
                        return;
                    }
                }
                cambiInseriti++;
            }

            this.Close();
        }
    }
}
