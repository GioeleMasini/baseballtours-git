﻿namespace BaseballTours
{
    partial class ModificaTorneo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModificaTorneo));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.annulla = new System.Windows.Forms.Button();
            this.salva = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nomeTorneo = new System.Windows.Forms.TextBox();
            this.annoEdizione = new System.Windows.Forms.TextBox();
            this.listaSquadre = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.annulla, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.salva, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.nomeTorneo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.annoEdizione, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.listaSquadre, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 67);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.52113F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.47887F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(402, 172);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // annulla
            // 
            this.annulla.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.annulla.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.annulla.Location = new System.Drawing.Point(3, 127);
            this.annulla.Name = "annulla";
            this.annulla.Size = new System.Drawing.Size(92, 36);
            this.annulla.TabIndex = 5;
            this.annulla.Text = "Annulla";
            this.annulla.UseVisualStyleBackColor = true;
            this.annulla.Click += new System.EventHandler(this.annulla_Click);
            // 
            // salva
            // 
            this.salva.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.salva.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salva.Location = new System.Drawing.Point(307, 127);
            this.salva.Name = "salva";
            this.salva.Size = new System.Drawing.Size(92, 36);
            this.salva.TabIndex = 4;
            this.salva.Text = "Salva";
            this.salva.UseVisualStyleBackColor = true;
            this.salva.Click += new System.EventHandler(this.salva_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome torneo";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Squadra organizzatrice";
            // 
            // nomeTorneo
            // 
            this.nomeTorneo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeTorneo.Location = new System.Drawing.Point(204, 5);
            this.nomeTorneo.Name = "nomeTorneo";
            this.nomeTorneo.Size = new System.Drawing.Size(195, 22);
            this.nomeTorneo.TabIndex = 1;
            // 
            // annoEdizione
            // 
            this.annoEdizione.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.annoEdizione.Location = new System.Drawing.Point(204, 67);
            this.annoEdizione.Name = "annoEdizione";
            this.annoEdizione.Size = new System.Drawing.Size(195, 22);
            this.annoEdizione.TabIndex = 3;
            // 
            // listaSquadre
            // 
            this.listaSquadre.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.listaSquadre.FormattingEnabled = true;
            this.listaSquadre.Location = new System.Drawing.Point(204, 36);
            this.listaSquadre.Name = "listaSquadre";
            this.listaSquadre.Size = new System.Drawing.Size(195, 24);
            this.listaSquadre.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Anno edizione";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(409, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Modifica Informazioni Torneo";
            // 
            // ModificaTorneo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 250);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ModificaTorneo";
            this.Text = "Baseball Tournaments - Modifica informazioni torneo";
            this.Load += new System.EventHandler(this.ModificaTorneo_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button annulla;
        private System.Windows.Forms.Button salva;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox nomeTorneo;
        private System.Windows.Forms.TextBox annoEdizione;
        private System.Windows.Forms.ComboBox listaSquadre;
    }
}