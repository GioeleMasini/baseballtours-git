﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class GestioneTorneo : Form
    {
        public int Torneo { get; set; }

        //private string nomeTorneo = null;
        private Torneo torneo = null;
        private int idTorneo = 0;
        private string anno = null;
        private string codSquadraTorneo = null;
        private string squadraTorneo = null;
        private List<object[]> partite = new List<object[]>();
        private List<int> pastPartite = new List<int>();
        private List<int> proxPartite = new List<int>();

        public GestioneTorneo(Torneo torneo, string anno)
        {
            InitializeComponent();
            this.torneo = torneo;
            this.idTorneo = this.torneo.ID;
            this.anno = anno;
            this.nomeTour.Text = this.torneo.Nome + " anno " + anno;
            this.codSquadraTorneo = this.torneo.Squadra;

            this.ottieniInfo();
        }

        private string retrieveBestTeams()
        {
            string text = "";

            string selectBestAttacker = "SELECT S.ID, S.NOME, S.CITTA, SUM(TOTPUNTI) AS TOTPUNTI FROM "
                + "("
                +    "SELECT SC.ID, SUM(PUNTICASA) AS TOTPUNTI "
                +    "FROM PARTITA PC, SQUADRA SC "
                + "WHERE PC.IDSQUADRACASA = SC.ID AND PC.IDTORNEO = @IDTORNEO AND PC.ANNOEDIZIONE = @ANNOTORNEO "
                +    "GROUP BY SC.ID "
                + "UNION ALL "
                +    "SELECT SO.ID, SUM(PUNTIOSPITI) AS TOTPUNTI "
                +    "FROM PARTITA PO, SQUADRA SO "
                + "WHERE PO.IDSQUADRAOSPITI = SO.ID AND PO.IDTORNEO = @IDTORNEO AND PO.ANNOEDIZIONE = @ANNOTORNEO "
                +    "GROUP BY SO.ID "
                + ") POINTS, SQUADRA S "
                + "WHERE S.ID = POINTS.ID "
                + "GROUP BY S.ID, S.NOME, S.CITTA "
                + "ORDER BY TOTPUNTI DESC";

            var cmd = DbConnection.Open(selectBestAttacker);
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOTORNEO", this.anno);

            var reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                //Prendo solo la prima riga del risultato della query
                //(clausola TOP non supportata da SQLce)
                text += "Miglior attacco: " + Environment.NewLine;
                text += reader["NOME"] + " " + reader["CITTA"] + Environment.NewLine;
                text += "con " + reader["TOTPUNTI"] + " punti segnati" + Environment.NewLine;
                text += Environment.NewLine;
            }

            string selectBestDefender = "SELECT S.ID, S.NOME, S.CITTA, SUM(TOTPUNTI) AS TOTPUNTI FROM "
                + "("
                +    "SELECT SC.ID, SUM(PUNTIOSPITI) AS TOTPUNTI "
                +    "FROM PARTITA PC, SQUADRA SC "
                +   "WHERE PC.IDSQUADRACASA = SC.ID AND PC.IDTORNEO = @IDTORNEO AND PC.ANNOEDIZIONE = @ANNOTORNEO "
                +    "GROUP BY SC.ID "
                + "UNION ALL "
                +    "SELECT SO.ID, SUM(PUNTICASA) AS TOTPUNTI "
                +    "FROM PARTITA PO, SQUADRA SO "
                +   "WHERE PO.IDSQUADRAOSPITI = SO.ID AND PO.IDTORNEO = @IDTORNEO AND PO.ANNOEDIZIONE = @ANNOTORNEO "
                +    "GROUP BY SO.ID "
                + ") POINTS, SQUADRA S "
                + "WHERE S.ID = POINTS.ID "
                + "GROUP BY S.ID, S.NOME, S.CITTA "
                + "ORDER BY TOTPUNTI";

            cmd.CommandText = selectBestDefender;

            reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                //Prendo solo la prima riga del risultato della query
                //(clausola TOP non supportata da SQLce)
                text += "Miglior difesa: " + Environment.NewLine;
                text += reader["NOME"] + " " + reader["CITTA"] + Environment.NewLine;
                text += "con " + reader["TOTPUNTI"] + " punti subiti" + Environment.NewLine;
            }

            return text;
        }

        private void chiudiTorneoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Owner.Visible = true;
            this.Close();
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Owner = null;
            Application.Exit();
        }

        private void btnInserisciPartita_Click(object sender, EventArgs e)
        {
            new Partita(this.idTorneo, this.anno).ShowDialog();
            this.ottieniInfo();
        }

        private void btnModificaPartita_Click(object sender, EventArgs e)
        {
            if (this.partiteGridView.SelectedRows.Count == 0)
            {
                ErrorDialog.Show("Errore", "Seleziona prima la partita da modificare!");
            }
            else
            {
                object[] infoPartita = this.partite[this.partiteGridView.SelectedRows[0].Index];
                int idPartita = (int)infoPartita[0];
                new Partita(idPartita, true).ShowDialog();
                this.ottieniInfo();
            }
        }

        private void GestioneTorneo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Owner != null)
            {
                this.Torneo = this.torneo.ID;
                this.DialogResult = DialogResult.OK;
                this.Owner.Visible = true;
            }
        }

        private void campiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaCampi().ShowDialog();
        }

        private void volontariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaPersone().ShowDialog();
        }

        private void refreshPartite()
        {
            this.partite.Clear();
            this.partiteGridView.Rows.Clear();

            //Retrieving matches
            string selectPartite = "SELECT ID, DATAORAPARTITA, IDSQUADRACASA, IDSQUADRAOSPITI, PUNTICASA, PUNTIOSPITI, IDCAMPO FROM PARTITA "
                + "WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOTORNEO";
            var cmd = DbConnection.Open(selectPartite);
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOTORNEO", this.anno);
            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                object[] datiPartita = new object[5];
                datiPartita[0] = reader.GetDateTime(1).ToString("dd/MM/yyyy HH:mm");
                datiPartita[2] = reader.IsDBNull(4) ? " - " : reader.GetInt16(4) + " - " + reader.GetInt16(5);

                string codSquadraCasa = reader["IDSQUADRACASA"].ToString();
                string codSquadraOspiti = reader["IDSQUADRAOSPITI"].ToString();
                int idPartita = int.Parse(reader["ID"].ToString());

                string selectNomeTeam = "SELECT NOME FROM SQUADRA WHERE ID = @ID";
                var cmd2 = DbConnection.Query(selectNomeTeam);
                cmd2.Parameters.AddWithValue("ID", codSquadraCasa);
                var reader2 = cmd2.ExecuteReader();
                if (reader2.Read())
                    datiPartita[1] = reader2.GetString(0);

                cmd2 = DbConnection.Query(selectNomeTeam);
                cmd2.Parameters.AddWithValue("ID", codSquadraOspiti);
                reader2 = cmd2.ExecuteReader();
                if (reader2.Read())
                    datiPartita[3] = reader2["NOME"];

                this.partiteGridView.Rows.Add(datiPartita);

                this.partite.Add(new object[] { idPartita });
            }
        }

        private void partiteGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            object[] infoPartita = this.partite[e.RowIndex];
            int idPartita = (int)infoPartita[0];
            new Partita(idPartita).Show();
        }

        private void GestioneTorneo_Load(object sender, EventArgs e)
        {
            PopulateDataGrid();

            if (this.gridPastPartite.Visible && !this.gridProxPartite.Visible)
            {
                this.gridPastPartite.Height *= 2;
            }
            else if (!this.gridPastPartite.Visible && this.gridProxPartite.Visible)
            {
                this.gridProxPartite.Height *= 2;
            }
        }

        // Funzione per la popolazione di staffDataGrid
        private void PopulateDataGrid()
        {
            try
            {

                string mansioniTorneo = "SELECT P.NOME, P.COGNOME, M.NOMEMANSIONE, M.DESCRIZIONE, "
                    + "T.DATAORAINIZIO, T.DATAORAFINE, T.IDPERSONA, T.IDMANSIONE "
                    + "FROM TURNO T, PERSONA P, MANSIONE M "
                    + "WHERE T.IDPERSONA = P.ID AND T.IDMANSIONE = M.ID AND IDTORNEO = @ID AND ANNOEDIZIONE = @ANNO";

                var cmd = DbConnection.Open(mansioniTorneo);
                cmd.Parameters.AddWithValue("ID", this.torneo.ID);
                cmd.Parameters.AddWithValue("ANNO", this.anno);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string[] row = { reader[6].ToString(), reader[0].ToString(), reader[1].ToString(),
                               reader[7].ToString(), reader[2].ToString(), reader[3].ToString(),
                               reader.GetDateTime(4).ToString("dd/MM/yyyy HH:mm"),
                               reader.GetDateTime(4).ToString("HH:mm dd/MM"), 
                               reader.GetDateTime(5).ToString("dd/MM/yyyy HH:mm"),
                               reader.GetDateTime(5).ToString("HH:mm dd/MM")};

                    staffDataGrid.Rows.Add(row);
                }
            }
            catch (Exception exc)
            {
                ErrorDialog.Show("Errore", exc.Message);
            }

            DbConnection.Close();

            return;
        }

        private void btnAggiungiStaff_Click(object sender, EventArgs e)
        {
            new InserisciTurno(this.torneo, this.anno).ShowDialog();
            gridStaffRefresh();
        }

        private void gridStaffRefresh()
        {
            staffDataGrid.Rows.Clear();
            PopulateDataGrid();
        }

        private void btnRimuoviStaff_Click(object sender, EventArgs e)
        {
            string persona = staffDataGrid.CurrentRow.Cells[1].Value.ToString() + " " 
                + staffDataGrid.CurrentRow.Cells[2].Value.ToString();
            string ora = staffDataGrid.CurrentRow.Cells[7].Value.ToString();

            DialogResult result = MessageBox.Show("Vuoli cancellare il turno di " + persona + " delle " + ora,
                "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.No)
                return;

            try
            {
                string idPersona = staffDataGrid.CurrentRow.Cells[0].Value.ToString();
                ora = staffDataGrid.CurrentRow.Cells[6].Value.ToString();

                string cancellaTurno = "DELETE FROM TURNO WHERE DATAORAINIZIO = @INIZIO AND IDPERSONA = @ID";

                var cmd = DbConnection.Open(cancellaTurno);
                cmd.Parameters.AddWithValue("INIZIO", ora);
                cmd.Parameters.AddWithValue("ID", idPersona);

                int affectedRows = cmd.ExecuteNonQuery();

                if (affectedRows != 1)
                {
                    ErrorDialog.Show("Problema cancellazione", "Non è stato possibile eliminare il turno.");
                }
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                DbConnection.Close();
                return;
            }

            DbConnection.Close();

            gridStaffRefresh();
        }

        private void btnModificaStaff_Click(object sender, EventArgs e)
        {
            string idPersona = staffDataGrid.CurrentRow.Cells[0].Value.ToString();
            string idMansione = staffDataGrid.CurrentRow.Cells[3].Value.ToString();
            string inizio = staffDataGrid.CurrentRow.Cells[6].Value.ToString();
            string fine = staffDataGrid.CurrentRow.Cells[8].Value.ToString();

            new InserisciTurno(this.torneo, this.anno, idPersona, idMansione, inizio, fine).ShowDialog();

            gridStaffRefresh();
        }

        private void squadreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaSquadre().ShowDialog();
        }

        private void mansioniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaMansioni().ShowDialog();
        }

        private void gridPastPartite_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            object[] infoPartita = this.partite[e.RowIndex];
            int idPartita = this.pastPartite[e.RowIndex];
            new Partita(idPartita).ShowDialog();
        }

        private void gridProxPartite_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            object[] infoPartita = this.partite[e.RowIndex];
            int idPartita = this.proxPartite[e.RowIndex];
            new Partita(idPartita).ShowDialog();
        }

        private void btnEliminaPartita_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Sei sicuro di volerla eliminare? Tutti i dati relativi andranno persi.", "Eliminazione partita", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                object[] infoPartita = this.partite[this.partiteGridView.SelectedRows[0].Index];
                int idPartita = (int)infoPartita[0];

                string deletePartita = "DELETE FROM PARTITA WHERE ID = @IDPARTITA";
                var cmd = DbConnection.Open(deletePartita);
                cmd.Parameters.AddWithValue("IDPARTITA", idPartita);
                var affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                    ErrorDialog.Show("Errore", "Errore nella cancellazione!");
                DbConnection.Close();
                this.refreshPartite();
            }
        }

        private void listFiltro_MouseClick(object sender, MouseEventArgs e)
        {
            gridStaffRefresh();
        }

        private void giocatoriIscrittiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaGiocatoriIscritti(this.idTorneo, this.anno).ShowDialog();
        }

        private void squadreIscritteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ListaSquadreIscritte(this.idTorneo, this.anno).ShowDialog();
        }

        private void modificaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ottieniInfo()
        {
            string selectTourData = "SELECT NOME "
                + "FROM SQUADRA WHERE ID = @IDSQUADRA";
            var cmd = DbConnection.Open(selectTourData);
            cmd.Parameters.AddWithValue("IDSQUADRA", this.torneo.Squadra);
            var reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                this.squadraTorneo = reader.GetString(0);
            }
            this.infoTorneo.Text = "Squadra organizzatrice: " + this.squadraTorneo + Environment.NewLine;

            // Cerco per le prossime partite
            DateTime today = DateTime.Now;

            try
            {
                //TODO recupero anche dell'arbitro
                string selectNextMatches = "SELECT DATAORAPARTITA, IDSQUADRACASA, IDSQUADRAOSPITI, ID "
                    + "FROM PARTITA "
                    + "WHERE DATAORAPARTITA > @OGGI AND IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOTORNEO "
                    + "ORDER BY DATAORAPARTITA";
                cmd = DbConnection.Query(selectNextMatches);
                cmd.CommandText = selectNextMatches;
                cmd.Parameters.AddWithValue("OGGI", today);
                cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
                cmd.Parameters.AddWithValue("ANNOTORNEO", this.anno);
                reader = cmd.ExecuteReader();
                if (reader.Read() && !reader.IsDBNull(0))
                {
                    var listProxPartite = new List<int>();
                    this.gridProxPartite.Rows.Clear();
                    do
                    {
                        listProxPartite.Add(int.Parse(reader[3].ToString()));
                        string[] row = { reader.GetDateTime(0).ToString("dd/MM/yyyy HH:mm"),
                                       reader.GetString(1),
                                       reader.GetString(2)};
                        this.gridProxPartite.Rows.Add(row);
                    } while (reader.Read() && !reader.IsDBNull(0));
                    this.proxPartite = listProxPartite;
                }
                else
                {
                    this.gridProxPartite.Visible = false;
                    this.txtProxPartite.Visible = false;
                    this.txtPastPartite.Height *= 2;
                }
            }
            catch (Exception exc)
            {
            }

            string selectPastMatches = "SELECT DATAORAPARTITA, IDCAMPO, IDSQUADRACASA, IDSQUADRAOSPITI, PUNTICASA, PUNTIOSPITI, ID "
                + "FROM PARTITA "
                + "WHERE DATAORAPARTITA < @OGGI AND IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOTORNEO "
                + "ORDER BY DATAORAPARTITA";
            cmd = DbConnection.Query(selectPastMatches);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("OGGI", today.ToString());
            cmd.Parameters.AddWithValue("IDTORNEO", this.idTorneo);
            cmd.Parameters.AddWithValue("ANNOTORNEO", this.anno);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                var listPastPartite = new List<int>();
                this.gridPastPartite.Rows.Clear();
                do
                {
                    object[] o = new object[7];
                    reader.GetValues(o);
                    o[4] = o[4] + " - " + o[5];
                    listPastPartite.Add(int.Parse(o[6].ToString()));
                    this.gridPastPartite.Rows.Add(o);
                } while (reader.Read());
                this.pastPartite = listPastPartite;
            }
            else
            {
                this.gridPastPartite.Visible = false;
                this.txtPastPartite.Visible = false;
            }

            //Refresh delle partite
            this.refreshPartite();

            DbConnection.Close();

            //TODO
            this.txtTourInfo.Text = "";
            string classifica = DbConnection.retrieveClassifica(this.torneo.ID, this.anno);
            if (classifica.StartsWith("1."))
            {
                this.txtTourInfo.Text = "Classifica: " + Environment.NewLine + Environment.NewLine;
                this.txtTourInfo.Text += classifica;
                this.txtBestTeams.Text = Environment.NewLine + this.retrieveBestTeams();
                this.txtBestTeams.Visible = true;
            }
            else
                this.txtTourInfo.Text += classifica;
        }
    }
}
