﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaGiocatoriIscritti : Form
    {
        private int torneoId;
        private string annoTorneo;
        private Dictionary<long, string[]> listMilita;
        private Dictionary<string, string> listSquadreEdizione;

        public ListaGiocatoriIscritti(int torneoId, string annoTorneo)
        {
            this.torneoId = torneoId;
            this.annoTorneo = annoTorneo;
            InitializeComponent();
        }

        private void ListaVolontari_Load(object sender, EventArgs e)
        {   
            //Recupero le squadre che possono essere scelte (da squadraedizione)
            string sqlSelectSquadreEdizione = "SELECT S.ID AS IDSQUADRA, S.NOME AS NOMESQUADRA FROM SQUADRA S, SQUADRAEDIZIONE SE WHERE S.ID = SE.IDSQUADRA AND SE.IDTORNEO = @IDTORNEO AND SE.ANNOEDIZIONE = @ANNOEDIZIONE";
            var cmd = DbConnection.Open(sqlSelectSquadreEdizione);
            cmd.Parameters.AddWithValue("IDTORNEO", this.torneoId);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
            var reader = cmd.ExecuteReader();

            this.listSquadreEdizione = new Dictionary<string, string>();
            this.listSquadreEdizione.Add("", "");
            while (reader.Read())
            {
                this.listSquadreEdizione.Add(reader["IDSQUADRA"].ToString(), reader["NOMESQUADRA"].ToString());
            }

            //Imposto il binding
            this.cbSquadraIscritto.DataSource = new BindingSource(this.listSquadreEdizione, null);
            this.cbSquadraIscritto.ValueMember = "Key";
            this.cbSquadraIscritto.DisplayMember = "Value";

            gridRefresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridRefresh()
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.PERSONA'. È possibile spostarla o rimuoverla se necessario.
            this.pERSONATableAdapter.Fill(this.baseDataDataSet.PERSONA);

            //Una volta riempita la tabella, carico tramite select manuale squadra e numero precedentemente salvati
            string sqlSelectMilita = "SELECT IDPERSONA, IDSQUADRA, NUMERO FROM MILITA WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE";
            var cmd = DbConnection.Open(sqlSelectMilita);
            cmd.Parameters.AddWithValue("IDTORNEO", this.torneoId);
            cmd.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);

            var reader = cmd.ExecuteReader();

            this.listMilita = new Dictionary<long, string[]>();
            while (reader.Read())
            {
                listMilita.Add(reader.GetInt64(0), new string[] { reader["IDSQUADRA"].ToString(), reader["NUMERO"].ToString() });
            }

            //Cerco l'id nella tabella
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                var idPersonaTable = int.Parse(row.Cells["iDDataGridViewTextBoxColumn"].Value.ToString());
                if (listMilita.ContainsKey(idPersonaTable))
                {
                    var arrayDati = listMilita[idPersonaTable];
                    row.Cells["cbSquadraIscritto"].Value = arrayDati[0];
                    row.Cells["txtNumero"].Value = arrayDati[1];
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Salva i "milita"
            try
            {
                //Creo la nuova lista di giocatori
                var newListMilita = new Dictionary<long, string[]>(); ;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    var idSquadra = row.Cells["cbSquadraIscritto"].Value;
                    var numMaglia = row.Cells["txtNumero"].Value;
                    if (idSquadra != null && idSquadra.ToString() != ""
                        && numMaglia != null && numMaglia.ToString() != "")
                    {
                        var idPersona = long.Parse(row.Cells["iDDataGridViewTextBoxColumn"].Value.ToString());
                        newListMilita.Add(idPersona, new string[] { idSquadra.ToString(), numMaglia.ToString() });
                    }
                }

                //Preparo le query
                string sqlDelete = "DELETE FROM MILITA WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDPERSONA = @IDPERSONA";
                var cmdDelete = DbConnection.Open(sqlDelete);
                cmdDelete.Parameters.AddWithValue("IDTORNEO", this.torneoId);
                cmdDelete.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                cmdDelete.Parameters.AddWithValue("IDPERSONA", null);

                string sqlInsert = "INSERT INTO MILITA VALUES (@IDPERSONA, @IDTORNEO, @ANNOEDIZIONE, @IDSQUADRA, @NUMERO)";
                var cmdInsert = DbConnection.Open(sqlInsert);
                cmdInsert.Parameters.AddWithValue("IDTORNEO", this.torneoId);
                cmdInsert.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                cmdInsert.Parameters.AddWithValue("IDPERSONA", null);
                cmdInsert.Parameters.AddWithValue("IDSQUADRA", null);
                cmdInsert.Parameters.AddWithValue("NUMERO", null);

                string sqlUpdate = "UPDATE MILITA SET IDSQUADRA = @IDSQUADRA, NUMERO = @NUMERO WHERE IDTORNEO = @IDTORNEO AND ANNOEDIZIONE = @ANNOEDIZIONE AND IDPERSONA = @IDPERSONA";
                var cmdUpdate = DbConnection.Open(sqlUpdate);
                cmdUpdate.Parameters.AddWithValue("IDTORNEO", this.torneoId);
                cmdUpdate.Parameters.AddWithValue("ANNOEDIZIONE", this.annoTorneo);
                cmdUpdate.Parameters.AddWithValue("IDPERSONA", null);
                cmdUpdate.Parameters.AddWithValue("IDSQUADRA", null);
                cmdUpdate.Parameters.AddWithValue("NUMERO", null);


                //Cancello tutti i milita non più iscritti in nessuna squadra
                foreach (var idPersona in this.listMilita.Keys)
                {
                    if (!newListMilita.ContainsKey(idPersona))
                    {
                        cmdDelete.Parameters["IDPERSONA"].Value = idPersona;
                        cmdDelete.ExecuteNonQuery();
                    }
                }

                //Inserisco o aggiorno tutti i milita iscritti
                foreach (var idPersona in newListMilita.Keys)
                {
                    var arrayDati = newListMilita[idPersona];   //Recupero i dati della persona dal dictionary
                    if (!this.listMilita.ContainsKey(idPersona))
                    {
                        //Effettuo l'inserimento
                        cmdInsert.Parameters["IDPERSONA"].Value = idPersona;
                        cmdInsert.Parameters["IDSQUADRA"].Value = arrayDati[0];
                        cmdInsert.Parameters["NUMERO"].Value = arrayDati[1];
                        cmdInsert.ExecuteNonQuery();
                    }
                    else
                    {
                        //Effettuo l'aggiornamento
                        cmdUpdate.Parameters["IDPERSONA"].Value = idPersona;
                        cmdUpdate.Parameters["IDSQUADRA"].Value = arrayDati[0];
                        cmdUpdate.Parameters["NUMERO"].Value = arrayDati[1];
                        cmdUpdate.ExecuteNonQuery();
                    }
                }

                this.listMilita = newListMilita;

                DbConnection.Close();
                gridRefresh();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }
        }
    }
}
