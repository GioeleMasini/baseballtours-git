﻿namespace BaseballTours
{
    partial class Welcome
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Welcome));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.nuovoTour = new System.Windows.Forms.LinkLabel();
            this.listAnni = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listTournaments = new System.Windows.Forms.ListBox();
            this.txtInfoTorneo = new System.Windows.Forms.Label();
            this.selSquadra = new System.Windows.Forms.Label();
            this.carica = new System.Windows.Forms.Button();
            this.selData = new System.Windows.Forms.Label();
            this.selNomeTour = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.organizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.squadreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.campiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.volontariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mansioniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pictureBox1);
            this.flowLayoutPanel1.Controls.Add(this.splitContainer1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(14, 34);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(730, 446);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(665, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 130);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel2);
            this.splitContainer1.Panel1.Controls.Add(this.listAnni);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.listTournaments);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("splitContainer1.Panel2.BackgroundImage")));
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.splitContainer1.Panel2.Controls.Add(this.txtInfoTorneo);
            this.splitContainer1.Panel2.Controls.Add(this.selSquadra);
            this.splitContainer1.Panel2.Controls.Add(this.carica);
            this.splitContainer1.Panel2.Controls.Add(this.selData);
            this.splitContainer1.Panel2.Controls.Add(this.selNomeTour);
            this.splitContainer1.Size = new System.Drawing.Size(719, 308);
            this.splitContainer1.SplitterDistance = 469;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 3;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.linkLabel1);
            this.flowLayoutPanel2.Controls.Add(this.linkLabel2);
            this.flowLayoutPanel2.Controls.Add(this.label3);
            this.flowLayoutPanel2.Controls.Add(this.nuovoTour);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(10, 278);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(428, 27);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel1.Location = new System.Drawing.Point(0, 0);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(96, 24);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Modifica,";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel2.Location = new System.Drawing.Point(96, 0);
            this.linkLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(76, 24);
            this.linkLabel2.TabIndex = 5;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "elimina";
            this.linkLabel2.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(172, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "o aggiungine uno";
            // 
            // nuovoTour
            // 
            this.nuovoTour.AutoSize = true;
            this.nuovoTour.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuovoTour.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.nuovoTour.Location = new System.Drawing.Point(346, 0);
            this.nuovoTour.Margin = new System.Windows.Forms.Padding(0);
            this.nuovoTour.Name = "nuovoTour";
            this.nuovoTour.Size = new System.Drawing.Size(76, 24);
            this.nuovoTour.TabIndex = 6;
            this.nuovoTour.TabStop = true;
            this.nuovoTour.Text = "nuovo!";
            this.nuovoTour.VisitedLinkColor = System.Drawing.Color.Blue;
            this.nuovoTour.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.nuovoTour_LinkClicked_1);
            // 
            // listAnni
            // 
            this.listAnni.Font = new System.Drawing.Font("Arial", 10F);
            this.listAnni.FormattingEnabled = true;
            this.listAnni.ItemHeight = 19;
            this.listAnni.Location = new System.Drawing.Point(324, 49);
            this.listAnni.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listAnni.Name = "listAnni";
            this.listAnni.Size = new System.Drawing.Size(106, 194);
            this.listAnni.TabIndex = 2;
            this.listAnni.SelectedIndexChanged += new System.EventHandler(this.listAnni_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Carica un torneo...";
            // 
            // listTournaments
            // 
            this.listTournaments.Font = new System.Drawing.Font("Arial", 10F);
            this.listTournaments.FormattingEnabled = true;
            this.listTournaments.ItemHeight = 19;
            this.listTournaments.Location = new System.Drawing.Point(10, 49);
            this.listTournaments.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTournaments.Name = "listTournaments";
            this.listTournaments.Size = new System.Drawing.Size(306, 194);
            this.listTournaments.TabIndex = 1;
            this.listTournaments.SelectedIndexChanged += new System.EventHandler(this.listTournament_SelectedIndexChanged);
            // 
            // txtInfoTorneo
            // 
            this.txtInfoTorneo.AutoSize = true;
            this.txtInfoTorneo.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic);
            this.txtInfoTorneo.Location = new System.Drawing.Point(3, 144);
            this.txtInfoTorneo.MaximumSize = new System.Drawing.Size(240, 0);
            this.txtInfoTorneo.MinimumSize = new System.Drawing.Size(240, 0);
            this.txtInfoTorneo.Name = "txtInfoTorneo";
            this.txtInfoTorneo.Size = new System.Drawing.Size(240, 40);
            this.txtInfoTorneo.TabIndex = 0;
            this.txtInfoTorneo.Text = "Torneo in corso/primi classificati";
            // 
            // selSquadra
            // 
            this.selSquadra.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic);
            this.selSquadra.Location = new System.Drawing.Point(22, 96);
            this.selSquadra.Name = "selSquadra";
            this.selSquadra.Size = new System.Drawing.Size(211, 21);
            this.selSquadra.TabIndex = 0;
            this.selSquadra.Text = "Squadra ospitante";
            this.selSquadra.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // carica
            // 
            this.carica.Location = new System.Drawing.Point(133, 254);
            this.carica.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.carica.Name = "carica";
            this.carica.Size = new System.Drawing.Size(87, 28);
            this.carica.TabIndex = 3;
            this.carica.Text = "Apri";
            this.carica.UseVisualStyleBackColor = true;
            this.carica.Click += new System.EventHandler(this.carica_Click);
            // 
            // selData
            // 
            this.selData.AutoSize = true;
            this.selData.Font = new System.Drawing.Font("Arial", 10F);
            this.selData.Location = new System.Drawing.Point(140, 38);
            this.selData.Name = "selData";
            this.selData.Size = new System.Drawing.Size(91, 19);
            this.selData.TabIndex = 0;
            this.selData.Text = "01/01/1990";
            // 
            // selNomeTour
            // 
            this.selNomeTour.Font = new System.Drawing.Font("Arial", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.selNomeTour.Location = new System.Drawing.Point(3, 63);
            this.selNomeTour.Name = "selNomeTour";
            this.selNomeTour.Size = new System.Drawing.Size(238, 33);
            this.selNomeTour.TabIndex = 0;
            this.selNomeTour.Text = "NomeTorneoSelezionato";
            this.selNomeTour.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.organizzaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(753, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // organizzaToolStripMenuItem
            // 
            this.organizzaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visualizzaToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.organizzaToolStripMenuItem.Name = "organizzaToolStripMenuItem";
            this.organizzaToolStripMenuItem.Size = new System.Drawing.Size(88, 24);
            this.organizzaToolStripMenuItem.Text = "Organizza";
            // 
            // visualizzaToolStripMenuItem
            // 
            this.visualizzaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.squadreToolStripMenuItem,
            this.campiToolStripMenuItem,
            this.volontariToolStripMenuItem,
            this.mansioniToolStripMenuItem});
            this.visualizzaToolStripMenuItem.Name = "visualizzaToolStripMenuItem";
            this.visualizzaToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.visualizzaToolStripMenuItem.Text = "Visualizza";
            // 
            // squadreToolStripMenuItem
            // 
            this.squadreToolStripMenuItem.Name = "squadreToolStripMenuItem";
            this.squadreToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.squadreToolStripMenuItem.Text = "Squadre";
            this.squadreToolStripMenuItem.Click += new System.EventHandler(this.squadreToolStripMenuItem_Click);
            // 
            // campiToolStripMenuItem
            // 
            this.campiToolStripMenuItem.Name = "campiToolStripMenuItem";
            this.campiToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.campiToolStripMenuItem.Text = "Campi";
            this.campiToolStripMenuItem.Click += new System.EventHandler(this.campiToolStripMenuItem_Click);
            // 
            // volontariToolStripMenuItem
            // 
            this.volontariToolStripMenuItem.Name = "volontariToolStripMenuItem";
            this.volontariToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.volontariToolStripMenuItem.Text = "Persone";
            this.volontariToolStripMenuItem.Click += new System.EventHandler(this.volontariToolStripMenuItem_Click);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.esciToolStripMenuItem_Click);
            // 
            // mansioniToolStripMenuItem
            // 
            this.mansioniToolStripMenuItem.Name = "mansioniToolStripMenuItem";
            this.mansioniToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.mansioniToolStripMenuItem.Text = "Mansioni";
            this.mansioniToolStripMenuItem.Click += new System.EventHandler(this.mansioniToolStripMenuItem_Click);
            // 
            // Welcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(753, 483);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Welcome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baseball Tournaments - Welcome";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Welcome_FormClosing);
            this.Load += new System.EventHandler(this.Welcome_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listTournaments;
        private System.Windows.Forms.ListBox listAnni;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel nuovoTour;
        private System.Windows.Forms.Label selNomeTour;
        private System.Windows.Forms.Label txtInfoTorneo;
        private System.Windows.Forms.Label selSquadra;
        private System.Windows.Forms.Button carica;
        private System.Windows.Forms.Label selData;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem organizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem campiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volontariToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem squadreToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.ToolStripMenuItem mansioniToolStripMenuItem;
    }
}

