﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaCampi : Form
    {
        public ListaCampi()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InserisciCampo newCampo = new InserisciCampo();
            newCampo.ShowDialog();
            gridRefresh();
        }

        private void ListaCampi_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.CAMPO'. È possibile spostarla o rimuoverla se necessario.
            this.cAMPOTableAdapter.Fill(this.baseDataDataSet.CAMPO);

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            new InserisciCampo(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value)).ShowDialog();
            gridRefresh();
        }

        private void gridRefresh()
        {
            this.cAMPOTableAdapter.Fill(this.baseDataDataSet.CAMPO);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new InserisciCampo(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value)).ShowDialog();
            gridRefresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string search = "SELECT COUNT(*) PARTITA WHERE "
                + "IDCAMPO = @ID";

            var cmd = DbConnection.Open(search);
            cmd.CommandText = search;
            cmd.Parameters.AddWithValue("ID", Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value));
            var reader = cmd.ExecuteReader();
            reader.Read();

            if (reader.GetInt32(0) > 0)
            {
                //ERROR
                ErrorDialog.Show("Errore", DbConnection.UNDELETABLE);
                return;
            }
            else
            {
                string deleteCampo = "DELETE FROM CAMPO WHERE ID = @ID";

                cmd = DbConnection.Open(deleteCampo);
                cmd.CommandText = deleteCampo;
                cmd.Parameters.AddWithValue("ID", Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value));

                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                }
            }

            cmd.Parameters.Clear();
            DbConnection.Close();

            gridRefresh();
        }
    }
}
