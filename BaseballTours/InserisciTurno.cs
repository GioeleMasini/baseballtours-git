﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class InserisciTurno : Form
    {
        Boolean editable = false;
        String insertTurno;
        Torneo torneo = null;
        string anno;
        string nomeMansione;
        string idMansione;
        string idMansioneToEdit;
        string idPersona;
        string idPersonaToEdit;
        string sqlOraInizio;
        string sqlOraFine;
        string inizioToEdit;
        string fineToEdit;
        System.Data.SqlServerCe.SqlCeCommand cmd;
        System.Data.SqlServerCe.SqlCeDataReader reader;

        public InserisciTurno(Torneo torneo, string torneoAnno)
        {
            InitializeComponent();
            this.torneo = torneo;
            this.nomeTorneo.Text = this.torneo.Nome;
            this.nomeTorneo.Enabled = false;
            this.annoTorneo.Text = torneoAnno;
            this.annoTorneo.Enabled = false;
            this.anno = torneoAnno;

            this.impostaDate();
        }

        //nuovo construttore per modifica turno
        public InserisciTurno(Torneo torneo, string anno, string idPersona, string idMansione, string dataInizio, string dataFine)
        {
            InitializeComponent();

            this.torneo = torneo;
            this.nomeTorneo.Text = this.torneo.Nome;
            this.nomeTorneo.Enabled = false;
            this.annoTorneo.Text = anno;
            this.annoTorneo.Enabled = false;
            this.anno = anno;

            this.idPersonaToEdit = idPersona;
            this.idMansioneToEdit = idMansione;
            this.inizioToEdit = dataInizio;
            this.fineToEdit = dataFine;

            this.oraInizio.Value = Convert.ToDateTime(inizioToEdit);
            this.oraFine.Value = Convert.ToDateTime(fineToEdit);

            this.editable = true;
        }

        private void GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Resetta_Click(object sender, EventArgs e)
        {
            mansione.ResetText();
            persona.ResetText();
            oraFine.ResetText();
            oraInizio.ResetText();
        }

        private void Salva_Click(object sender, EventArgs e)
        {
            //Controllo presenza valori
            if (this.mansione.Text.Equals("") || this.persona.Text.Equals(""))
            {
                ErrorDialog.Show("Form incompleto", "Controllare di aver inserito tutti i valori.");
                return;
            }

            //controllo validità orario
            if (oraInizio.Value >= oraFine.Value)
            {
                ErrorDialog.Show("Orario invalido", "L'ora di fine non può essere antecedente o uguale all'ora di inizio.");
                return;
            }
            else if (!oraInizio.Value.Year.ToString().Equals(this.anno)
                || !oraFine.Value.Year.ToString().Equals(this.anno))
            {
                ErrorDialog.Show("Anno invalido", "L'anno non può essere diverso da quello del torneo.");
                return;
            }

            nomeMansione = mansione.Text;
            sqlOraInizio = oraInizio.Value.ToString("yyyy-MM-dd HH:mm");
            sqlOraFine = oraFine.Value.ToString("yyyy-MM-dd HH:mm");
            idPersona = (persona.SelectedItem as Item).Value.ToString();
            idMansione = (mansione.SelectedItem as Item).Value.ToString();

            // Controllo presenza di tuple con lo stesso valore di chiave
            if (!editable)
            {
                string checkKey = "SELECT * FROM TURNO WHERE DATAORAINIZIO = @ORA AND IDPERSONA = @PERSONA";

                cmd = DbConnection.Open(checkKey);
                cmd.Parameters.AddWithValue("ORA", sqlOraInizio);
                cmd.Parameters.AddWithValue("PERSONA", idPersona);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    // Errore, tupla con valore di chiave già presente
                    ErrorDialog.Show("Errore di chiave", "Questa persona ha già un turno per quel periodo di tempo.\n"
                        + "Inserire un turno con valori di chiave validi");
                    return;
                }
                else
                {
                    // Nessuna tupla con quella chiave, può continuare, ripulisco il cmd.
                    cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }
            else
            {
                string checkKey = "SELECT * FROM TURNO WHERE DATAORAINIZIO = @ORA AND IDPERSONA = @PERSONA "
                    + "AND IDPERSONA <> @PERSONAPRIMA";

                cmd = DbConnection.Open(checkKey);
                cmd.Parameters.AddWithValue("ORA", sqlOraInizio);
                cmd.Parameters.AddWithValue("PERSONA", idPersona);
                cmd.Parameters.AddWithValue("PERSONAPRIMA", idPersonaToEdit);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    // Errore, tupla con valore di chiave già presente
                    ErrorDialog.Show("Errore di chiave", "Questa persona ha già un turno per quel periodo di tempo.\n"
                        + "Inserire un turno con valori di chiave validi");
                    return;
                }
                else
                {
                    // Nessuna tupla con quella chiave, può continuare, ripulisco il cmd.
                    cmd.Parameters.Clear();
                }

                DbConnection.Close();
            }

            try
            {
                if (!editable)
                {
                    //controllo se la persona è già occupata
                    if (DbConnection.occupato(sqlOraInizio, idPersona))
                    {
                        ErrorDialog.Show("Errore temporale", "Questa persona ha già un turno in quell'intervallo di tempo.\n"
                                + "Inserire un turno con valori temporali validi.");
                        return;
                    }

                    insertTurno = "INSERT INTO TURNO (DATAORAINIZIO, IDPERSONA, DATAORAFINE, IDMANSIONE, IDTORNEO, ANNOEDIZIONE)"
                        + "VALUES(@INIZIO, @PERSONA, @FINE, @MANSIONE, @TORNEO, @ANNO)";

                    cmd = DbConnection.Open(insertTurno);
                    cmd.Parameters.AddWithValue("TORNEO", this.torneo.ID);
                    cmd.Parameters.AddWithValue("ANNO", this.anno);
                }
                else
                {
                    insertTurno = "UPDATE TURNO SET IDMANSIONE = @MANSIONE, IDPERSONA = @PERSONA, "
                        + "DATAORAINIZIO = @INIZIO, DATAORAFINE = @FINE "
                        + "WHERE DATAORAINIZIO = @INIZIOPRIMA AND IDPERSONA = @PERSONAPRIMA";

                    cmd = DbConnection.Open(insertTurno);
                    cmd.Parameters.AddWithValue("INIZIOPRIMA", inizioToEdit);
                    cmd.Parameters.AddWithValue("PERSONAPRIMA", idPersonaToEdit);
                }

                cmd.CommandText = insertTurno;
                cmd.Parameters.AddWithValue("MANSIONE", idMansione);
                cmd.Parameters.AddWithValue("PERSONA", idPersona);
                cmd.Parameters.AddWithValue("INIZIO", sqlOraInizio);
                cmd.Parameters.AddWithValue("FINE", sqlOraFine);

                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                }

                DbConnection.Close();
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }

            this.Close();
            return;
        }

        private void InserisciTurno_Load(object sender, EventArgs e)
        {
            Item temp;

            // Recupero lista persone
            string recuperaPersone = "SELECT ID, NOME, COGNOME, DESCRIZIONE FROM PERSONA ORDER BY NOME, COGNOME";

            this.cmd = DbConnection.Open(recuperaPersone);

            this.cmd.CommandText = recuperaPersone;
            this.reader = this.cmd.ExecuteReader();

            while (this.reader.Read())
            {
                temp = new Item();
                temp.Text = reader[1].ToString() + " " + reader[2].ToString();
                temp.Value = reader[0].ToString();
                temp.Descr = reader[3].ToString();
                this.persona.Items.Add(temp);

                if (editable && temp.Value.Equals(this.idPersonaToEdit))
                    this.persona.SelectedItem = temp;
            }

            this.cmd.Parameters.Clear();

            // Recupero lista mansioni
            string recuperaMansioni = "SELECT ID, NOMEMANSIONE, DESCRIZIONE FROM MANSIONE ORDER BY NOMEMANSIONE";

            this.cmd.CommandText = recuperaMansioni;
            this.reader = this.cmd.ExecuteReader();

            while (this.reader.Read())
            {
                temp = new Item();
                temp.Text = reader[1].ToString();
                temp.Value = reader[0].ToString();
                temp.Descr = reader[2].ToString();
                this.mansione.Items.Add(temp);

                if (editable && temp.Value.Equals(this.idMansioneToEdit))
                    this.mansione.SelectedItem = temp;
            }

            this.cmd.Parameters.Clear();

            DbConnection.Close();
        }

        private void mansione_MouseHover(object sender, EventArgs e)
        {
            if (this.mansione.SelectedItem != null)
                this.visualizzaInfo.SetToolTip(this.mansione, (this.mansione.SelectedItem as Item).Descr);
        }

        private void persona_MouseHover(object sender, EventArgs e)
        {
            if (this.persona.SelectedItem != null && !(this.persona.SelectedItem as Item).Descr.Equals(""))
                this.visualizzaInfo.SetToolTip(this.persona, (this.persona.SelectedItem as Item).Descr);
        }

        private void impostaDate() 
        {
            string sql = "SELECT MIN(DATAORAPARTITA) FROM PARTITA "
                + "WHERE IDTORNEO = @ID AND ANNOEDIZIONE = @EDIZIONE";

            var cmd = DbConnection.Open(sql);
            cmd.Parameters.AddWithValue("ID", this.torneo.ID);
            cmd.Parameters.AddWithValue("EDIZIONE", this.anno);

            var reader = cmd.ExecuteReader();

            if (reader.Read() && !reader.IsDBNull(0))
            {
                this.oraInizio.Value = Convert.ToDateTime(reader[0]);
                this.oraFine.Value = Convert.ToDateTime(reader[0]);
            }
            else
            {
                DateTime ora = DateTime.Now;
                this.oraInizio.Value = new DateTime(int.Parse(this.anno), ora.Month, ora.Day, ora.Hour, ora.Minute, 0);
            }
        }
    }
}
