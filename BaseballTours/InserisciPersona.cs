﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class InserisciPersona : Form
    {
        Boolean editable = false;
        String insertPersona;
        string nome;
        string cognome;
        string descrizione;
        string sqlDataNascita;
        Persona persona = null;
        System.Data.SqlServerCe.SqlCeCommand cmd;
        System.Data.SqlServerCe.SqlCeDataReader reader;

        public InserisciPersona()
        {
            InitializeComponent();
            dataPersona.Value = DateTime.Now;
        }

        public InserisciPersona(long personaID)
        {
            InitializeComponent();

            string selection = "SELECT * FROM PERSONA "
                + "WHERE ID = @ID";

            cmd = DbConnection.Open(selection);

            cmd.CommandText = selection;

            cmd.Parameters.AddWithValue("ID", personaID);

            reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                persona = new Persona();
                persona.ID = personaID;
                persona.Nome = reader[1].ToString();
                persona.Cognome = reader[2].ToString();
                persona.DataNascita = reader.GetDateTime(3);
                persona.Descrizione = reader[4].ToString();
                /*nomePersona.Text = reader[0].ToString();
                cognomePersona.Text = reader[1].ToString();
                dataPersona.Value = reader.GetDateTime(2);*/
            }

            nomePersona.Text = persona.Nome;
            cognomePersona.Text = persona.Cognome;
            dataPersona.Value = persona.DataNascita;
            descrizionePersona.Text = persona.Descrizione;

            cmd.Parameters.Clear();
            DbConnection.Close();

            editable = true;
            this.label1.Text = "Modifica di " + persona.Nome + " " + persona.Cognome;
        }

        private void GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InserisciPersona_Load(object sender, EventArgs e)
        {
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            nomePersona.Refresh();
            cognomePersona.Refresh();
            dataPersona.Refresh();
            descrizionePersona.Refresh();
        }

        private void Salva_Click(object sender, EventArgs e)
        {            
            // Controllo presenza valori nei campi
            if (nomePersona.Text.Equals("") || cognomePersona.Text.Equals("") || dataPersona.Value == DateTime.Now)
            {
                ErrorDialog.Show("Errore", "Inserire tutti i valori per tutti i campi");
                return;
            }

            //controllo presenza di omonimi o omonimi con descrizione uguale
            //TODO cambiare controllo, se è già presente uno con lo stesso nome+cognome, far aggiungere la descrizione
            string controlloOmonimo = "SELECT * FROM PERSONA WHERE NOME = @NOME AND COGNOME = @COGNOME";

            if (editable)
                controlloOmonimo += " AND ID <> @ID";

            cmd = DbConnection.Open(controlloOmonimo);

            cmd.CommandText = controlloOmonimo;

            cmd.Parameters.AddWithValue("NOME", nomePersona.Text);
            cmd.Parameters.AddWithValue("COGNOME", cognomePersona.Text);

            if (editable)
                cmd.Parameters.AddWithValue("ID", persona.ID);

            reader = cmd.ExecuteReader();

            //possono essere presenti più omonimi
            while (reader.Read())
            {
                //omonimo presente

                if (reader[4].ToString().Equals(descrizionePersona.Text))
                {
                    ErrorDialog.Show("Omonimia", "Omonimo presente. Aggiungere un valore nel campo descrizione in modo da riuscire a distinguere le persone.");
                    return;
                }
            }

            // Nessun omonimo
            cmd.Parameters.Clear();

            DbConnection.Close();

            nome = nomePersona.Text;
            cognome = cognomePersona.Text;
            descrizione = descrizionePersona.Text;
            sqlDataNascita = dataPersona.Value.ToString("yyyy-MM-dd");

            try
            {
                if (!editable)
                {
                    insertPersona = "INSERT INTO PERSONA(NOME,COGNOME, DATANASCITA, DESCRIZIONE) "
                        + "VALUES (@NOME, @COGNOME, @DATANASCITA, @DESCRIZIONE)";

                    cmd = DbConnection.Open(insertPersona);
                }
                else
                {
                    //cambiare condizione where
                    insertPersona = "UPDATE PERSONA SET NOME = @NOME, COGNOME = @COGNOME, "
                        + "DATANASCITA = @DATANASCITA, DESCRIZIONE = @DESCRIZIONE WHERE ID = @ID";

                    cmd = DbConnection.Open(insertPersona);

                    cmd.Parameters.AddWithValue("ID", persona.ID);
                }

                cmd.CommandText = insertPersona;
                cmd.Parameters.AddWithValue("NOME", nome);
                cmd.Parameters.AddWithValue("COGNOME", cognome);
                cmd.Parameters.AddWithValue("DATANASCITA", sqlDataNascita);
                cmd.Parameters.AddWithValue("DESCRIZIONE", descrizione);

                int affectedRows = cmd.ExecuteNonQuery();

                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                    return;
                }
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }

            DbConnection.Close();
            this.Close();
        }
    }
}
