﻿namespace BaseballTours
{
    partial class InserisciMansione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciMansione));
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.descrizioneMansione = new System.Windows.Forms.TextBox();
            this.nomeMansione = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Salva = new System.Windows.Forms.Button();
            this.Resetta = new System.Windows.Forms.Button();
            this.GoBack = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserisci nuova mansione";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.53489F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.46511F));
            this.tableLayoutPanel1.Controls.Add(this.descrizioneMansione, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.nomeMansione, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 70);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(324, 162);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // descrizioneMansione
            // 
            this.descrizioneMansione.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.descrizioneMansione.Location = new System.Drawing.Point(130, 46);
            this.descrizioneMansione.Margin = new System.Windows.Forms.Padding(2);
            this.descrizioneMansione.Multiline = true;
            this.descrizioneMansione.Name = "descrizioneMansione";
            this.descrizioneMansione.Size = new System.Drawing.Size(191, 107);
            this.descrizioneMansione.TabIndex = 2;
            // 
            // nomeMansione
            // 
            this.nomeMansione.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nomeMansione.Location = new System.Drawing.Point(130, 5);
            this.nomeMansione.Margin = new System.Windows.Forms.Padding(2);
            this.nomeMansione.Name = "nomeMansione";
            this.nomeMansione.Size = new System.Drawing.Size(191, 27);
            this.nomeMansione.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nome";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 90);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Descrizione";
            // 
            // Salva
            // 
            this.Salva.Location = new System.Drawing.Point(240, 267);
            this.Salva.Margin = new System.Windows.Forms.Padding(2);
            this.Salva.Name = "Salva";
            this.Salva.Size = new System.Drawing.Size(89, 40);
            this.Salva.TabIndex = 3;
            this.Salva.Text = "Salva";
            this.Salva.UseVisualStyleBackColor = true;
            this.Salva.Click += new System.EventHandler(this.Salva_Click);
            // 
            // Resetta
            // 
            this.Resetta.Location = new System.Drawing.Point(124, 267);
            this.Resetta.Margin = new System.Windows.Forms.Padding(2);
            this.Resetta.Name = "Resetta";
            this.Resetta.Size = new System.Drawing.Size(93, 40);
            this.Resetta.TabIndex = 4;
            this.Resetta.Text = "Resetta campi";
            this.Resetta.UseVisualStyleBackColor = true;
            this.Resetta.Click += new System.EventHandler(this.Resetta_Click);
            // 
            // GoBack
            // 
            this.GoBack.Location = new System.Drawing.Point(11, 267);
            this.GoBack.Margin = new System.Windows.Forms.Padding(2);
            this.GoBack.Name = "GoBack";
            this.GoBack.Size = new System.Drawing.Size(89, 40);
            this.GoBack.TabIndex = 5;
            this.GoBack.Text = "Annulla";
            this.GoBack.UseVisualStyleBackColor = true;
            this.GoBack.Click += new System.EventHandler(this.GoBack_Click);
            // 
            // InserisciMansione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 318);
            this.Controls.Add(this.GoBack);
            this.Controls.Add(this.Resetta);
            this.Controls.Add(this.Salva);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "InserisciMansione";
            this.Text = "Baseball Tournaments - Inserisci mansione";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox descrizioneMansione;
        private System.Windows.Forms.TextBox nomeMansione;
        private System.Windows.Forms.Button Salva;
        private System.Windows.Forms.Button Resetta;
        private System.Windows.Forms.Button GoBack;
    }
}