﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseballTours
{
    class Item
    {
        public string Text { get; set; }
        public object Value { get; set; }
        public string Descr { get; set; }

        public Item()
        {
        }

        public Item(string text, object value)
            : this()
        {
            this.Text = text;
            this.Value = value;
        }

        public Item(string text, object value, string desc)
            : this()
        {
            this.Text = text;
            this.Value = value;
            this.Descr = desc;
        }

        public override string ToString()
        {
            return Text;
        }

        public override bool Equals(object obj)
        {
            string text = obj.ToString();
            if (text == null)
                return false;

            return this.Text.Equals(text);
        }

        public override int GetHashCode()
        {
            return this.Text.GetHashCode();
        }
    }
}
