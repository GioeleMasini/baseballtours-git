﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace BaseballTours
{
    public partial class Squadra : Form
    {
        private PrivateFontCollection pfc = null;
        private Boolean firstTimeDraw = true;

        private Color coloreSquadra;
        private string codSquadra;

        public Squadra(string idSquadra)
        {
            InitializeComponent();

            this.codSquadra = idSquadra;

            //Leggo i dati della squadra
            string selectSquadra = "SELECT * FROM SQUADRA WHERE ID = @ID";
            var cmd = DbConnection.Open(selectSquadra);
            cmd.Parameters.AddWithValue("ID", idSquadra);
            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                this.txtNomeSquadra.Text = reader["NOME"].ToString();
                this.txtCittaSquadra.Text = reader["CITTA"].ToString();
                this.coloreSquadra = Utilities.colorFromHex(reader["COLORE"].ToString());
                try
                {
                    this.imgSquadra.Image = Image.FromFile(reader["LOGO"].ToString());
                }
                catch (System.IO.FileNotFoundException) { }
            }
            DbConnection.Close();

        }

        private void drawShirtOnCanvas(Color c)
        {
            Color backgroundColor = Color.White;

            Graphics g = this.canvas.CreateGraphics();
            g.Clear(backgroundColor);
            g.SmoothingMode = SmoothingMode.HighQuality;    //Imposto l'antialising (toglie l'effetto pixellato)
            Brush brush = new SolidBrush(c);

            //Disegno la maglietta
            int height = this.canvas.Height;
            int width = this.canvas.Width;
            int centerWidth = 100;
            int bordTop = 20;
            Rectangle rCentro = new Rectangle((width - centerWidth) / 2, bordTop, centerWidth, height - bordTop);
            Rectangle rManicaDx = new Rectangle(width - 65, 35, 60, 45);
            Rectangle rManicaSx = new Rectangle(6, 36, 60, 45);
            Rectangle rTesta = new Rectangle((width - 60) / 2, -20, 60, 50);

            this.RotateRectangle(g, rManicaDx, 45, brush);
            this.RotateRectangle(g, rManicaSx, 135, brush);

            g.FillRectangle(brush, rCentro);
            g.FillEllipse(new SolidBrush(backgroundColor), rTesta);

            //Riquadro lo sfondo di nero
            Rectangle r = new Rectangle(0, 0, width - 1, height - 1);
            g.DrawRectangle(Pens.Black, r);

            brush.Dispose();
            g.Dispose();
        }

        //Function to rotate a rectangle
        public void RotateRectangle(Graphics g, Rectangle r, float angle, Brush brush)
        {
            using (Matrix m = new Matrix())
            {
                m.RotateAt(angle, new PointF(r.Left + (r.Width / 2), r.Top + (r.Height / 2)));
                g.Transform = m;
                g.FillRectangle(brush, r);
                g.ResetTransform();
            }
        }

        private void drawStringOnCanvas(string stringToPrint, int fontSize)
        {
            Graphics g = this.canvas.CreateGraphics();
            g.SmoothingMode = SmoothingMode.HighQuality;    //Imposto l'antialising (toglie l'effetto pixellato)
            int height = this.canvas.Height;
            int width = this.canvas.Width;
            int bordTop = 20;

            //Disegno il numero
            Brush brush = new SolidBrush(Color.Black);

            if (this.pfc == null)
            {
                //Reading font from properties
                byte[] font = Properties.Resources.Athletic;
                IntPtr fontBuffer = Marshal.AllocCoTaskMem(font.Length);
                Marshal.Copy(font, 0, fontBuffer, font.Length);
                pfc = new PrivateFontCollection();
                pfc.AddMemoryFont(fontBuffer, font.Length);
            }

            Font drawFont = new Font(pfc.Families[0], fontSize);
            StringFormat drawFormat = new StringFormat();
            drawFormat.Alignment = StringAlignment.Center;
            drawFormat.LineAlignment = StringAlignment.Center;
            int x = width / 2;
            g.DrawString(stringToPrint, drawFont, brush, x, (height + bordTop + 5) / 2, drawFormat);

            drawFont.Dispose();
            brush.Dispose();
            g.Dispose();
        }


        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (firstTimeDraw)
            {
                //Tutto questo per disegnare all'avvio
                this.drawShirtOnCanvas(this.coloreSquadra);
                this.drawStringOnCanvas(this.codSquadra, 40);
                this.firstTimeDraw = false;
            }
        }

        private void Squadra_Load(object sender, EventArgs e)
        {
        }
    }
}
