﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseballTours
{
    public class Torneo
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Squadra { get; set; }
    }
}
