﻿CREATE TABLE SQUADRA	(
   ID		CHAR (4)      NOT NULL,
   Nome		CHAR (30)     NOT NULL,
   Citta	CHAR (20)     NOT NULL,
   NumeroMembri  INT,
   Colore   CHAR (6),
   PRIMARY KEY (ID)
);