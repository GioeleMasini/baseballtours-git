﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class InserisciSquadra : Form
    {
        static string DEFAULT_IMAGE = Application.StartupPath + "\\..\\..\\res\\img\\baseball-batter.png";

        public string id { get; set; }

        Color coloreSquadra = new Color();
        Boolean editable = false;
        string insertSquadra;
        string nome;
        string citta;
        string colore;
        string urlLogo;
        System.Data.SqlServerCe.SqlCeCommand cmd;
        System.Data.SqlServerCe.SqlCeDataReader reader;

        public InserisciSquadra()
        {
            InitializeComponent();
        }

        public InserisciSquadra(string idToEdit)
        {
            InitializeComponent();

            string selection = "SELECT NOME, CITTA, COLORE, LOGO FROM SQUADRA WHERE ID = @ID";

            cmd = DbConnection.Open(selection);
            cmd.Parameters.AddWithValue("ID", idToEdit);
            reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                id = idToEdit;
                this.idSquadra.Text = id;
                nome = reader[0].ToString();
                this.nomeSquadra.Text = nome;
                citta = reader[1].ToString();
                this.cittàSquadra.Text = citta;
                colore = reader[2].ToString();
                this.scegliColore.Text = colore;
                this.coloreSquadra = Utilities.colorFromHex(colore);
                urlLogo = reader[3].ToString();
                try
                {
                    this.squadraLogo.Load(urlLogo);
                }
                catch (Exception) { };
            }

            cmd.Parameters.Clear();

            editable = true;

            idSquadra.Enabled = false;
        }

        private void caricaLogo_Click(object sender, EventArgs e)
        {
            // Mostra la open file dialog. Se l'utente clicca ok,
            // carica l'immagine che questi ha scelto.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                squadraLogo.Load(openFileDialog1.FileName);
            }
        }

        private void scegliColore_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.coloreSquadra = colorDialog1.Color;
                this.scegliColore.Text = this.coloreSquadra.Name;
            }

            scegliColore.Text = this.coloreSquadra.Name;
        }

        private void GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void idSquadra_Enter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(idSquadra, "Inserire un ID di 3 caratteri.");
        }

        private void Salva_Click(object sender, EventArgs e)
        {
            // Controllo presenza valori campi
            if (idSquadra.TextLength == 0 || nomeSquadra.TextLength == 0 || cittàSquadra.TextLength == 0 
                || scegliColore.Text.Equals("Scegli colore squadra"))
            {
                ErrorDialog.Show("Errore", "Inserire tutti i campi (escluso al più il logo).");
                return;
            }

            // Controllo formalità id della squadra
            if (!Checks.idSquadra(idSquadra.Text))
            {
                ErrorDialog.Show("Errore", "Il campo ID deve essere di 3 o 4 caratteri!");
                return;
            }

            id = idSquadra.Text;

            if (!editable)
            {
                //// Controllo presenza di tuple con stesso valore di chiave
                string checkKey = "SELECT * FROM SQUADRA WHERE ID = @ID";
                cmd = DbConnection.Open(checkKey);

                cmd.CommandText = checkKey;
                cmd.Parameters.AddWithValue("ID", id);
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    // Errore, tupla con valore di chiave già presente
                    ErrorDialog.Show("Errore di chiave", "Nel DB è già presente una squadra con questo valore di chiave.\n"
                        + "Inserire un valore di chiave valido");
                    return;
                }
                else
                {
                    // Nessuna tupla con quella chiave, può continuare, ripulisco il cmd.
                    cmd.Parameters.Clear();
                }

                this.DialogResult = DialogResult.OK;

                DbConnection.Close();
            }

            nome = nomeSquadra.Text;
            citta = cittàSquadra.Text;
            colore = Utilities.colorToHex(this.coloreSquadra);
            if (squadraLogo.ImageLocation == null)
            {
                urlLogo = DEFAULT_IMAGE;
                squadraLogo.Image = Image.FromFile(urlLogo);
            }
            else
                urlLogo = squadraLogo.ImageLocation;

            try
            {
                if (!editable)
                {
                    insertSquadra = "INSERT INTO SQUADRA "
                        + "VALUES (@ID, @NOME, @CITTA, @COLORE, @LOGO)";

                    cmd = DbConnection.Open(insertSquadra);
                }
                else
                {
                    insertSquadra = "UPDATE SQUADRA SET NOME = @NOME, CITTA = @CITTA, COLORE = @COLORE, "
                        + "LOGO = @LOGO WHERE ID = @ID";

                    cmd = DbConnection.Open(insertSquadra);
                }

                cmd.CommandText = insertSquadra;
                cmd.Parameters.AddWithValue("ID", id);
                cmd.Parameters.AddWithValue("NOME", nome);
                cmd.Parameters.AddWithValue("CITTA", citta);
                cmd.Parameters.AddWithValue("COLORE", colore);
                cmd.Parameters.AddWithValue("LOGO", urlLogo);

                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                {
                    //ERROR
                    ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                }
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception exc)
            {
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.GENERAL_ERROR_SQL);
                DbConnection.Close();
                return;
            }

            DbConnection.Close();
            this.Close();
            
            return;
        }
    }
}
