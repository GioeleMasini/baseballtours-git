﻿namespace BaseballTours
{
    partial class InserisciFormazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserisciFormazione));
            this.gridFormazione = new System.Windows.Forms.DataGridView();
            this.pERSONABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baseDataDataSet = new BaseballTours.BaseDataDataSet();
            this.rUOLOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rUOLOINNINGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sQUADRABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pERSONATableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.PERSONATableAdapter();
            this.sQUADRATableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.SQUADRATableAdapter();
            this.rUOLOINNINGTableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.RUOLOINNINGTableAdapter();
            this.rUOLOTableAdapter = new BaseballTours.BaseDataDataSetTableAdapters.RUOLOTableAdapter();
            this.cbGiocatore = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbRuolo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.numInning = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridFormazione)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERSONABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rUOLOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rUOLOINNINGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridFormazione
            // 
            this.gridFormazione.AllowUserToOrderColumns = true;
            this.gridFormazione.AllowUserToResizeRows = false;
            this.gridFormazione.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridFormazione.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFormazione.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbGiocatore,
            this.cbRuolo,
            this.numInning,
            this.ID});
            this.gridFormazione.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gridFormazione.Location = new System.Drawing.Point(2, 2);
            this.gridFormazione.Margin = new System.Windows.Forms.Padding(2);
            this.gridFormazione.MultiSelect = false;
            this.gridFormazione.Name = "gridFormazione";
            this.gridFormazione.RowTemplate.Height = 24;
            this.gridFormazione.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridFormazione.Size = new System.Drawing.Size(559, 287);
            this.gridFormazione.TabIndex = 0;
            // 
            // pERSONABindingSource
            // 
            this.pERSONABindingSource.DataMember = "PERSONA";
            this.pERSONABindingSource.DataSource = this.baseDataDataSet;
            // 
            // baseDataDataSet
            // 
            this.baseDataDataSet.DataSetName = "BaseDataDataSet";
            this.baseDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rUOLOBindingSource
            // 
            this.rUOLOBindingSource.DataMember = "RUOLO";
            this.rUOLOBindingSource.DataSource = this.baseDataDataSet;
            // 
            // rUOLOINNINGBindingSource
            // 
            this.rUOLOINNINGBindingSource.DataMember = "RUOLOINNING";
            this.rUOLOINNINGBindingSource.DataSource = this.baseDataDataSet;
            // 
            // sQUADRABindingSource
            // 
            this.sQUADRABindingSource.DataMember = "SQUADRA";
            this.sQUADRABindingSource.DataSource = this.baseDataDataSet;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(464, 308);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "Salva";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 308);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 39);
            this.button2.TabIndex = 4;
            this.button2.Text = "Indietro";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pERSONATableAdapter
            // 
            this.pERSONATableAdapter.ClearBeforeFill = true;
            // 
            // sQUADRATableAdapter
            // 
            this.sQUADRATableAdapter.ClearBeforeFill = true;
            // 
            // rUOLOINNINGTableAdapter
            // 
            this.rUOLOINNINGTableAdapter.ClearBeforeFill = true;
            // 
            // rUOLOTableAdapter
            // 
            this.rUOLOTableAdapter.ClearBeforeFill = true;
            // 
            // cbGiocatore
            // 
            this.cbGiocatore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cbGiocatore.DataPropertyName = "IDPERSONA";
            this.cbGiocatore.DataSource = this.pERSONABindingSource;
            this.cbGiocatore.DisplayMember = "COGNOME";
            this.cbGiocatore.HeaderText = "Giocatore";
            this.cbGiocatore.Name = "cbGiocatore";
            this.cbGiocatore.ValueMember = "ID";
            // 
            // cbRuolo
            // 
            this.cbRuolo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cbRuolo.DataPropertyName = "IDRUOLO";
            this.cbRuolo.DataSource = this.rUOLOBindingSource;
            this.cbRuolo.DisplayMember = "NOME";
            this.cbRuolo.HeaderText = "Ruolo";
            this.cbRuolo.Name = "cbRuolo";
            this.cbRuolo.ValueMember = "ID";
            // 
            // numInning
            // 
            this.numInning.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.numInning.DataPropertyName = "INNING";
            this.numInning.HeaderText = "Inning";
            this.numInning.Name = "numInning";
            this.numInning.Width = 71;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // InserisciFormazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(566, 358);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gridFormazione);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "InserisciFormazione";
            this.Text = "Baseball Tournaments - Persone";
            this.Load += new System.EventHandler(this.ListaVolontari_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridFormazione)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pERSONABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDataDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rUOLOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rUOLOINNINGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQUADRABindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridFormazione;
        private BaseDataDataSet baseDataDataSet;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource pERSONABindingSource;
        private BaseDataDataSetTableAdapters.PERSONATableAdapter pERSONATableAdapter;
        private System.Windows.Forms.BindingSource sQUADRABindingSource;
        private BaseDataDataSetTableAdapters.SQUADRATableAdapter sQUADRATableAdapter;
        private System.Windows.Forms.BindingSource rUOLOINNINGBindingSource;
        private BaseDataDataSetTableAdapters.RUOLOINNINGTableAdapter rUOLOINNINGTableAdapter;
        private System.Windows.Forms.BindingSource rUOLOBindingSource;
        private BaseDataDataSetTableAdapters.RUOLOTableAdapter rUOLOTableAdapter;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbGiocatore;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbRuolo;
        private System.Windows.Forms.DataGridViewTextBoxColumn numInning;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}