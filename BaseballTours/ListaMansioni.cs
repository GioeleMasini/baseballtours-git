﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaMansioni : Form
    {
        private string codTorneo;

        public ListaMansioni()
        {
            InitializeComponent();
        }

        public ListaMansioni(string codTorneo)
        {
            InitializeComponent();

            this.codTorneo = codTorneo;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.MANSIONE'. È possibile spostarla o rimuoverla se necessario.
            this.mANSIONETableAdapter.Fill(this.baseDataDataSet.MANSIONE);
            //PopulateGridView();
        }

        private void btnIndietro_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAggiungi_Click(object sender, EventArgs e)
        {
            //new InserisciMansione(this.codTorneo).ShowDialog();
            new InserisciMansione().ShowDialog();
            gridMansioniRefresh();
        }

        private void btnModifica_Click(object sender, EventArgs e)
        {
            /*string nomeMansione = this.gridMansioni.SelectedRows[0].Cells[0].Value.ToString();

            new InserisciMansione(this.codTorneo, nomeMansione).ShowDialog();*/
            new InserisciMansione((int)this.gridMansioni.SelectedRows[0].Cells[0].Value).ShowDialog();
            gridMansioniRefresh();
        }

        private void btnElimina_Click(object sender, EventArgs e)
        {
            string nomeMansione = this.gridMansioni.SelectedRows[0].Cells[0].Value.ToString();

            string deleteMansione = "DELETE FROM MANSIONE WHERE NOMEMANSIONE = @NOME AND NOMETORNEO = @TORNEO";
            var cmd = DbConnection.Open(deleteMansione);
            cmd.Parameters.AddWithValue("NOME", nomeMansione);
            cmd.Parameters.AddWithValue("TORNEO", this.codTorneo);
            
            var affectedRows = cmd.ExecuteNonQuery();
            
            DbConnection.Close();
            
            if (affectedRows != 1)
                ErrorDialog.Show("Errore", "Errore nella cancellazione!");
        }

        private void PopulateGridView()
        {
            string retrieve = "SELECT NOMEMANSIONE, DESCRIZIONE FROM MANSIONE "
                + "WHERE NOMETORNEO = @TORNEO";

            var cmd = DbConnection.Open(retrieve);
            cmd.Parameters.AddWithValue("TORNEO", codTorneo);
            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string[] rows = { reader[0].ToString(), reader[1].ToString() };

                gridMansioni.Rows.Add(rows);
            }

            cmd.Parameters.Clear();

            DbConnection.Close();
        }

        private void gridMansioniRefresh()
        {
            this.mANSIONETableAdapter.Fill(this.baseDataDataSet.MANSIONE);
            /*gridMansioni.Rows.Clear();
            PopulateGridView();*/
        }

        private void gridMansioni_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            new InserisciMansione((int)this.gridMansioni.CurrentRow.Cells[0].Value).ShowDialog();
            this.gridMansioniRefresh();
        }
    }
}
