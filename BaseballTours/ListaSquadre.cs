﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseballTours
{
    public partial class ListaSquadre : Form
    {
        public ListaSquadre()
        {
            InitializeComponent();
        }

        private void btnInserisci_Click(object sender, EventArgs e)
        {
            new InserisciSquadra().ShowDialog();

            this.sQUADRATableAdapter.Fill(this.baseDataDataSet.SQUADRA);
        }

        private void btnModifica_Click(object sender, EventArgs e)
        {
            string codSquadra = this.gridSquadre.SelectedRows[0].Cells[0].Value.ToString();
            new InserisciSquadra(codSquadra).ShowDialog();
            this.sQUADRATableAdapter.Fill(this.baseDataDataSet.SQUADRA);
        }

        private void ListaSquadre_Load(object sender, EventArgs e)
        {
            // TODO: questa riga di codice carica i dati nella tabella 'baseDataDataSet.SQUADRA'. È possibile spostarla o rimuoverla se necessario.
            this.sQUADRATableAdapter.Fill(this.baseDataDataSet.SQUADRA);
        }

        private void btnElimina_Click(object sender, EventArgs e)
        {
            string codSquadra = this.gridSquadre.SelectedRows[0].Cells[0].Value.ToString();

            try
            {
                controllaSquadra(codSquadra);
                
                string deleteSquadra = "DELETE FROM SQUADRA WHERE ID = @ID";
                var cmd = DbConnection.Open(deleteSquadra);
                cmd.Parameters.AddWithValue("ID", codSquadra);
                var affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows != 1)
                    ErrorDialog.Show("Errore", "Impossibile eliminare la squadra.");
            }
            catch (Exception exc)
            {
                //ERROR
                System.Console.Write(exc.StackTrace);
                ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                DbConnection.Close();
                return;
            }
            finally
            {
                DbConnection.Close();
                this.sQUADRATableAdapter.Fill(this.baseDataDataSet.SQUADRA);
            }
        }

        private void gridSquadre_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string codSquadra = this.gridSquadre.SelectedRows[0].Cells[0].Value.ToString();

            new Squadra(codSquadra).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*
         * Eseguo controlli sulla squadra: se è organizzatrice di tornei o gioca come ospite in qualche
         * partita: nel primo caso elimino prima i tornei, nel secondo caso le partite in cui è ospite,
         * perché non siamo riusciti a definire regole di eliminazione nel db
         */
        private void controllaSquadra(string codSquadra)
        {
            if (DbConnection.organizza(codSquadra))
            {
                /*
                 * la squadra è organizzatrice di qualche torneo, chiedo all'utente se è sicuro
                 * di voler eliminare la squadra e quindi anche tutti i tornei a cui fa riferimento
                 */
                DialogResult result = MessageBox.Show("Questa squadra è organizzatrice di tornei.\n"
                    + "Eliminando questa squadra eliminerai anche tutti i dati dei tornei di cui è organizzatrice."
                    + "\nProcedere con l'eliminazione?", "Eliminazione squadra", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    string deleteTornei = "DELETE FROM TORNEO WHERE IDSQUADRAORGANIZZATRICE = @ID";

                    var op = DbConnection.Open(deleteTornei);
                    op.Parameters.AddWithValue("ID", codSquadra);

                    int rows = op.ExecuteNonQuery();

                    if (rows < 1)
                    {
                        ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                        DbConnection.Close();
                        return;
                    }
                    DbConnection.Close();
                }
                else
                    return;
            }

            if (DbConnection.giocaOspite(codSquadra))
            {
                /*
                 * la squadra partecipa come ospite in qualche partita, le elimino
                 */
                string deletePartite = "DELETE FROM PARTITA WHERE IDSQUADRAOSPITI = @ID";

                var op = DbConnection.Open(deletePartite);
                op.Parameters.AddWithValue("ID", codSquadra);

                int rows = op.ExecuteNonQuery();

                if (rows < 1)
                {
                    ErrorDialog.Show("Errore", DbConnection.DELETE_ERROR);
                    DbConnection.Close();
                    return;
                }
                DbConnection.Close();
            }
            else
                return;
        }
    }
}
