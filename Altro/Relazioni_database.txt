Non sono riuscito a creare le seguenti relazioni:
- PARTITA_SQUADRAEDIZIONE_OSPITI
- TORNEO_SQUADRA_ORGANIZZATRICE
- PARTITA_FORMAZIONE_OSPITI		=> Non importa, tanto l'id in formazione non cambia mai

Ho tolto queste relazioni:
- PARTITA_EDIZIONE (in quanto ridondante nel ciclo con SQUADRAEDIZIONE ed EDIZIONE. 
		    Il "problema" adesso � che se si elimina una SQUADRAEDIZIONE vengono eliminate
		    anche le partite relative, ma nel caso basta mettere un warning)
